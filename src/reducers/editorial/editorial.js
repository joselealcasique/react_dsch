import {
    GET_LIST_REQUEST_EDITORIAL, LOGOUT_SUCCESS, AUTH_ERROR, GET_REQUEST_EDITORIAL,
} from '../../actions/types';


const initialState = {
    request: [],
    edit: null,
    load: false,
}

export default function (state = initialState, action) {
    switch (action.type){

        case GET_LIST_REQUEST_EDITORIAL:
            return {
                ...state,
                request: action.payload,
                load: action.load
            }

        case GET_REQUEST_EDITORIAL:
            return {
                ...state,
                edit: action.payload
            }

        case LOGOUT_SUCCESS:
        case AUTH_ERROR:
            return {
                ...state,
                request: null,
                edit: null,
                load: false,
            }

        default:
            return {
                ...state
            }
    }
}