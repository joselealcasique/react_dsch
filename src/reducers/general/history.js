import {
    GET_HISTORY_ACADEMIC_FOR_USER, FAIL_HISTORY_ACADEMIC_FOR_USER,
    GET_HISTORY_FOR_USER_FOR_NUMBER, FAIL_HISTORY_FOR_USER_FORM_NUMBER,
    LOGOUT_SUCCESS, AUTH_ERROR, GET_SCHOLARSHIP_NUMBER, FAIL_SCHOLARSHIP_NUMBER
} from '../../actions/types';


const initialState = {
    registers: null,
    load: false,
    history: null,
    scholarship: null
}

export default function (state = initialState, action) {
    switch (action.type){

        case GET_HISTORY_FOR_USER_FOR_NUMBER:
            return {
                ...state,
                history: action.payload,
                load: true
            }

        case FAIL_HISTORY_FOR_USER_FORM_NUMBER:
            return {
                ...state,
                history: null,
                load: false,
            }

        case GET_HISTORY_ACADEMIC_FOR_USER:
            return {
                ...state,
                registers: action.payload,
                load: true
            }

        case FAIL_HISTORY_ACADEMIC_FOR_USER:
            return {
                ...state,
                registers: null,
                load: false
            }

        case LOGOUT_SUCCESS:
        case AUTH_ERROR:
            return {
                ...state,
                registers: null,
                load: false,
                scholarship: null
            }

        case GET_SCHOLARSHIP_NUMBER:
            return {
                ...state,
                scholarship: action.payload
            }

        case FAIL_SCHOLARSHIP_NUMBER:
            return {
                ...state,
                scholarship: null
            }

        default:
            return {
                ...state
            }
    }
}