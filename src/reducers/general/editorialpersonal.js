import {
    GET_LIST_REQUEST_EDITORIAL_PERSONAL, GET_REQUEST_EDITORIAL, LOGOUT_SUCCESS, AUTH_ERROR,
} from '../../actions/types';


const initialState = {
    request: null,
    load: false,
    book: null
}

export default function (state = initialState, action) {
    switch (action.type){

        case GET_LIST_REQUEST_EDITORIAL_PERSONAL:
            return {
                ...state,
                request: action.payload,
                load: true
            }

        case GET_REQUEST_EDITORIAL:
            return {
                ...state,
                book: action.payload
            }

        case LOGOUT_SUCCESS:
        case AUTH_ERROR:
            return {
                ...state,
                request: null,
                load: false,
                book: null
            }

        default:
            return {
                ...state
            }
    }
}