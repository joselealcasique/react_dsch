import {
    LOGOUT_SUCCESS,
    AUTH_ERROR,
    CREATE_PROJECT_INVESTIGATION,
    GET_LIST_PROJECT_INVESTIGATION,
    GET_PROJECT_INVESTIGATION,
    GET_LETTERS_PROJECT_INVESTIGATION,
    SET_LETTER_PROJECT_INVESTIGATION,
    GET_PRESUPUESTAL_INVESTIGATION,
    CREATE_PRESUPUESTAL_INVESTIGATION,
    UPDATE_PRESUPUESTAL_INVESTIGATION,
    GET_LIST_ADVANCE_TO_PROJECT,
    ADD_ADVANCE_TO_PROJECT,
    GET_LIST_EXTENSIONS_TO_PROJECT,
    ADD_FILE_TO_EXTENSION_PROJECT,
    ADD_OFFICE_TO_EXTENSION_PROJECT,
    FINALIZED_PROCESS_EXTENSION
} from '../../actions/types';


const initialState = {
    list: [],
    letters: [],
    presupuestal: [],
    advance: [],
    extensions: [],
    edit: null,
    load: false,
}

export default function (state = initialState, action) {
    switch (action.type){

        case GET_LIST_EXTENSIONS_TO_PROJECT:
        case ADD_FILE_TO_EXTENSION_PROJECT:
        case ADD_OFFICE_TO_EXTENSION_PROJECT:
        case FINALIZED_PROCESS_EXTENSION:
            return {
                ...state,
                extensions: action.payload,
            }

        case ADD_ADVANCE_TO_PROJECT:
        case GET_LIST_ADVANCE_TO_PROJECT:
            return {
                ...state,
                advance: action.payload,
            }

        case GET_PRESUPUESTAL_INVESTIGATION:
        case CREATE_PRESUPUESTAL_INVESTIGATION:
        case UPDATE_PRESUPUESTAL_INVESTIGATION:
            return  {
                ...state,
                presupuestal: action.payload,
            }

        case GET_LETTERS_PROJECT_INVESTIGATION:
        case SET_LETTER_PROJECT_INVESTIGATION:
            return {
                ...state,
                letters: action.payload
            }

        case CREATE_PROJECT_INVESTIGATION:
        case GET_PROJECT_INVESTIGATION:
            return {
                ...state,
                edit: action.payload,
                load: true,
            }

        case GET_LIST_PROJECT_INVESTIGATION:
            return {
                ...state,
                list: action.payload,
            }

        case LOGOUT_SUCCESS:
        case AUTH_ERROR:
            return {
                ...state,
                list: [],
                letters: [],
                presupuestal: [],
                advance: [],
                extensions: [],
                edit: null,
                load: false,
            }

        default:
            return {
                ...state
            }
    }
}
