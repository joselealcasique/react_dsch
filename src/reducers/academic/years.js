import {
    FAIL_LOAD_YEARS, SUCCESS_LOAD_YEARS, LOGOUT_SUCCESS, AUTH_ERROR
} from '../../actions/types';


const initialState = {
    years: null,
    load: false
}


export default function (state = initialState, action){
    switch (action.type){

        case FAIL_LOAD_YEARS:
            return {
                ...state,
                load: false
            };

        case SUCCESS_LOAD_YEARS:
            return {
                ...state,
                load: true,
                years: action.payload
            }

        case LOGOUT_SUCCESS:
        case AUTH_ERROR:
            return {
                ...state,
                years: null,
                load: false
            }

        default:
            return {
                ...state
            }
    }
}