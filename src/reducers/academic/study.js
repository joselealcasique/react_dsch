import {
    FAIL_LOAD_STUDY_PLANS, SUCCESS_LOAD_STUDY_PLANS, LOGOUT_SUCCESS, AUTH_ERROR
} from '../../actions/types';


const initialState = {
    study_plans: null,
    load: false
}


export default function (state = initialState, action){
    switch (action.type){

        case FAIL_LOAD_STUDY_PLANS:
            return {
                ...state,
                load: false
            };

        case SUCCESS_LOAD_STUDY_PLANS:
            return {
                ...state,
                load: true,
                study_plans: action.payload
            }

        case LOGOUT_SUCCESS:
        case AUTH_ERROR:
            return {
                ...state,
                study_plans: null,
                load: false
            }

        default:
            return {
                ...state
            }
    }
}