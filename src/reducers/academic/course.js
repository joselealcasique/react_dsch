import {
    SUCCESS_LOAD_COURSES, LOGOUT_SUCCESS, AUTH_ERROR
} from '../../actions/types';


const initialState = {
    courses: null,
    load: false
}


export default function (state = initialState, action){
    switch (action.type){

        case SUCCESS_LOAD_COURSES:
            return {
                ...state,
                load: true,
                courses: action.payload
            };

        case LOGOUT_SUCCESS:
        case AUTH_ERROR:
            return {
                ...state,
                courses: null,
                load: false
            }

        default:
            return {
                ...state
            }
    }
}