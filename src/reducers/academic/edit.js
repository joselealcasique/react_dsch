import {
   GET_EDIT_REGISTER_ACADEMIC, FAIL_EDIT_REGISTER_ACADEMIC, SUCCESS_EDIT_REGISTER_ACADEMIC,
    LOGOUT_SUCCESS, AUTH_ERROR
} from '../../actions/types';


const initialState = {
    register: null,
    inProcess: false,
    finalized: false
}

export default function (state = initialState, action) {
    switch (action.type){

        case GET_EDIT_REGISTER_ACADEMIC:
            return {
                ...state,
                register: action.payload,
                inProcess: true,
                finalized: false
            }

        case SUCCESS_EDIT_REGISTER_ACADEMIC:
            return {
                ...state,
                register: action.payload,
                inProcess: true,
                finalized: true
            };

        case FAIL_EDIT_REGISTER_ACADEMIC:
            return {
                ...state,
                register: null,
                inProcess: false,
                finalized: true,
            }

        case LOGOUT_SUCCESS:
        case AUTH_ERROR:
            return {
                ...state,
                register: null,
                inProcess: false,
                finalized: false
            }

        default:
            return {
                ...state
            }
    }
}