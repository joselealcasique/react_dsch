import {
    GET_EDIT_REGISTER_SCHOLARSHIP, FAIL_EDIT_REGISTER_SCHOLARSHIP,
    SUCCESS_EDIT_REGISTER_SCHOLARSHIP, REGISTER_DICTUM_SCHOLARSHIP,
    REGISTER_NOTIFICATION_SCHOLARSHIP, REGISTER_RESOLUTION_SCHOLARSHIP,
    REGISTER_REPLY_SCHOLARSHIP, REGISTER_BIS_SCHOLARSHIP, REGISTER_FiNALIZED_SCHOLARSHIP,
    REGISTER_RECEIPT_BIS_SCHOLARSHIP, REGISTER_RECEIPT_DICTUM_SCHOLARSHIP, LOGOUT_SUCCESS,
    AUTH_ERROR, DELETED_SCHOLARSHIP, LIST_FILES_ADDITIONAL, NOT_LIST_FILES_ADDITIONAL,
    GET_SCHOLARSHIP_NUMBER, GET_SCHOLARSHIP_YEAR
} from '../../actions/types';


const initialState = {
    register: null,
    files: [],
    inProcess: false,
    finalized: false,
    delete: false
}

export default function (state = initialState, action) {
    switch (action.type){

        case GET_EDIT_REGISTER_SCHOLARSHIP:
            return {
                ...state,
                register: action.payload,
                inProcess: true,
                finalized: false
            }

        case SUCCESS_EDIT_REGISTER_SCHOLARSHIP:
            return {
                ...state,
                register: action.payload,
                inProcess: true,
                finalized: true
            };

        case FAIL_EDIT_REGISTER_SCHOLARSHIP:
            return {
                ...state,
                register: null,
                inProcess: false,
                finalized: true,
            }

        case LIST_FILES_ADDITIONAL:
            return {
                ...state,
                files: action.payload
            }

        case NOT_LIST_FILES_ADDITIONAL:
        case GET_SCHOLARSHIP_NUMBER:
        case GET_SCHOLARSHIP_YEAR:
            return {
                ...state,
                files: []
            }

        case REGISTER_DICTUM_SCHOLARSHIP:
        case REGISTER_NOTIFICATION_SCHOLARSHIP:
        case REGISTER_RESOLUTION_SCHOLARSHIP:
        case REGISTER_REPLY_SCHOLARSHIP:
        case REGISTER_BIS_SCHOLARSHIP:
        case REGISTER_RECEIPT_BIS_SCHOLARSHIP:
        case REGISTER_RECEIPT_DICTUM_SCHOLARSHIP:
        case REGISTER_FiNALIZED_SCHOLARSHIP:
            return {
                ...state,
                register: action.payload
            }

        case DELETED_SCHOLARSHIP:
            return {
                ...state,
                register: null,
                inProcess: false,
                finalized: false,
                files: [],
                delete: true
            }

        case LOGOUT_SUCCESS:
        case AUTH_ERROR:
            return {
                ...state,
                register: null,
                inProcess: false,
                finalized: false,
                delete: false,
                files: [],
            }

        default:
            return {
                ...state
            }
    }
}