import {
    LOGOUT_SUCCESS, AUTH_ERROR, GET_LIST_TYPES_REPORT
} from '../../actions/types';


const initialState = {
    list: [],
    load: false,
}

export default function (state = initialState, action) {
    switch (action.type){

        case GET_LIST_TYPES_REPORT:
            return {
                ...state,
                list: action.payload,
                load: true
            }

        case LOGOUT_SUCCESS:
        case AUTH_ERROR:
            return {
                ...state,
                list: [],
                load: false,
            }

        default:
            return {
                ...state
            }
    }
}
