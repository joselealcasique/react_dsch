import {
    LOGOUT_SUCCESS, AUTH_ERROR, GET_LIST_TEACHER_TRAINING, GET_LIST_EXCHANGE_ACTIVITY,
    GET_LIST_ACADEMIC_EVENT, GET_LIST_ACADEMIC_COLLABORATION, GET_LIST_PROJECT_COLLABORATION,
    GET_LIST_MAGAZINE_PUBLICATIONS, GET_LIST_ELECTRONIC_JOURNALS, GET_LIST_NEWSPAPER_PUBLICATION,
    GET_LIST_PUBLISHED_BOOKS, GET_LIST_CHAPTERS_BOOKS, GET_LIST_REVIEWS_BOOKS,
    GET_LIST_PUBLISHED_LECTURES, GET_LIST_UNPUBLISHED_LECTURES, GET_LIST_RESEARCH_PROJECTS,
    GET_REPORT_PERSONAL_FOR_USER
} from '../../actions/types';


const initialState = {
    training: [],
    exchange: [],
    event: [],
    a_collaboration: [],
    p_collaboration: [],
    journals: [],
    magazine: [],
    newspaper: [],
    books: [],
    chapters: [],
    reviews: [],
    lectures: [],
    u_lectures: [],
    project: [],
    view: null,
    load: false,
    personal: false
}

export default function (state = initialState, action) {
    switch (action.type){

        case GET_LIST_RESEARCH_PROJECTS:
            return {
                ...state,
                project: action.payload,
                load: action.load
            }

        case GET_LIST_UNPUBLISHED_LECTURES:
            return {
                ...state,
                u_lectures: action.payload,
                load: action.load
            }

        case GET_LIST_PUBLISHED_LECTURES:
            return {
                ...state,
                lectures: action.payload,
                load: action.load
            }

        case GET_LIST_REVIEWS_BOOKS:
            return  {
                ...state,
                reviews: action.payload,
                load: action.load
            }

        case GET_LIST_CHAPTERS_BOOKS:
            return {
                ...state,
                chapters: action.payload,
                load: action.load
            }

        case GET_LIST_PUBLISHED_BOOKS:
            return {
                ...state,
                books: action.payload,
                load: action.load
            }

        case GET_LIST_NEWSPAPER_PUBLICATION:
            return {
                ...state,
                newspaper: action.payload,
                load: action.load
            }

        case GET_LIST_ELECTRONIC_JOURNALS:
            return {
                ...state,
                journals: action.payload,
                load: action.load
            }

        case GET_LIST_MAGAZINE_PUBLICATIONS:
            return {
                ...state,
                magazine: action.payload,
                load: action.load
            }

        case GET_LIST_PROJECT_COLLABORATION:
            return {
                ...state,
                p_collaboration: action.payload,
                load: action.load
            }

        case GET_LIST_ACADEMIC_COLLABORATION:
            return {
                ...state,
                a_collaboration: action.payload,
                load: action.load
            }

        case GET_LIST_EXCHANGE_ACTIVITY:
            return {
                ...state,
                exchange: action.payload,
                load: action.load
            }

        case GET_LIST_TEACHER_TRAINING:
            return {
                ...state,
                training: action.payload,
                load: action.load
            }

        case GET_LIST_ACADEMIC_EVENT:
            return {
                ...state,
                event: action.payload,
                load: action.load
            }

        case GET_REPORT_PERSONAL_FOR_USER:
            return {
                ...state,
                training: action.payload.teacher_training,
                exchange: action.payload.exchange_activity,
                event: action.payload.academic_events,
                a_collaboration: action.payload.academic_collaboration,
                p_collaboration: action.payload.project_collaboration,
                journals: action.payload.electronic_journals,
                magazine: action.payload.magazine_publications,
                newspaper: action.payload.newspaper_publication,
                books: action.payload.published_books,
                chapters: action.payload.chapters_books,
                reviews: action.payload.book_reviews,
                lectures: action.payload.published_lectures,
                u_lectures: action.payload.unpublished_lectures,
                project: action.payload.research_projects,
                view: null,
                load: true,
                personal: true,
            }

        case LOGOUT_SUCCESS:
        case AUTH_ERROR:
            return {
                ...state,
                training: [],
                exchange: [],
                event: [],
                a_collaboration: [],
                p_collaboration: [],
                journals: [],
                magazine: [],
                newspaper: [],
                books: [],
                chapters: [],
                reviews: [],
                lectures: [],
                u_lectures: [],
                project: [],
                view: null,
                load: false,
                personal: false,
            }

        default:
            return {
                ...state
            }
    }
}
