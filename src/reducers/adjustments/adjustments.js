import {
    LOAD_PLANS_ADJUSTMENTS,
    LOAD_COLLEGIATE_ADJUSTMENTS,
    GET_LIST_ADJUSTMENTS_FOR_LIST,
    GET_ADJUSTMENTS_RETRIEVE,
    ADD_ADJUSTMENTS_MODIFICATION,
    EDIT_ADJUSTMENTS_MODIFICATION,
    FINALIZED_ADJUSTMENTS_MODIFICATION,
    AUTH_ERROR,
    LOGOUT_SUCCESS
} from '../../actions/types';


const initialState = {
    load: false,
    load_c: false,
    plans: [],
    collegiate: [],
    list: [],
    view: null,
    modifications: []
}

export default function (state = initialState, action) {
    switch (action.type){

        case LOAD_PLANS_ADJUSTMENTS:
            return {
                ...state,
                plans: action.payload.results,
                load: true
            }

        case LOAD_COLLEGIATE_ADJUSTMENTS:
            return {
                ...state,
                collegiate: action.payload.results,
                load_c: true
            }

        case GET_LIST_ADJUSTMENTS_FOR_LIST:
            return {
                ...state,
                list: action.payload.results,
                view: null,
                modifications: []
            }

        case GET_ADJUSTMENTS_RETRIEVE:
        case ADD_ADJUSTMENTS_MODIFICATION:
        case EDIT_ADJUSTMENTS_MODIFICATION:
        case FINALIZED_ADJUSTMENTS_MODIFICATION:
            return {
                ...state,
                view: action.payload,
                modifications: action.payload.modifications
            }

        case LOGOUT_SUCCESS:
        case AUTH_ERROR:
            return {
                ...state,
                list: [],
                plans: [],
                modifications: [],
                collegiate: [],
                load: false,
            }

        default:
            return {
                ...state
            }

    }
}