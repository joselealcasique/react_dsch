import {
    LOGIN_FAIL, LOGIN_SUCCESS, USER_LOADED, USER_LOADING,
    AUTH_ERROR, AUTH_RESET, RESET_FAIL, RESET_SUCCESS,
    LOGOUT_SUCCESS, SERVER_CONNECTION_REFUSED
} from '../../actions/types';


const initialState = {
    isAuthenticated: false,
    inProcess: false,
    verified: false,
    user: null,
    permissions: []
}

export default function (state = initialState, action) {
    switch (action.type){
        case SERVER_CONNECTION_REFUSED:
            localStorage.removeItem('access');
            localStorage.removeItem('refresh');
            return {
                ...state,
                isAuthenticated: false,
                inProcess: false,
                verified: false,
                user: null,
                permissions: []
            }

        case LOGOUT_SUCCESS:
            localStorage.removeItem('access');
            localStorage.removeItem('refresh');
            return {
                ...state,
                user: null,
                verified: false,
                inProcess: false,
                isAuthenticated: false,
                permissions: []
            }

        case USER_LOADING:
            return {
                ...state,
                inProcess: true
            };

        case USER_LOADED:
            return {
                ...state,
                isAuthenticated: true,
                inProcess: false,
                verified: action.payload.is_verified,
                user: action.payload.user,
                permissions: action.payload.permissions
            }

        case LOGIN_SUCCESS:
            localStorage.setItem('access', action.payload.tokens.access);
            localStorage.setItem('refresh', action.payload.tokens.refresh);

            return {
                ...state,
                isAuthenticated: true,
                inProcess: false,
                verified: action.payload.is_verified,
                user: action.payload.user,
                permissions: action.payload.permissions
            };

        case AUTH_ERROR:
        case LOGIN_FAIL:
            return {
                ...state,
                isAuthenticated: false,
                inProcess: false,
                verified: false,
                user: null,
                permissions: [],
            }

        case AUTH_RESET:
        case RESET_FAIL:
            return {
                ...state,
                isAuthenticated: true,
                inProcess: true,
                verified: false
            };

        case RESET_SUCCESS:
            localStorage.removeItem('access');
            localStorage.removeItem('refresh');
            return {
                ...state,
                inProcess: false,
                isAuthenticated: false
            }

        default:
            return {
                ...state
            }
    }
}