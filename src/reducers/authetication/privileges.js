import {
    GET_PERMISSIONS,
    REMOVE_PERMISSION,
    ASSIGN_PERMISSION,
    AUTH_ERROR,
    LOGOUT_SUCCESS
} from '../../actions/types';


const initialState = {
    list: null,
    load: false,
}

export default function (state = initialState, action) {
    switch (action.type){

        case GET_PERMISSIONS:
        case REMOVE_PERMISSION:
        case ASSIGN_PERMISSION:
            return {
                ...state,
                list: action.payload,
                load: true
            }

        case LOGOUT_SUCCESS:
        case AUTH_ERROR:
            return {
                ...state,
                list: null,
                load: false
            }

        default:
            return {
                ...state
            }
    }
}