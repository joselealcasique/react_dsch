import axios from 'axios';

import { returnErrors, createMessage } from '../messages';
import {
    SUCCESS_REGISTER_ACADEMIC_FILE,
    FAIL_REGISTER_ACADEMIC_FILE,
    SERVER_CONNECTION_REFUSED,
    AUTH_ERROR
} from "../types";
import { SERVER } from '../server';


export const register_file = (file, pk) => (dispatch) => {
    const token = localStorage.getItem('access');
    const config = {
        'headers': {}}

    if (token) {
        config.headers['Authorization'] = `Bearer ${token}`;
    }

    axios.post(`${SERVER}/academic/upload-statistics/${pk}/`, file, config)
        .then((res) => {
            dispatch(createMessage({registerFile: 'Información Almacenada Correctamente'}));
            dispatch({ type: SUCCESS_REGISTER_ACADEMIC_FILE });
        })
        .catch((err) => {
            try{
                dispatch({ type: FAIL_REGISTER_ACADEMIC_FILE });
                switch (err.response.status){
                    case 500:
                        dispatch(returnErrors(
                            'Verifique el archivo, no sé pudo guardar la información', 500));
                        break;

                    case 400:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 400));

                        if (err.response.data.file) dispatch(
                            returnErrors(err.response.data.detail, 400));
                        break;

                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break
                    case 406:
                        dispatch(createMessage({detailFile: err.response.data.file}));
                        break;

                    default:
                        console.log(err.response);
                }

            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });


}

export default register_file;
