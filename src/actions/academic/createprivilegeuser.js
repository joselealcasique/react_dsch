import axios from 'axios';
import {
    POST_USERS_PRIVILEGES_VIEW,
    SERVER_CONNECTION_REFUSED,
    AUTH_ERROR
} from '../types';
import {createMessage, returnErrors} from '../messages';
import { configAuthToken } from '../configAuthToken';
import { SERVER } from '../server';


export const post_user_privilege = (number, privilege) => (dispatch) => {

    const data = JSON.stringify({number, privilege});

    axios.post(`${SERVER}/academic/users/`, data, configAuthToken())
        .then((res) => {
            dispatch(createMessage({
                registerStatistics: 'Información Guardada Correctamente'}));
        })
        .catch((err) => {
            try{
                switch (err.response.status){
                    case 400:
                        if (err.response.data.privilege) dispatch({
                            type: POST_USERS_PRIVILEGES_VIEW,
                            msg: err.response.data.privilege
                        });

                        if (err.response.data.number) dispatch({
                            type: POST_USERS_PRIVILEGES_VIEW,
                            msg: err.response.data.number
                        });

                        if (err.response.data.message) dispatch({
                            type: POST_USERS_PRIVILEGES_VIEW,
                            msg: err.response.data.message
                        });

                        break;

                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    default:
                        console.log(err.response);
                }

            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage(
                        {serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default post_user_privilege;
