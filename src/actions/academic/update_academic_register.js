import axios from 'axios';
import {
    SUCCESS_EDIT_REGISTER_ACADEMIC,
    FAIL_EDIT_REGISTER_ACADEMIC,
    SERVER_CONNECTION_REFUSED,
    AUTH_ERROR
} from '../types';
import { returnErrors, createMessage } from '../messages';
import { configAuthToken } from '../configAuthToken';
import { SERVER } from '../server';


export const update_academic_register = (pk, number_course, number_hour, number_student ) => (dispatch) => {

    const data = JSON.stringify({number_course, number_hour, number_student});

    axios.put(`${SERVER}/academic/update-academic/${pk}/`, data, configAuthToken())
        .then((res) => {
            dispatch(createMessage({
                detail: 'Información Actualizada Correctamente'}));
            dispatch({
                type: SUCCESS_EDIT_REGISTER_ACADEMIC,
                payload: res.data
            });
        })
        .catch((err) => {
            try{
                switch (err.response.status){
                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    case 404:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 404));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    default:
                        console.log(err.response);
                }
                dispatch(returnErrors(err.response.data, err.response.status));
                dispatch({type: FAIL_EDIT_REGISTER_ACADEMIC});
            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default update_academic_register;
