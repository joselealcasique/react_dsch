import axios from 'axios';
import {
    SUCCESS_REGISTER_ACADEMIC_STATICS,
    FAIL_REGISTER_ACADEMIC_STATICS,
    SERVER_CONNECTION_REFUSED,
    AUTH_ERROR
} from '../types';
import {createMessage, returnErrors} from '../messages';
import { configAuthToken } from '../configAuthToken';
import { SERVER } from '../server';


export const register_academic_statistics = (
    user, study_plan, departament, year, trimester, classroom, course,
    number_course, number_student, number_hour
    ) => (dispatch) => {

    const data = JSON.stringify({
        user, study_plan, departament, year, trimester, classroom, course, number_course,
        number_student, number_hour
    });

    axios.post(`${SERVER}/academic/create-statistics/`, data, configAuthToken())
        .then((res) => {
            dispatch(createMessage({
                registerStatistics: 'Información Guardada Correctamente'}));
            dispatch({
                type: SUCCESS_REGISTER_ACADEMIC_STATICS,
                payload: res.data.results
            });
        })
        .catch((err) => {
            try{
                dispatch({ type: FAIL_REGISTER_ACADEMIC_STATICS });
                switch (err.response.status){
                    case 400:
                        if (err.response.data.course) dispatch(
                            returnErrors('Nombre del Curso: '+err.response.data.course, 400));
                        if (err.response.data.user) dispatch(
                            returnErrors('Número Económico: '+err.response.data.user, 400));
                        if (err.response.data) dispatch(
                            returnErrors(err.response.data, 400));
                        break;

                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    default:
                        console.log(err.response);
                }

            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage(
                        {serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default register_academic_statistics;
