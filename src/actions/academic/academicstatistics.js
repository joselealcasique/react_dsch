import axios from 'axios';
import {GET_ACADEMIC_STATISTICS, FAIL_ACADEMIC_STATISTICS, SERVER_CONNECTION_REFUSED, AUTH_ERROR} from '../types';
import {createMessage, returnErrors} from '../messages';
import { configAuthToken } from '../configAuthToken';
import { SERVER } from '../server';


export const get_academic_statistics = (plan, departament, year, trimester) => (dispatch) => {

    const url = `${SERVER}/academic/list-statistics/${plan}/${departament}/${year}/${trimester}/`
    axios.get(url, configAuthToken())
        .then((res) => {
            if (res.data.results.length > 0) dispatch(
                { type: GET_ACADEMIC_STATISTICS, payload: res.data.results});
        })
        .catch((err) => {
            try{
                dispatch({ type: FAIL_ACADEMIC_STATISTICS });
                switch (err.response.status){
                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;
                    case 404:
                        break;

                    default:
                        console.log(err.response);
                }

            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default get_academic_statistics;
