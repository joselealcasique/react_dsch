import axios from 'axios';
import { returnErrors, createMessage } from '../messages';
import { SERVER_CONNECTION_REFUSED, AUTH_ERROR, EDIT_REQUEST_SABBATICAL } from "../types";

import { SERVER } from '../server';
import { configAuthTokenFiles } from '../configAuthToken';


export const edit_request_sabbatical = (pk, data) => (dispatch) => {

    axios.put(`${SERVER}/sabbatical/update/${pk}/`, data, configAuthTokenFiles())
        .then((res) => {
            alert(
                'Cambios guardados correctamente. Los cambios puedan tardar unos minutos en actualizar'
            );
            dispatch({
                type: EDIT_REQUEST_SABBATICAL,
                payload: res.data
            })

        })
        .catch((err) => {
            try{
                switch (err.response.status){
                    case 400:
                        if (err.response.data.user) dispatch(
                            returnErrors("Usuario: " +
                                err.response.data.user, 400));

                        if (err.response.data.date_initial) dispatch(
                            returnErrors("Fecha de inicio: " +
                                err.response.data.date_initial, 400));

                        if (err.response.data.date_final) dispatch(
                            returnErrors("Fecha de finalización: " +
                                err.response.data.date_final, 400));

                        if (err.response.data.months) dispatch(
                            returnErrors("Meses de duración: " +
                                err.response.data.months, 400));

                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 400));
                        break;

                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    case 500:
                        dispatch(returnErrors(
                            'Verifique el archivo, no sé pudo guardar la información',
                            500));
                        break;

                    default:
                        console.log(err.response);
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({
                        serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default edit_request_sabbatical;
