import axios from 'axios';
import { SERVER_CONNECTION_REFUSED, AUTH_ERROR } from '../types';
import {createMessage, returnErrors} from '../messages';
import { configAuthToken } from '../configAuthToken';
import { SERVER } from '../server';


export const get_report_sabbatical = (initial, final) => (dispatch) => {

    axios.get(
        `${SERVER}/sabbatical/export/report/${initial}/${final}/`,
        configAuthToken())
        .then((res) => {
            window.open(
                `${SERVER}/sabbatical/export/report/${initial}/${final}/`
            )
        })
        .catch((err) => {
            try {
                switch (err.response.status){
                    case 400:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 400));
                        alert(err.response.data.detail);
                        break;

                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    default:
                        console.log(err.response);
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage(
                        {serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default get_report_sabbatical;
