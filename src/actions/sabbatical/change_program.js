import axios from 'axios';
import { returnErrors, createMessage } from '../messages';
import { SERVER_CONNECTION_REFUSED, AUTH_ERROR, GET_DETAIL_SABBATICAL } from "../types";

import { SERVER } from '../server';
import { configAuthTokenFiles } from '../configAuthToken';


export const change_program = (pk, data) => (dispatch) => {

    axios.put(`${SERVER}/sabbatical/edit/program/${pk}/`, data, configAuthTokenFiles())
        .then((res) => {
            alert('Modificación enviada. Puede tardar unos minutos en visualizarse en sistema.');
            dispatch({
                type: GET_DETAIL_SABBATICAL,
                payload: res.data
            })
        })
        .catch((err) => {
            try{
                switch (err.response.status){

                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    case 500:
                        dispatch(returnErrors(
                            'Verifique el archivo, no sé pudo guardar la información', 500));
                        break;

                    default:
                        console.log(err.response);
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default change_program;
