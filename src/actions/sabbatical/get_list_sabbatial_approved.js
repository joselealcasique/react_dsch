import axios from 'axios';
import { returnErrors, createMessage } from '../messages';
import { SERVER_CONNECTION_REFUSED, AUTH_ERROR, GET_lIST_SABBATICAL_APPROVED } from "../types";

import { SERVER } from '../server';
import { configAuthToken } from '../configAuthToken';


export const get_list_sabbatical_approved = (number, year) => (dispatch) => {

    axios.get(`${SERVER}/sabbatical/list/accept/sabbatical/${number}/${year}/`, configAuthToken())
        .then((res) => {
            dispatch({
                type: GET_lIST_SABBATICAL_APPROVED,
                payload: res.data.results,
            });
        })
        .catch((err) => {
            try{
                switch (err.response.status){

                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    case 500:
                        dispatch(returnErrors(
                            'Verifique el archivo, no sé pudo guardar la información', 500));
                        break;

                    default:
                        console.log(err.response);
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default get_list_sabbatical_approved;
