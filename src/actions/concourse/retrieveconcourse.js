import axios from 'axios';
import { GET_CONCOURSE, SERVER_CONNECTION_REFUSED, AUTH_ERROR } from '../types';
import {createMessage, returnErrors} from '../messages';
import { configAuthToken } from '../configAuthToken';
import { SERVER } from '../server';


export const get_retrieve_concourse = (pk) => (dispatch) => {

    axios.get(`${SERVER}/concourse/retrieve/${pk}/`, configAuthToken())
        .then((res) => {
            dispatch({
                type: GET_CONCOURSE,
                payload: res.data,
            });
        })
        .catch((err) => {
            try {
                switch (err.response.status){
                    case 404:
                        break;

                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break

                    default:
                        console.log(err.response);
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default get_retrieve_concourse;
