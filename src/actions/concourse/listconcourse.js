import axios from 'axios';
import { GET_LIST_CONCOURSE, SERVER_CONNECTION_REFUSED, AUTH_ERROR } from '../types';
import {createMessage, returnErrors} from '../messages';
import { configAuthToken } from '../configAuthToken';
import { SERVER } from '../server';


export const get_list_concourse = (year, status, type_concourse) => (dispatch) => {

    axios.get(`${SERVER}/concourse/list-concourse/${year}/${status}/${type_concourse}/`,
        configAuthToken())
        .then((res) => {
            dispatch({
                type: GET_LIST_CONCOURSE,
                payload: res.data,
                load: true
            });
        })
        .catch((err) => {
            try {
                switch (err.response.status){
                    case 404:
                        break;

                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break

                    default:
                        console.log(err.response);
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default get_list_concourse;
