import axios from 'axios';
import { returnErrors, createMessage } from '../messages';
import {SERVER_CONNECTION_REFUSED, DELETE_ASPIRANT_FOR_CONCOURSE, AUTH_ERROR} from "../types";
import { SERVER } from '../server';
import {configAuthToken} from '../configAuthToken';


export const set_delete_aspirant = (pk) => (dispatch) => {

    axios.delete(`${SERVER}/concourse/delete-aspirant/${pk}/`, configAuthToken())
        .then((res) => {
            dispatch({ type: DELETE_ASPIRANT_FOR_CONCOURSE, payload: res.data});
        })
        .catch((err) => {
            try{
                switch (err.response.status){
                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    case 404:
                        dispatch(createMessage({NotFound: 'NO HAY COINCIDENCIAS'}));
                        break;

                    case 500:
                        break;

                    default:
                        console.log(err.response);
                }

            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default set_delete_aspirant;
