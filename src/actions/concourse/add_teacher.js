import axios from 'axios';
import { returnErrors, createMessage } from '../messages';
import {SERVER_CONNECTION_REFUSED, ADD_TEACHER_FOR_CONCOURSE, AUTH_ERROR} from "../types";
import { SERVER } from '../server';
import { configAuthTokenFiles } from '../configAuthToken';


export const add_teacher = (data, pk, ) => (dispatch) => {

    axios.post(`${SERVER}/concourse/teacher/add/${pk}/`, data, configAuthTokenFiles())
        .then((res) => {
            dispatch(createMessage({registerFile: 'Información Almacenada Correctamente'}));
            dispatch({ type: ADD_TEACHER_FOR_CONCOURSE, payload: res.data});
        })
        .catch((err) => {
            try{
                switch (err.response.status){
                    case 400:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 400));

                        if (err.response.data.name) dispatch(
                            returnErrors('Nombre: '+err.response.data.name, 400));

                        if (err.response.data.paternal_surname) dispatch(
                            returnErrors('Apellido Paterno: '+err.response.data.paternal_surname, 400));

                        if (err.response.data.maternal_surname) dispatch(
                            returnErrors('Apellido Materno: '+err.response.data.maternal_surname, 400));

                        if (err.response.data.mail_personal) dispatch(
                            returnErrors('Correo Personal: '+err.response.data.mail_personal, 400));

                        if (err.response.data.area) dispatch(
                            returnErrors('Departamento: '+err.response.data.area, 400));

                        if (err.response.data.number) dispatch(
                            returnErrors('Número Económico: '+err.response.data.number, 400));
                        break;

                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    case 500:
                        dispatch(returnErrors(
                            'Verifique el archivo, no sé pudo guardar la información', 500));
                        break;

                    default:
                        console.log(err.response);
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });


}

export default add_teacher;
