import axios from 'axios';
import { ADD_SECOND_EXTENSION_CONCOURSE, SERVER_CONNECTION_REFUSED, AUTH_ERROR } from '../types';
import {createMessage, returnErrors} from '../messages';
import { configAuthTokenFiles } from '../configAuthToken';
import { SERVER } from '../server';


export const add_second_extension = ( data, pk ) => (dispatch) => {

    axios.post(`${SERVER}/concourse/second-extension/${pk}/`, data, configAuthTokenFiles())
        .then((res) => {
            alert('Información Almacenada');
            dispatch({ type: ADD_SECOND_EXTENSION_CONCOURSE, payload: res.data});

        })
        .catch((err) => {
            try{
                switch (err.response.status){
                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    default:
                        console.log(err.response);
                }

            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage(
                        {serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default add_second_extension;
