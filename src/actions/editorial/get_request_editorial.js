import axios from 'axios';
import {AUTH_ERROR, GET_REQUEST_EDITORIAL, SERVER_CONNECTION_REFUSED} from '../types';
import {createMessage, returnErrors} from '../messages';
import { configAuthToken } from '../configAuthToken';
import { SERVER } from '../server';


export const get_request_editorial = (pk) => (dispatch) => {

    const url = `${SERVER}/editorial/retrieve-request/${pk}/`;
    axios.get(url, configAuthToken())
        .then((res) => {
            dispatch({ type: GET_REQUEST_EDITORIAL, payload: res.data });
        })
        .catch((err) => {
            try {
                switch (err.response.status){
                    case 404:
                        dispatch(createMessage({NotFound: 'NO HAY COINCIDENCIAS'}))
                        break;

                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break

                    default:
                        console.log(err.response);
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default get_request_editorial;
