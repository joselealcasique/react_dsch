import axios from 'axios';
import { returnErrors, createMessage } from '../messages';
import {SERVER_CONNECTION_REFUSED, GET_REQUEST_EDITORIAL, AUTH_ERROR} from "../types";
import { SERVER } from '../server';
import { configAuthToken } from '../configAuthToken';


export const add_meeting = (date, status, pk) => (dispatch) => {

    const data = JSON.stringify({date, status});

    axios.post(`${SERVER}/editorial/add-meeting/${pk}/`, data, configAuthToken())
        .then((res) => {
            dispatch(createMessage({registerFile: 'Información Almacenada Correctamente'}));
            dispatch({ type: GET_REQUEST_EDITORIAL, payload: res.data});
        })
        .catch((err) => {
            try{
                switch (err.response.status){
                    case 400:
                        if (err.response.data.date) dispatch(
                            returnErrors(err.response.data.date, 400));

                        if (err.response.data.status) dispatch(
                            returnErrors(err.response.data.status, 400));
                        break;

                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    case 500:
                        dispatch(returnErrors(
                            'Verifique el archivo, no sé pudo guardar la información', 500));
                        break;

                    default:
                        console.log(err.response);
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });


}

export default add_meeting;
