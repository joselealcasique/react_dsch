import axios from 'axios';
import { returnErrors, createMessage } from '../messages';
import {
    SERVER_CONNECTION_REFUSED, AUTH_ERROR, SAVE_FAILED_FILE_PRODUCTION, SAVE_SUCCESS_FILE_PRODUCTION
} from "../types";

import { SERVER } from '../server';
import { configAuthTokenFiles } from '../configAuthToken';


export const set_file_data_production = (data) => (dispatch) => {

    axios.post(`${SERVER}/production/file/`, data, configAuthTokenFiles())
        .then((res) => {
            dispatch(createMessage({registerFile: 'Información Almacenada Correctamente'}));
            dispatch({ type: SAVE_SUCCESS_FILE_PRODUCTION});
        })
        .catch((err) => {
            try{
                switch (err.response.status){
                    case 400:
                        dispatch({ type: SAVE_FAILED_FILE_PRODUCTION});

                        if (err.response.data.errors) dispatch(
                            returnErrors(err.response.data.errors, 400));
                        else dispatch(createMessage({detailFile: err.response.data.file}));
                        break;

                    case 406:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 406));
                        break;

                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    case 500:
                        dispatch(returnErrors(
                            'Verifique el archivo, no sé pudo guardar la información', 500));
                        break;

                    default:
                        console.log(err.response);
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default set_file_data_production;
