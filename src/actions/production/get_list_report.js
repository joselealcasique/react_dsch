import axios from 'axios';
import {
    SERVER_CONNECTION_REFUSED,
    AUTH_ERROR,
    GET_LIST_TEACHER_TRAINING,
    GET_LIST_EXCHANGE_ACTIVITY,
    GET_LIST_ACADEMIC_EVENT,
    GET_LIST_ACADEMIC_COLLABORATION,
    GET_LIST_PROJECT_COLLABORATION,
    GET_LIST_MAGAZINE_PUBLICATIONS,
    GET_LIST_ELECTRONIC_JOURNALS,
    GET_LIST_NEWSPAPER_PUBLICATION,
    GET_LIST_PUBLISHED_BOOKS,
    GET_LIST_CHAPTERS_BOOKS,
    GET_LIST_REVIEWS_BOOKS,
    GET_LIST_PUBLISHED_LECTURES,
    GET_LIST_UNPUBLISHED_LECTURES,
    GET_LIST_RESEARCH_PROJECTS
} from '../types';
import {createMessage, returnErrors} from '../messages';
import { configAuthToken } from '../configAuthToken';
import { SERVER } from '../server';


export const get_list_report = (departament, report, initial, final, option) => (dispatch) => {

    axios.get(`${SERVER}/production/list/${departament}/${report}/${initial}/${final}/`, configAuthToken())
        .then((res) => {

            switch (parseInt(option)) {

                case 1:
                    dispatch({ type: GET_LIST_TEACHER_TRAINING, payload: res.data, load: true});
                    break;

                case 2:
                    dispatch({ type: GET_LIST_EXCHANGE_ACTIVITY, payload: res.data, load: true});
                    break;

                case 3:
                    dispatch({ type: GET_LIST_ACADEMIC_EVENT, payload: res.data, load: true});
                    break;

                case 4:
                    dispatch({ type: GET_LIST_ACADEMIC_COLLABORATION, payload: res.data, load: true});
                    break;

                case 5:
                    dispatch({ type: GET_LIST_PROJECT_COLLABORATION, payload: res.data, load: true});
                    break;

                case 6:
                    dispatch({ type: GET_LIST_MAGAZINE_PUBLICATIONS, payload: res.data, load: true});
                    break;

                case 7:
                    dispatch({ type: GET_LIST_ELECTRONIC_JOURNALS, payload: res.data, load: true});
                    break;

                case 8:
                    dispatch({ type: GET_LIST_NEWSPAPER_PUBLICATION, payload: res.data, load: true});
                    break;

                case 9:
                    dispatch({ type: GET_LIST_PUBLISHED_BOOKS, payload: res.data, load: true});
                    break;

                case 10:
                    dispatch({ type: GET_LIST_CHAPTERS_BOOKS, payload: res.data, load: true});
                    break;

                case 11:
                    dispatch({ type: GET_LIST_REVIEWS_BOOKS, payload: res.data, load: true});
                    break;

                case 12:
                    dispatch({ type: GET_LIST_PUBLISHED_LECTURES, payload: res.data, load: true});
                    break;

                case 13:
                    dispatch({ type: GET_LIST_UNPUBLISHED_LECTURES, payload: res.data, load: true});
                    break;

                case 14:
                    dispatch({ type: GET_LIST_RESEARCH_PROJECTS, payload: res.data, load: true});
                    break;

                default:
                    console.log(res);

            }
        })
        .catch((err) => {
            try {
                switch (err.response.status){

                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break

                    default:
                        console.log(err.response);
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default get_list_report;
