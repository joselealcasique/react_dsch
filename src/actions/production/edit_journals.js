import axios from 'axios';
import { returnErrors, createMessage } from '../messages';
import { SERVER_CONNECTION_REFUSED, AUTH_ERROR, EDIT_PRODUCTION_ELECTRONIC_JOURNALS } from "../types";

import { SERVER } from '../server';
import { configAuthTokenFiles } from '../configAuthToken';


export const edit_journals = (pk, data) => (dispatch) => {

    axios.put(`${SERVER}/production/journals/edit/${pk}/`, data, configAuthTokenFiles())
        .then((res) => {
            alert('Para ver los cambios hechos, aplique el filtro de búsqueda nuevamente');
            dispatch({ type: EDIT_PRODUCTION_ELECTRONIC_JOURNALS});
        })
        .catch((err) => {
            try{
                switch (err.response.status){
                    case 406:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 406));
                        break;

                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    case 500:
                        dispatch(returnErrors(
                            'Verifique el archivo, no sé pudo guardar la información', 500));
                        break;

                    default:
                        console.log(err.response);
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default edit_journals;
