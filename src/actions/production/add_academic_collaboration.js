import axios from 'axios';
import { returnErrors, createMessage } from '../messages';
import { SERVER_CONNECTION_REFUSED, AUTH_ERROR, ADD_PRODUCTION_ACADEMIC_COLLABORATION } from "../types";

import { SERVER } from '../server';
import { configAuthTokenFiles } from '../configAuthToken';


export const add_academic_collaboration = (data) => (dispatch) => {

    axios.post(`${SERVER}/production/academic-collaboration/add/`, data, configAuthTokenFiles())
        .then((res) => {
            dispatch(createMessage({registerFile: res.data.detail}));
            dispatch({ type: ADD_PRODUCTION_ACADEMIC_COLLABORATION });
        })
        .catch((err) => {
            try{
                switch (err.response.status){
                    case 400:
                        if (err.response.data.user) dispatch(
                            returnErrors("Nombre de profesor: "+err.response.data.user, 400));

                        if (err.response.data.name) dispatch(
                            returnErrors("Nombre de Red Académica: "+err.response.data.name, 400));

                        if (err.response.data.institutions) dispatch(
                            returnErrors("Instituciones: "+err.response.data.institutions, 400));

                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 400));
                        break;

                    case 406:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 406));
                        break;

                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    case 500:
                        dispatch(returnErrors(
                            'Verifique el archivo, no sé pudo guardar la información', 500));
                        break;

                    default:
                        console.log(err.response);
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default add_academic_collaboration;
