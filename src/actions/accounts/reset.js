import axios from "axios";
import { createMessage, returnErrors } from '../messages';
import {AUTH_ERROR, RESET_FAIL, RESET_SUCCESS, SERVER_CONNECTION_REFUSED} from '../types';
import { configAuthToken } from '../configAuthToken';
import { SERVER } from '../server';


export const resetPassword = (number, password) => (dispatch) => {
    const data = JSON.stringify({number, password});

    axios.patch(`${SERVER}/authentication/reset-password/`, data, configAuthToken())
        .then((res) => {
            dispatch(createMessage({ PasswordRestComplete: 'Contraseña Actualizada' }));
            dispatch({ type: RESET_SUCCESS, payload: res.data });
        })
        .catch((err) => {
            try{
                switch (err.response.status){
                    case 400:
                        if (err.response.data.number) dispatch(
                            returnErrors('No. Económico: '+err.response.data.number, 400));
                        if (err.response.data.password) dispatch(
                            returnErrors('Contraseña: '+err.response.data.password, 400));
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 400));
                        dispatch({type: RESET_FAIL});
                        break;

                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    case 500:
                        dispatch(createMessage({detail: 'Error'}));
                        break;

                    default:
                        console.log(err.response);
                }
            } catch (e){
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
};

export default resetPassword;
