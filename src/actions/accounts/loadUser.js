import axios from 'axios';
import {USER_LOADING, USER_LOADED, AUTH_ERROR, AUTH_RESET, SERVER_CONNECTION_REFUSED} from '../types';
import { createMessage, returnErrors } from '../messages';
import { configAuthToken } from '../configAuthToken';
import { SERVER } from '../server';


export const loadUser = () => (dispatch) => {
    dispatch({type: USER_LOADING});

    axios.get(`${SERVER}/authentication/load-user/`, configAuthToken())
        .then((res) => {

            if (res.status === 202){
                dispatch(createMessage({userLoaded: 'Sesión Iniciada'}));
                dispatch({ type: USER_LOADED, payload: res.data });
            }
        })
        .catch((err) => {
            try {
                switch (err.response.status){
                    case 401:
                        if (err.response.data.message) dispatch(
                            createMessage({resetPassword: err.response.data.message}));
                            dispatch({ type: AUTH_RESET });

                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    default:
                        console.log(err.response);
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default loadUser;
