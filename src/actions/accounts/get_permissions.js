import axios from 'axios';
import { createMessage, returnErrors } from '../messages';
import {
    GET_PERMISSIONS, SERVER_CONNECTION_REFUSED, AUTH_ERROR
} from '../types';
import { SERVER } from '../server';
import { configAuthToken } from "../configAuthToken";


export const get_permissions = () => (dispatch) => {

    axios.get(`${SERVER}/authentication/permissions/users/`, configAuthToken())
        .then((res) => {
            dispatch({ type: GET_PERMISSIONS, payload: res.data});
        })
        .catch((err) => {
            try{
                switch (err.response.status){
                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    case 500:
                        dispatch(createMessage({detail: 'Error'}));
                        break;

                    default:
                        console.log(err.response);
                    }
            } catch (e){
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
};

export default get_permissions;


