import axios from 'axios';
import { createMessage, returnErrors } from '../messages';
import {
    GET_PERMISSIONS, SERVER_CONNECTION_REFUSED, AUTH_ERROR
} from '../types';
import { SERVER } from '../server';
import { configAuthToken } from "../configAuthToken";


export const clear_permissions = (number) => (dispatch) => {

    axios.delete(`${SERVER}/authentication/clear/permission/${number}/`, configAuthToken())
        .then((res) => {
            dispatch({ type: GET_PERMISSIONS, payload: res.data});
            dispatch(createMessage({
                PasswordRestComplete:'Permisos Eliminados Correctamente'}));
        })
        .catch((err) => {
            try{
                switch (err.response.status){
                    case 400:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 400));
                        break;

                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    case 500:
                        dispatch(createMessage({detail: 'Error'}));
                        break;

                    default:
                        console.log(err.response);
                    }
            } catch (e){
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
};

export default clear_permissions;


