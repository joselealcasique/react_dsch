import { AUTH_ERROR } from './types';
import { returnErrors } from './messages';


export const Unauthorized = (status) => (dispatch) =>{
    if (status === 401){
        dispatch({type: AUTH_ERROR});
        dispatch(returnErrors('La Sesión Termino', status));
        return false;
    } else {
        return true;
    }
};