import axios from 'axios';
import {FINALIZED_ADJUSTMENTS_MODIFICATION, SERVER_CONNECTION_REFUSED, AUTH_ERROR} from '../types';
import {createMessage, returnErrors} from '../messages';
import { SERVER } from '../server';
import { configAuthToken } from '../configAuthToken';

export const finalized_modification = (pk) => (dispatch) => {

    axios.delete(`${SERVER}/adjustments/modification/${pk}/finalized/`, configAuthToken())
        .then((res) => {
            dispatch({ type: FINALIZED_ADJUSTMENTS_MODIFICATION, payload: res.data });
        })
        .catch((err) => {
            try{
                switch (err.response.status){
                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    default:
                        console.log(err.response);
                }

            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default finalized_modification;
