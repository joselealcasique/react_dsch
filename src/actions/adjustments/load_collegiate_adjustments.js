import axios from 'axios';
import {LOAD_COLLEGIATE_ADJUSTMENTS, SERVER_CONNECTION_REFUSED, AUTH_ERROR} from '../types';
import {createMessage, returnErrors} from '../messages';
import { SERVER } from '../server';
import { configAuthToken } from '../configAuthToken';

export const load_collegiate_adjustments = () => (dispatch) => {

    axios.get(`${SERVER}/adjustments/collegiate/`, configAuthToken())
        .then((res) => {
            dispatch({ type: LOAD_COLLEGIATE_ADJUSTMENTS, payload: res.data });
        })
        .catch((err) => {
            try{
                switch (err.response.status){
                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    default:
                        console.log(err.response);
                }

            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default load_collegiate_adjustments;
