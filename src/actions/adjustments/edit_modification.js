import axios from 'axios';
import { returnErrors, createMessage } from '../messages';
import { SERVER } from '../server';
import { configAuthTokenFiles } from '../configAuthToken';
import { EDIT_ADJUSTMENTS_MODIFICATION, SERVER_CONNECTION_REFUSED, AUTH_ERROR } from "../types";


export const edit_modification = (pk, data) => (dispatch) => {

    axios.put(`${SERVER}/adjustments/modification/${pk}/edit/`, data, configAuthTokenFiles())
        .then((res) => {
            console.log(res.data);
            dispatch({
                type: EDIT_ADJUSTMENTS_MODIFICATION,
                payload: res.data
            });
            alert("Información Almacenada Correctamente");
        })
        .catch((err) => {
            try{
                switch (err.response.status){

                    case 500:
                        dispatch(returnErrors(
                            'Verifique el archivo, no sé pudo guardar la información', 500));
                        break;

                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break
                    case 406:
                        dispatch(createMessage({detailFile: err.response.data.file}));
                        break;

                    default:
                        console.log(err.response);
                }

            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
                return false;
            }
        });
}

export default edit_modification;
