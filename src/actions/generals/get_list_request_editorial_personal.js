import axios from 'axios';
import {
    GET_LIST_REQUEST_EDITORIAL_PERSONAL, SERVER_CONNECTION_REFUSED, AUTH_ERROR
} from '../types';
import {createMessage, returnErrors} from '../messages';
import { configAuthToken } from '../configAuthToken';
import { SERVER } from '../server';


export const get_list_request_editorial_personal = (year, status) => (dispatch) => {

    axios.get(`${SERVER}/editorial/list-request-personal/`, configAuthToken())
        .then((res) => {
            let state = false;
            if (res.data.results.length > 0){
                state = true;
            }
            dispatch({
                type: GET_LIST_REQUEST_EDITORIAL_PERSONAL,
                payload: res.data.results,
                load: state
            });


        })
        .catch((err) => {
            try {
                switch (err.response.status){
                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break

                    default:
                        console.log(err.response);
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default get_list_request_editorial_personal;
