import axios from 'axios';
import {
    AUTH_ERROR, SERVER_CONNECTION_REFUSED
} from '../types';
import {createMessage, returnErrors} from '../messages';
import { configAuthToken } from '../configAuthToken';
import { SERVER } from '../server';


export const send_project_investigation = (pk) => (dispatch) => {
    console.log(configAuthToken())

    axios.put(`${SERVER}/investigation/send/project/${pk}/`, configAuthToken())
        .then((res) => {
            dispatch(createMessage({registerFile: res.data.detail}));
        })
        .catch((err) => {
            try {
                switch (err.response.status) {
                    case 400:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 400));
                        break;

                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break

                    default:
                        console.log(err.response.data)
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default send_project_investigation;
