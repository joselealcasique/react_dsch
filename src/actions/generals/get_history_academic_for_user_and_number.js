import axios from 'axios';
import {
    GET_HISTORY_FOR_USER_FOR_NUMBER, FAIL_HISTORY_FOR_USER_FORM_NUMBER,
    SERVER_CONNECTION_REFUSED, AUTH_ERROR
} from '../types';
import {createMessage} from '../messages';
import { configAuthToken } from '../configAuthToken';
import { SERVER } from '../server';


export const get_history_academic_for_user_and_for_number = (year) => (dispatch) => {

    axios.get(
        `${SERVER}/academic/list-history-academic-for-user-and-for-year/${year}/`,
        configAuthToken())
        .then((res) => {
            dispatch({ payload: res.data.results, type: GET_HISTORY_FOR_USER_FOR_NUMBER });
        })
        .catch((err) => {

            try {
                switch (err.response.status){
                    case 401:
                        dispatch({type: AUTH_ERROR});
                        break
                    case 404:
                        dispatch({type: FAIL_HISTORY_FOR_USER_FORM_NUMBER})
                        break;
                    default:
                        console.log(err.response)
                }

            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({
                        serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default get_history_academic_for_user_and_for_number;
