
export const configAuthToken = () => {
    const token = localStorage.getItem('access');
    const config = {
        'headers': {
            'Content-Type': 'application/json'}};

    if (token) {
        config.headers['Authorization'] = `Bearer ${token}`;
    }

    return config;
};


export const configAuthTokenFiles = () => {

    const token = localStorage.getItem('access');
    const config = {
        'headers': {
            'Content-Type': 'multipart/form-data'}}

    if (token) {
        config.headers['Authorization'] = `Bearer ${token}`;
    }
    return config;
};

