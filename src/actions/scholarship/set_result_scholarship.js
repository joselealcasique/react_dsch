import axios from 'axios';
import { returnErrors, createMessage } from '../messages';
import {SERVER_CONNECTION_REFUSED, REGISTER_FiNALIZED_SCHOLARSHIP, AUTH_ERROR} from "../types";
import { SERVER } from '../server';
import {configAuthToken} from '../configAuthToken';


export const set_result_scholarship = (pk, scholarship, result) => (dispatch) => {
    const data = JSON.stringify({scholarship, result});

    axios.put(`${SERVER}/scholarship/set-result-scholarship/${pk}/`, data, configAuthToken())
        .then((res) => {
            dispatch(createMessage({registerFile: 'Actualización Correcta'}))
            dispatch({
                type: REGISTER_FiNALIZED_SCHOLARSHIP,
                payload: res.data});
        })
        .catch((err) => {
            try{
                switch (err.response.status){
                    case 400:
                        if (err.response.data.scholarship) dispatch(
                            returnErrors(err.response.data.scholarship, err.response.status));

                        if (err.response.data.result) dispatch(
                            returnErrors(err.response.data.result, err.response.status));
                        break;

                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    case 404:
                        dispatch(createMessage({NotFound: 'NO HAY COINCIDENCIAS'}));
                        break;
                    case 500:
                        break;

                    default:
                        console.log(err.response);
                }

            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default set_result_scholarship;
