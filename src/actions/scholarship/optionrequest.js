import axios from 'axios';
import {
    GET_SCHOLARSHIP_OPTIONS, FAIL_SCHOLARSHIP_OPTIONS, SERVER_CONNECTION_REFUSED
} from '../types';
import {createMessage, returnErrors} from '../messages';
import { SERVER } from '../server';
import { configAuthToken } from '../configAuthToken';

export const get_option_request = () => (dispatch) => {
    axios.get(`${SERVER}/scholarship/list-option-requests/`, configAuthToken())
        .then((res) => {
            dispatch({
                type: GET_SCHOLARSHIP_OPTIONS,
                payload: res.data.results
            });
        })
        .catch((err) => {
            try{
                dispatch(returnErrors(err.response.data, err.response.status));
                dispatch({ type: FAIL_SCHOLARSHIP_OPTIONS });

            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default get_option_request;
