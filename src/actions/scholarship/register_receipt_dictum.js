import axios from 'axios';
import { returnErrors, createMessage } from '../messages';
import {SERVER_CONNECTION_REFUSED, REGISTER_RECEIPT_DICTUM_SCHOLARSHIP, AUTH_ERROR} from "../types";
import { SERVER } from '../server';
import { configAuthTokenFiles } from '../configAuthToken';


export const register_receipt_dictum = (data, pk) => (dispatch) => {

    axios.put(`${SERVER}/scholarship/receipt-dictum-scholarship/${pk}/`, data, configAuthTokenFiles())
        .then((res) => {
            dispatch(createMessage({registerFile: 'Información Almacenada Correctamente'}));
            dispatch({ type: REGISTER_RECEIPT_DICTUM_SCHOLARSHIP, payload: res.data});
        })
        .catch((err) => {
            try{
                switch (err.response.status){
                    case 400:
                        if (err.response.data.errors) dispatch(
                            returnErrors(err.response.data.errors, 400));
                        else dispatch(createMessage({detailFile: err.response.data.file}));
                        break;

                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    case 500:
                        dispatch(returnErrors(
                            'Verifique el archivo, no sé pudo guardar la información', 500));
                        break;

                    default:
                        console.log(err.response);
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default register_receipt_dictum;
