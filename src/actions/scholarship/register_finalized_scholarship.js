import axios from 'axios';
import { returnErrors, createMessage } from '../messages';
import {SERVER_CONNECTION_REFUSED, REGISTER_FiNALIZED_SCHOLARSHIP, AUTH_ERROR} from "../types";
import { SERVER } from '../server';
import {configAuthToken} from '../configAuthToken';


export const register_finalized_scholarship = (pk) => (dispatch) => {
    const data = {}

    axios.put(`${SERVER}/scholarship/finalized-scholarship/${pk}/`, data, configAuthToken())
        .then((res) => {
            if (res.data.finalized === true){
                dispatch(createMessage({registerFile: 'Solicitud Finalizada'}))
            } else {
                dispatch(createMessage({registerFile: 'La Solicitud es Editable'}))
            }
            dispatch({
                type: REGISTER_FiNALIZED_SCHOLARSHIP,
                payload: res.data});
        })
        .catch((err) => {
            try{
                switch (err.response.status){
                    case 400:
                        dispatch(returnErrors(err.response.data, err.response.status));
                        break;

                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    case 404:
                        if (err.response.status === 404) dispatch(
                            createMessage({NotFound: 'NO HAY COINCIDENCIAS'}));
                        break;

                    default:
                        console.log(err.response);
                }

            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default register_finalized_scholarship;
