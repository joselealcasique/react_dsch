import axios from 'axios';
import {
    GET_LIST_EXTENSIONS_TO_PROJECT, SERVER_CONNECTION_REFUSED, AUTH_ERROR
} from '../types';
import {createMessage, returnErrors} from '../messages';
import { configAuthToken } from '../configAuthToken';
import { SERVER } from '../server';


export const list_project_investigation_commission = (pk) => (dispatch) => {

    axios.get(`${SERVER}/investigation/extension/list/${pk}/`, configAuthToken())
        .then((res) => {
            dispatch({
                type: GET_LIST_EXTENSIONS_TO_PROJECT,
                payload: res.data.results,
            });
        })
        .catch((err) => {
            try {
                switch (err.response.status){
                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break

                    default:
                        console.log(err.response);
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default list_project_investigation_commission;
