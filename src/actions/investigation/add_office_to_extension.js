import axios from 'axios';
import { returnErrors, createMessage } from '../messages';
import {SERVER_CONNECTION_REFUSED, AUTH_ERROR, ADD_OFFICE_TO_EXTENSION_PROJECT} from "../types";
import { SERVER } from '../server';
import { configAuthTokenFiles } from '../configAuthToken';


export const add_office_to_extension = (data, pk) => (dispatch) => {

    axios.put(`${SERVER}/investigation/office/project/add/${pk}/`, data, configAuthTokenFiles())
        .then((res) => {
            alert('Informe Almacenado Correctamente');
            dispatch({ type: ADD_OFFICE_TO_EXTENSION_PROJECT, payload: res.data});
        })
        .catch((err) => {
            try{
                switch (err.response.status){
                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    default:
                        console.log(err.response);
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });


}

export default add_office_to_extension;
