import axios from 'axios';
import { returnErrors, createMessage } from '../messages';
import { SERVER_CONNECTION_REFUSED, AUTH_ERROR, CREATE_PRESUPUESTAL_INVESTIGATION } from "../types";

import { SERVER } from '../server';
import { configAuthTokenFiles } from '../configAuthToken';


export const add_presupuestal = (pk, data) => (dispatch) => {

    axios.post(`${SERVER}/investigation/assign/budget/${pk}/`, data, configAuthTokenFiles())
        .then((res) => {
            alert('Asignación presupuestal registrado');

            dispatch({
                type: CREATE_PRESUPUESTAL_INVESTIGATION,
                payload: res.data
            });
        })
        .catch((err) => {
            try{
                switch (err.response.status){
                    case 400:
                        if (err.response.data.code) dispatch(
                            returnErrors("Estructura: "+err.response.data.code, 400));

                        if (err.response.data.sub_code) dispatch(
                            returnErrors(
                                "Partida Sub especifica: "+err.response.data.sub_code, 400));

                        if (err.response.data.name) dispatch(
                            returnErrors("Nombre de Partida: "+err.response.data.name, 400));

                        if (err.response.data.amount) dispatch(
                            returnErrors("Monto: "+err.response.data.amount, 400))

                        if (err.response.data.date) dispatch(
                            returnErrors("Fecha de asignación: "+err.response.data.date, 400))

                        if (err.response.data.assign) dispatch(
                            returnErrors(
                                "Forma de Asignación: "+err.response.data.assign, 400))

                        if (err.response.data.file) dispatch(
                            returnErrors(
                                "Documentación de asignación: "+err.response.data.file, 400))

                        if (err.response.data.observations) dispatch(
                            returnErrors(
                                "Observaciones: "+err.response.data.observations, 400))

                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 400));
                        break;

                    case 401:
                        if (err.response.data.detail) dispatch(
                            returnErrors(err.response.data.detail, 401));
                            dispatch({ type: AUTH_ERROR });
                        break;

                    case 500:
                        dispatch(returnErrors(
                            'Verifique el archivo, no sé pudo guardar la información', 500));
                        break;

                    default:
                        console.log(err.response);
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    dispatch({type: SERVER_CONNECTION_REFUSED});
                    dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
}

export default add_presupuestal;
