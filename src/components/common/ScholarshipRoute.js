import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';


const ScholarshipRoute = ({ component: Component, permissions, ...rest }) => (
    <Route
        {...rest}

        render={(props) =>{

            try {
                if (
                    permissions.includes('authentication.admin_scholarship') |
                    permissions.includes('authentication.admin_root')
                ){
                    return <Component {...props} />
                } else{
                    return <Redirect to='/' />
                }
            } catch (e) {
                return <Redirect to='/' />
            }

        } }
    />
);

const mapStateToProps = (state) => ({
    permissions: state.auth.permissions
});

export default connect(mapStateToProps, {})(ScholarshipRoute);

