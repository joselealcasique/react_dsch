import React, { Component } from "react";
import PropTypes from "prop-types";

import CardUser from "./CardUser";


export class Modules extends Component {

    static propTypes = {
        privileges: PropTypes.object.isRequired,
    }

    render() {

        if (this.props.privileges.load){
            return (
                <section className="col-12 py-3">
                    <div className="filter-section-dsch-multi-form">

                        <div className="col-6 mb-3">
                            <CardUser
                                module={'Módulo de Docencia'}
                                option={1}
                                text={
                                    "Usuario encargado de realizar la carga académica."
                                }
                                privileges={
                                    this.props.privileges.list.permission_academic_module
                                }
                            />
                        </div>

                        <div className="col-6 mb-3">
                            <CardUser
                                module={'Módulo de Adecuaciones'}
                                option={2}
                                text={
                                    "Usuario encargado de registrar las modificaciones " +
                                    "en el plan de estudios."
                                }
                                privileges={
                                    this.props.privileges.list.permission_adjustments_module
                                }
                            />
                        </div>
                    </div>

                    <div className="filter-section-dsch-multi-form">
                        <div className="col-6 mb-3">
                            <CardUser
                                module={'Módulo de Concursos'}
                                option={3}
                                text={
                                    "Usuario encargado de registrar y dar continuidad " +
                                    "a los concursos " +
                                    "(Oposición, Cátedra, Curricular, etc.)."
                                }
                                privileges={
                                    this.props.privileges.list.permission_concourse_module
                                }
                            />
                        </div>

                        <div className="col-6 mb-3">
                            <CardUser
                                module={'Módulo de Editorial'}
                                option={4}
                                text={
                                    "Usuario encargado de registrar y dar continuidad " +
                                    "a las solicitudes enviadas a editorial."
                                }
                                privileges={
                                    this.props.privileges.list.permission_editorial_module
                                }
                            />
                        </div>
                    </div>

                    <div className="filter-section-dsch-multi-form">
                        <div className="col-6 mb-2">
                            <CardUser
                                module={'Módulo de Producción Académica'}
                                option={5}
                                text={
                                    "Usuario encargado de registrar las actividades en " +
                                    "donde participen los profesores " +
                                    "(revistas, periódicos, etc.)."
                                }
                                privileges={
                                    this.props.privileges.list.permission_production_module
                                }
                            />
                        </div>
                        <div className="col-6 mb-2">
                            <CardUser
                                module={'Módulo de Becas'}
                                option={6}
                                text={
                                    "Usuario encargado de registrar y dar continuidad " +
                                    "a las becas solicitadas por profesores. "
                                }
                                privileges={
                                    this.props.privileges.list.permission_scholarship_module
                                }
                            />
                        </div>
                    </div>
                </section>
            )
        } else {
            return (
                <section className="py-3">
                    <section
                        className="col-8 alert alert-warning mx-auto mt-5 py-5 text-center">
                        <h1><strong>loading...</strong></h1>
                    </section>
                </section>
            )
        }
    }

}


export default Modules;
