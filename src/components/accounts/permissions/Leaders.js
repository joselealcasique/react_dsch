import React, { Component } from "react";
import PropTypes from "prop-types";

import CardUser from "./CardUser";


export class Leaders extends Component {

    static propTypes = {
        privileges: PropTypes.object.isRequired,
    }

    render() {

        if (this.props.privileges.load){
            return (
                <section className="col-12 py-3">
                    <div className="filter-section-dsch-multi-form">
                        <div className="col-6 mb-3">
                            <CardUser
                                module={'Depto. Humanidades'}
                                option={8}
                                text={
                                    "Jefe del Departamento de Humanidades, podrá revisar " +
                                    "la carga académica de su departamento y los proyectos " +
                                    "de investigación."
                                }
                                privileges={
                                    this.props.privileges.list.permission_leader_departament_humanities
                                }
                            />
                        </div>

                        <div className="col-6 mb-3">
                            <CardUser
                                module={'Depto. Ciencias Sociales'}
                                option={9}
                                text={
                                    "Jefe del Departamento de Ciencias Sociales, podrá revisar " +
                                    "la carga académica de su departamento y los proyectos " +
                                    "de investigación."
                                }
                                privileges={
                                    this.props.privileges.list.permission_leader_departament_social_sciences
                                }
                            />
                        </div>
                    </div>

                    <div className="filter-section-dsch-multi-form">
                        <div className="col-6 mb-2">
                            <CardUser
                                module={'Depto. Estudios Institucionales'}
                                option={10}
                                text={
                                    "Jefe del Departamento de Estudios Institucionales, " +
                                    "podrá revisar  la carga académica de su departamento " +
                                    "y los proyectos de investigación."
                                }
                                privileges={
                                    this.props.privileges.list.permission_leader_departament_institutional_studies
                                }
                            />
                        </div>
                    </div>
                </section>
            )
        } else {
            return (
                <section className="py-3">
                    <section
                        className="col-8 alert alert-warning mx-auto mt-5 py-5 text-center">
                        <h1><strong>loading...</strong></h1>
                    </section>
                </section>
            )
        }
    }

}


export default Leaders;
