import React, { Component } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import SelectInitial from "../../Investigation/reports/SelectInitial";
import SelectFinal from "../../Investigation/reports/SelectFinal";
import {get_report_sabbatical} from "../../../actions/sabbatical/get_report_sabbatical";


export class ReportSabbatical extends Component {

    state = {
        initial: new Date().getFullYear(),
        final: new Date().getFullYear(),
    }

    static propTypes = {
        user: PropTypes.object,
        get_report_sabbatical: PropTypes.func.isRequired,
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});

    onSubmitReportSabbatical = (e) => {
        e.preventDefault();
        this.props.get_report_sabbatical(this.state.initial, this.state.final)
    }

    render() {
        return (
            <div className="modal fade" id="create-report-sabbatical" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                <div className="modal-dialog modal-lg" role="document">
                    <div className="modal-content">
                        <div className="modal-header bg-form-presupuestal">
                            <h5 className="modal-title" id="exampleModalLongTitle">
                                Generar Reporte sobre Periodos Sabáticos
                            </h5>
                            <button type="button" className="close"
                                    data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body alert-warning">
                            <form className="col-12 mx-auto text-center" id="report-sabbatical"
                                  onSubmit={this.onSubmitReportSabbatical}>
                                <section className="filter-section-dsch-multi-form pt-2 pb-4">
                                    <div className="col-1"></div>
                                    <div className="col-4">
                                        <h6 className="text-left pl-1">
                                            <strong>
                                                Elegir Fecha de Inicio
                                            </strong>
                                        </h6>
                                        <SelectInitial
                                            select_initial={this.state.initial.toString()}
                                            onChange={this.onChange}
                                        />
                                    </div>

                                    <div className="col-4">
                                        <h6 className="text-left pl-1">
                                            <strong>
                                                Elegir Fecha de Término
                                            </strong>
                                        </h6>
                                        <SelectFinal
                                            select_final={this.state.final.toString()}
                                            onChange={this.onChange}
                                        />
                                    </div>

                                    <div className="col-2 pt-4">
                                        <button className="btn btn-warning"
                                                onClick={this.onSubmitReportSabbatical}>
                                            Enviar
                                        </button>
                                    </div>
                                </section>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

const mapStateToProps = (state) => ({
    user: state.auth.user
});

export default connect(mapStateToProps, {get_report_sabbatical} )(ReportSabbatical);