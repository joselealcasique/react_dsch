import React, { Component } from "react";
import {Link} from "react-router-dom";

import FormSabbatical from "./components/FormSabbatical";


class CreateSabbatical extends Component {

    RedirectMyListSabbatical = () => {
        return this.props.history.push('/sabbatical');
    }

    render() {
        return (
            <section className="col-12 mx-auto">
                <section className="filter-section-dsch-multi-form alert alert-warning mt-4">
                    <div className="col-8 mx-auto text-right pr-5 pt-2">
                        <h4><strong>Solicitud de Periodo Sabático</strong></h4>
                    </div>
                    <div className="col-4 mx-auto">
                        <Link to={
                            { pathname: '/sabbatical', state:{pk: 0, edit: false} }
                        } >
                            <button className="btn-dsch float-right px-1">
                                Regresar
                            </button>
                        </Link>
                    </div>
                </section>

                <section className="card col-10 mx-auto mt-2">
                    <FormSabbatical />
                </section>
            </section>
        )
    }

}

export default CreateSabbatical;