import React, { Component } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";



class SelectMonth extends Component {

    static propTypes = {
        months: PropTypes.array.isRequired,
        onChange: PropTypes.func.isRequired,
        select_month: PropTypes.number,
    }

    render() {
        return (
            <select className="form-control" name="months" onChange={this.props.onChange}
                    value={this.props.select_month}>
                {
                    this.props.months.map((obj, index) => (
                        <option key={index} value={obj}>
                            {
                                obj > 1
                                    ?
                                    'Periodo de ' + obj.toString() + ' meses'
                                    :
                                    'Periodo de un mes'
                            }
                        </option>
                    ))
                }
            </select>
        )
    }

}

const mapStateToProps = (state) => ({
    months: state.sabbatical.months
});

export default connect(mapStateToProps, null)(SelectMonth);
