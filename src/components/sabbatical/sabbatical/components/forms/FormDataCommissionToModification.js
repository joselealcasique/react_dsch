import React, { Component } from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";

import {ConfigDateSave} from "../../../../FormatDate";
import {
    send_data_approval_sabbatical
} from "../../../../../actions/sabbatical/send_data_approval_sabbatical";


export class FormDataCommissionToModification extends Component {

    state = {
        register: '',
        session: '',
        agreement: '',
        created: '',
        date_final: '',
    }

    static propTypes = {
        pk: PropTypes.number.isRequired,
        send_data_approval_sabbatical: PropTypes.func.isRequired,
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});

    onSubmitDataAdditional = (e) => {
        e.preventDefault();
        let data = null;

        if (this.state.date_final !== ''){
            data = JSON.stringify({
                register: this.state.register,
                session: this.state.session,
                agreement: this.state.agreement,
                created: ConfigDateSave(this.state.created),
                date_final: ConfigDateSave(this.state.date_final),
            });
        }else {
            data = JSON.stringify({
                register: this.state.register,
                session: this.state.session,
                agreement: this.state.agreement,
                created: ConfigDateSave(this.state.created),
            });

        }

        this.props.send_data_approval_sabbatical(this.props.pk, data);
    }

    render() {
        return (
            <div className="modal fade" tabIndex="-1" role="dialog" id="modal-data-commission"
                 aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header bg-form-presupuestal">
                            <h5 className="modal-title" id="exampleModalLabel">
                                <strong>Información de Consejo de Divisional</strong>
                            </h5>
                            <button type="button" className="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <form onSubmit={this.onSubmitDataAdditional}>
                                <div className="col-11 mx-auto alert alert-danger">
                                    <h6><strong>Nueva fecha de finalización</strong></h6>
                                    <input type="date" className="form-control" name="date_final"
                                           value={this.state.date_final} onChange={this.onChange} />
                                    <small>
                                        <strong>
                                            Usar en caso de querer finalizar antes el periodo sabático.
                                        </strong>
                                    </small>
                                </div>
                                <div className="col-11 mx-auto">
                                    <h6><strong>No. de Registro de periodo sabático</strong></h6>
                                    <input type="text" className="form-control"
                                           required name="register"
                                           value={this.state.register} onChange={this.onChange} />
                                </div>

                                <div className="col-11 mx-auto">
                                    <h6><strong>No. De Sesión en la que fue aprobado</strong></h6>
                                    <input type="text" className="form-control"
                                           required name="session"
                                           value={this.state.session} onChange={this.onChange} />
                                </div>

                                <div className="col-11 mx-auto">
                                    <h6><strong>No. De Acuerdo con el que se aprobó</strong></h6>
                                    <input type="text" className="form-control"
                                           required name="agreement"
                                           value={this.state.agreement} onChange={this.onChange} />
                                </div>

                                <div className="col-11 mx-auto mb-3">
                                    <h6><strong>Fecha de aprobación</strong></h6>
                                    <input type="date" className="form-control"
                                           required name="created"
                                           value={this.state.created} onChange={this.onChange} />
                                </div>
                                <button className="btn btn-dark">Guardar Información</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

const mapStateToProps = (state) => ({});

export default connect(
    mapStateToProps, { send_data_approval_sabbatical } )(FormDataCommissionToModification);