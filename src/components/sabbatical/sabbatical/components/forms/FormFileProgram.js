import React, { Component } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import { change_program } from "../../../../../actions/sabbatical/change_program";


export class FormFileProgram extends Component {

    state = {
        file: null,
        program: null,
        letter: null
    }

    static propTypes = {
        change_program: PropTypes.func.isRequired,
        sabbatical: PropTypes.object,
    }

    onChangeFile = (e) => this.setState({[e.target.name]: e.target.files[0]});

    onResetForm = () => {
        document.getElementById('form-change-program').reset();
    }

    onSubmit = (e) => {
        e.preventDefault();
        let data = new FormData()

        try {
            data.append('file', this.state.file, this.state.file.name);

        } catch (e){
            if (e.TypeError === undefined){
                //pass
            }
        }

        try {
            data.append('program', this.state.program, this.state.program.name);

        } catch (e){
            if (e.TypeError === undefined){
                //pass
            }
        }

        try {
            data.append('letter', this.state.letter, this.state.letter.name);

        } catch (e){
            if (e.TypeError === undefined){
                //pass
            }
        }

        if (this.state.file !== null || this.state.program !== null || this.state.letter !== null){
            this.props.change_program(this.props.sabbatical.id, data);

        } else {
            alert('No se modificará nada.');
        }
        this.onResetForm();
    }

    render() {
        return (
            <div className="modal fade " tabIndex="-1" role="dialog" id="modal-change-program"
                 aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header bg-form-presupuestal">
                            <h5 className="modal-title" id="exampleModalLabel">
                                Edición de programa de actividades académicas
                            </h5>
                            <button type="button" className="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form  id="form-change-program" onSubmit={this.onSubmit}>
                            <div className="modal-body">
                                <div className="col-11 mx-auto py-2">
                                    <h5><strong>Solicitud</strong></h5>
                                    <input type="file" className="form-control"
                                           accept="application/pdf" name="file"
                                           value={this.file}
                                           onChange={this.onChangeFile}/>
                                </div>
                                <div className="col-11 mx-auto py-2">
                                    <h5><strong>Programa de actividades</strong></h5>
                                    <input type="file" className="form-control"
                                           accept="application/pdf" name="program"
                                           value={this.program}
                                           onChange={this.onChangeFile}/>
                                </div>
                                <div className="col-11 mx-auto py-2">
                                    <h5><strong>Carta</strong></h5>
                                    <input type="file" className="form-control"
                                           accept="application/pdf" name="letter"
                                           value={this.letter}
                                           onChange={this.onChangeFile}/>
                                </div>
                            </div>
                            <div className="modal-footer bg-form-presupuestal">
                                <button type="button" className="btn btn-light"
                                        onClick={this.onResetForm}>
                                    Limpiar Formulario
                                </button>
                                <button type="submit" className="btn btn-dark">
                                    Enviar cambios
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }

}

const mapStateToProps = (state) => ({
    sabbatical: state.sabbatical.sabbatical
});

export default connect(mapStateToProps, { change_program } )(FormFileProgram);