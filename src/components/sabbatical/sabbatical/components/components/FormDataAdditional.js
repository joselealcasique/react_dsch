import React, { Component } from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";

import { ConfigDateSave } from "../../../../FormatDate";
import {
    send_data_approval_sabbatical
} from "../../../../../actions/sabbatical/send_data_approval_sabbatical";


export class FormDataAdditional extends Component {

    state = {
        register: '',
        session: '',
        agreement: '',
        created: '',
    }

    static propTypes = {
        pk: PropTypes.number.isRequired,
        send_data_approval_sabbatical: PropTypes.func.isRequired,
        onRedirectToListRequest: PropTypes.func.isRequired,
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});

    onSubmitDataAdditional = (e) => {
        e.preventDefault();
        let data = JSON.stringify({
            register: this.state.register,
            session: this.state.session,
            agreement: this.state.agreement,
            created: ConfigDateSave(this.state.created),
        })
        this.props.send_data_approval_sabbatical(this.props.pk, data);
        this.props.onRedirectToListRequest()
    }

    render() {

        return (
            <section className="text-center">
                <form onSubmit={this.onSubmitDataAdditional}>

                    <div className="filter-section-dsch-multi-form mb-3">
                        <div className="col-6">
                            <h6><strong>No. de registro de periodo sabático</strong></h6>
                            <input type="text" className="form-control" required name="register"
                                   value={this.state.register} onChange={this.onChange} />
                        </div>

                        <div className="col-6">
                            <h6><strong>No. de sesión en la que fue aprobado</strong></h6>
                            <input type="text" className="form-control" required name="session"
                                   value={this.state.session} onChange={this.onChange} />
                        </div>

                    </div>
                    <div className="filter-section-dsch-multi-form mb-3">
                        <div className="col-6">
                            <h6><strong>No. de acuerdo con el que se aprobó</strong></h6>
                            <input type="text" className="form-control" required name="agreement"
                                   value={this.state.agreement} onChange={this.onChange} />
                        </div>

                        <div className="col-6">
                            <h6><strong>Fecha de registro de profesor</strong></h6>
                            <input type="date" className="form-control" required name="created"
                                   value={this.state.created} onChange={this.onChange} />
                        </div>
                    </div>

                    <button className="btn btn-dark">Guardar Información</button>
                </form>
            </section>
        )
    }

}


const mapStateToProps = (state) => ({});

export default connect( mapStateToProps, { send_data_approval_sabbatical } )(FormDataAdditional);