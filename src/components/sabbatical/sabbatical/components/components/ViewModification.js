import React, { Component } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import { FormatDate } from "../../../../FormatDate";
import {SERVER} from "../../../../../actions/server";


export class ViewModification extends Component {

    static propTypes = {
        modification: PropTypes.object,
        sabbatical: PropTypes.object,
    }


    render() {
        return (
            <div className="modal fade " tabIndex="-1" role="dialog" id="view-modification"
                 aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header bg-form-presupuestal">
                            <h5 className="modal-title" id="exampleModalLabel">
                                <strong>Detalle de modificación en solicitud</strong>
                            </h5>
                            <button type="button" className="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body py-2">
                            {
                                this.props.modification !== null
                                    ?
                                    <section>
                                        <div className="alert alert-warning">
                                            <h6>
                                                <strong>Tipo de Solicitud: </strong>
                                                {this.props.modification.status}
                                            </h6>
                                        </div>

                                        <div className="alert alert-warning">
                                            <h6>
                                                <strong>Modificación de inicio: </strong>
                                                {
                                                    this.props.modification.date_initial
                                                        ?
                                                        FormatDate(this.props.modification.date_initial)
                                                        :
                                                        'Sin modificación'
                                                }
                                            </h6>
                                        </div>

                                        <div className="alert alert-warning">
                                            <h6>
                                                <strong>Modificación de fin: </strong>
                                                {
                                                    this.props.modification.date_final
                                                        ?
                                                        FormatDate(this.props.modification.date_final)
                                                        :
                                                        'Sin modificación'
                                                }
                                            </h6>
                                        </div>

                                        <div className="alert alert-warning">
                                            <h6>
                                                <strong>Modificación de solicitud: </strong>
                                            </h6>
                                                {
                                                    this.props.modification.file
                                                        ?
                                                        <a target="_blank"
                                                           rel="noopener noreferrer"
                                                           className="btn btn-dark py-0"
                                                           href={
                                                               `${SERVER}${this.props.modification.file}`}>
                                                            Ver solicitud
                                                        </a>
                                                        :
                                                        "Sin modificación"
                                                }
                                        </div>

                                        <div className="alert alert-warning">
                                            <h6>
                                                <strong>Modificación de programa de actividades: </strong>
                                            </h6>
                                                {
                                                    this.props.modification.program
                                                        ?
                                                        <a target="_blank"
                                                           rel="noopener noreferrer"
                                                           className="btn btn-dark py-0"
                                                           href={
                                                               `${SERVER}${this.props.modification.program}`}>
                                                            Ver programa de actividades
                                                        </a>
                                                        :
                                                        "Sin modificación"
                                                }
                                        </div>

                                        <div className="alert alert-warning">
                                            <h6>
                                                <strong>Carta agregada: </strong>
                                            </h6>
                                            {
                                                this.props.modification.letter !== null
                                                    ?
                                                    <a target="_blank"
                                                       rel="noopener noreferrer"
                                                       className="btn btn-dark py-0"
                                                       href={
                                                           `${SERVER}${this.props.modification.letter}`}>
                                                        Ver carta agregada
                                                    </a>
                                                    :
                                                    "Sin modificación"
                                            }
                                        </div>

                                    </section>
                                    :
                                    <section className="py-3">
                                        <h5><strong>loading...</strong></h5>
                                    </section>
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

const mapStateToProps = (state) => ({
    sabbatical: state.sabbatical.sabbatical
});

export default connect(mapStateToProps, null )(ViewModification);