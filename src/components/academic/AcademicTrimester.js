import React, { Component, Fragment } from "react";
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import SectionFilter from './components/SectionFilter';


export class AcademicTrimester extends Component {

    static propTypes = {
        isAuthenticated: PropTypes.bool,
    }

    render() {
        return (

            <Fragment>
                <div className="text-center mt-3">
                    <h2 className="mt-3 text-center">Carga académica por trimestre</h2>
                </div>
                <SectionFilter />
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    isAuthenticated: state.auth.isAuthenticated
});

export default connect(mapStateToProps, null)(AcademicTrimester);
