import React, { Component, Fragment } from "react";
import { connect } from 'react-redux';

import SectionFilterTotals from './components/SectionFilterTotals';


export class AcademicTotals extends Component {
    render() {
        return (
            <Fragment>
                <div className="text-center mt-3">
                    <h2 className="mt-3 text-center">Carga académica anual</h2>
                </div>
                <SectionFilterTotals />
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    user: state.auth.user
});

export default connect(mapStateToProps, null)(AcademicTotals);
