import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { loadYears } from "../../../actions/academic/years";


export class Year extends Component {

    static propTypes = {
        years: PropTypes.object.isRequired,
        loadYears: PropTypes.func.isRequired,
        select_year: PropTypes.string
    }

    componentDidMount() {

        if (this.props.years.years === null){
            this.props.loadYears();
        }
    }

    render() {

        if (this.props.years.load === true){
            return (
                <select className="form-control" name="year"
                        value={this.props.select_year} onChange={this.props.onChange}>
                    {this.props.years.years.map((year) => (
                        <option key={year.id}>{year.year}</option>
                        ))};
                </select>
            );
        } else {
            return <h1 className="text-center">Loading...</h1>
        }

    }
}

const mapStateToProps = (state) => ({
    years: state.years
});

export default connect(
    mapStateToProps, { loadYears })(Year);
