import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import {loadCourses} from '../../../actions/academic/course';


export class Course extends Component {

    static propTypes = {
        select_course: PropTypes.string,
        loadCourses: PropTypes.func.isRequired
    }

    componentDidMount() {
        this.props.loadCourses(1);
    }

    render() {

        if (this.props.course.load === true){
            return (
                <select className="form-control" name="course"
                        value={this.props.select_course} onChange={this.props.onChange}>
                    {this.props.course.courses.map((course) => (
                        <option key={course.id}>{course.name}</option>
                        ))};
                </select>
            );
        } else {
            return <h4 className="text-center">Loading...</h4>
        }

    }
}

const mapStateToProps = (state) => ({
    course: state.course
});

export default connect(mapStateToProps, {loadCourses} )(Course);
