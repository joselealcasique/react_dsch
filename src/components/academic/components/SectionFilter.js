import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from "prop-types";

import { get_academic_statistics } from '../../../actions/academic/academicstatistics';

import Departaments from "./Departaments";
import Plans from './Plans';
import ListStatistics from "./ListStatistics";
import Trimester from './Trimester';
import Year from './Year';


let initial_year = new Date().getFullYear();


export class SectionFilter extends Component {

    state = {
        departament: 'Humanidades',
        study_plan: 'Departamentos',
        trimester: 'Invierno',
        year:  initial_year.toString()
    }

    static propTypes = {
        academic: PropTypes.array,
        get_academic_statistics: PropTypes.func.isRequired,
        user: PropTypes.object.isRequired,
        permissions: PropTypes.array.isRequired,
    };

    componentDidMount() {
        if (this.props.permissions.includes('authentication.view_academic_social_sciences')){
            this.setState({ departament: 'Ciencias Sociales' });

        } else if (this.props.permissions.includes('authentication.view_academic_institutional_studies')){
            this.setState({ departament: 'Estudios Institucionales' });

        } else if (this.props.permissions.includes('authentication.view_academic_humanities')){
            this.setState({ departament: 'Humanidades' });
        }
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});

    onSubmit = (e) => {
        e.preventDefault();
        this.props.get_academic_statistics(
            this.state.study_plan, this.state.departament, this.state.year, this.state.trimester);
    };


    render() {

        const { departament, study_plan, trimester, year } = this.state;

        let inputDepartament = null;

        if (this.props.permissions.includes('authentication.view_academic_humanities')){
            inputDepartament = <input type="text" value="Humanidades" className="form-control" disabled/>

        } else if (this.props.permissions.includes('authentication.view_academic_social_sciences')){
            inputDepartament = <input type="text"  value="Ciencias Sociales" className="form-control" disabled/>

        } else if (this.props.permissions.includes('authentication.view_academic_institutional_studies')){
            inputDepartament = <input type="text" value="Estudios Institucionales" className="form-control" disabled/>
        }

        return (
            <Fragment>
                <form className="filter-section-dsch" onSubmit={this.onSubmit}>

                    <Plans
                        select_plan={study_plan}
                        onChange={this.onChange}
                    />

                    {
                        this.props.permissions.includes('authentication.admin_academic') |
                        this.props.permissions.includes('authentication.admin_root') |
                        this.props.permissions.includes('authentication.view_academic_general') ?
                            <Departaments
                                select_departament={departament}
                                onChange={this.onChange}
                            />
                            : inputDepartament
                    }

                    <Year
                        select_year={year}
                        onChange={this.onChange}
                    />

                    <Trimester
                        select_trimester={trimester}
                        onChange={this.onChange}
                    />

                    <button className="btn-dsch mt-2">Mostar</button>
                </form>

                <ListStatistics
                    academic={this.props.academic}
                />
            </Fragment>
            );
    }
}

const mapStateToProps = (state) => ({
    study: state.study,
    academic: state.academic.statistics,
    user: state.auth.user,
    permissions: state.auth.permissions,
});

export default connect(
    mapStateToProps, { get_academic_statistics })(SectionFilter);
