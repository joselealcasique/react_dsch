import React, { Component } from "react";
import { connect } from 'react-redux';
import PropTypes from "prop-types";

import Course from "./Course";

import { loadCourses } from '../../../actions/academic/course';
import { get_academic_filter_course_totals } from '../../../actions/academic/academicfiltercoursetotals';


export class ReportCourse extends Component {

    state = {
        course: 'Cultura Contemporánea',
        filter_course: '',
        number: '',
        search_n: false
    }

    static propTypes = {
        loadCourses: PropTypes.func.isRequired,
        get_academic_filter_course_totals: PropTypes.func.isRequired,
        courses: PropTypes.object.isRequired
    }


    filterCourse = (e) => {
        this.setState({ filter_course: e.target.value});
        this.props.loadCourses(e.target.value);
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onChecked = (e) => this.setState({search_n: this.state.search_n ? false : true});

    onSubmit = (e) => {
        e.preventDefault();
        let number = 'all'
        let course = 'all'

        if (this.state.search_n){
            if (this.state.number === ''){
                alert('Agregar Número económico');
                return false;
            }

            number = this.state.number;
            course = 'all';

        } else {
            course = this.state.course;
            number = 'all';
        }

        this.props.get_academic_filter_course_totals(number, course);
        this.setState({filter_course: '', number: ''});
    }

    render() {
        return (
            <div className="col-12 mx-auto text-center alert alert-primary">
                <h5 className="text-center">
                    <strong>Reportes de carga académica anual por curso</strong>
                </h5>
                <div className="col-12 text-justify mx-auto my-3">
                    Para definir el tipo de búsqueda es necesario activar o
                    inactivar la siguiente casilla.
                </div>

                <div className="form-check my-2">
                    <input type="checkbox" className="form-check-input float-right" name="search_n"
                           value={this.state.search_n} onChange={this.onChecked}/>
                    <label className="form-check-label" htmlFor="postgraduate">
                        {
                            this.state.search_n ?
                                'Búsqueda por número económico' : 'Búsqueda por curso'
                        }
                    </label>
                </div>
                <hr></hr>
                <form className="mt-3" onSubmit={this.onSubmit}>
                    <div className="filter-section-dsch-multi-form">
                        <div className="form-group col-12">
                            <strong>Número Económico: </strong>
                            <input type="text" className="form-control" name="number"
                                   value={this.state.number} onChange={this.onChange}
                            />
                        </div>
                    </div>

                    <div className="form-group filter-section-dsch-multi-form b-filter mt-1 pt-2">
                        <div className="form-group col-4">
                            <strong>Buscar: </strong>
                            <input type="text" className="form-control" name="filter_course" maxLength={4}
                                   value={this.state.filter_course} onChange={this.filterCourse}
                            />
                        </div>
                        <div className="form-group col-8">
                            <strong>Curso: </strong>
                            <Course
                                select_course={this.state.course}
                                onChange={this.onChange}
                            />
                        </div>
                    </div>

                    <div className="form-group mt-4 ml-3">
                        <button className="btn btn-primary" data-toggle="modal" data-target="#ModalReportTotals">
                            Ver reporte
                        </button>
                    </div>
                </form>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    user: state.auth.user,
    courses: state.course
});

export default connect(
    mapStateToProps, {loadCourses, get_academic_filter_course_totals})(ReportCourse);
