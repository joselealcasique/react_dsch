import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from "prop-types";
import {connect} from "react-redux";

import register_file from '../../../actions/academic/registerfile';
import Plans from "./Plans";

export class FormFile extends Component {

    state = {
        file: null,
        study_plan: 'Departamentos',
    }

    static propTypes = {
        register_file: PropTypes.func.isRequired,
    }

    onChange = (e) => {
        this.setState({[e.target.name]: e.target.value});
    }

    onChangeFile = (e) => this.setState({file: e.target.files[0]});


    onSubmit = (e) => {
        e.preventDefault();
        const files = new FormData();
        files.append('file', this.state.file, this.state.file.name);
        this.props.register_file(files, this.state.study_plan);
        return <Redirect to="/" />
    };

    render() {

        return (
            <div className="col-md-4 m-auto">
                <div className="card card-body">
                    <h3 className="text-center">Registrar por Archivo (xslx)</h3>
                    <form onSubmit={this.onSubmit} >

                        <div className="form-group col-12">
                            <label className="mt-5">Selecionar Plan de Estudio</label>
                            <Plans
                                select_plan={this.state.study_plan}
                                    onChange={this.onChange}
                            />
                        </div>
                        <hr></hr>

                        <div className="form-group col-12 mt-5">
                                <input type="file" className="form-control" name="file" required
                                       value={this.file} onChange={this.onChangeFile} />
                        </div>

                        <div className="form-group">
                            <div className="form-group col-6 m-auto">
                                <button type="submit" className="btn btn-file-academic">
                                    Guardar Información
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({});

export default connect(
    mapStateToProps, { register_file })(FormFile);
