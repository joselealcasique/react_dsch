import React, { Component, Fragment } from 'react';
import {Link} from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { get_academic_statistics } from '../../../actions/academic/academicstatistics';

export class ListStatistics extends Component {

    static propTypes = {
        academic: PropTypes.array,
        get_academic_statistics: PropTypes.func.isRequired,
        load: PropTypes.bool,
        auth: PropTypes.object,
    };

    componentDidMount() {
        const year = new Date().getFullYear();
        this.props.get_academic_statistics('Departamentos', 'Humanidades', year, 'Invierno');
    }

    render() {
        if (this.props.load === true && this.props.academic) {
            return (
                <Fragment>
                    <table className="table table-striped mt-3">
                        <thead className="text-center">
                            <tr className="text-center">
                                <th width={200}>Número Económico</th>
                                <th width={200}>Profesor(a)</th>
                                <th width={250}>Nombre de Curso</th>
                                <th width={150}>Horas</th>
                                <th width={150}>Estudiantes</th>
                                <th width={150}>Detalle</th>
                                <th width={150}>Acciones</th>
                            </tr>
                        </thead>
                        <tbody className="text-center table-scroll-a">
                            {this.props.academic.map((lead) => (
                            <tr key={lead.id}>
                                <td width={200}>{lead.user}</td>
                                <td width={200}>{lead.name}</td>
                                <td width={250}>{lead.course}</td>
                                <td width={150}>{lead.number_hour.toFixed(2)}</td>
                                <td width={150}>{lead.number_student.toFixed(2)}</td>
                                <td width={150}>{lead.classroom === true ? 'En Aula': 'Fuera de Aula'}</td>
                                <td width={150}>
                                    {
                                        this.props.auth.user !== null ?
                                            this.props.auth.user.departament !== 'view_academic_humanities' ?
                                                <Link to={{
                                                    pathname: '/academic-edit',
                                                    state: {id: lead.id}
                                                }} >Editar</Link>
                                                :
                                                'Sin acción'
                                            :
                                            null
                                    }
                                </td>
                            </tr>
                            ))}
                        </tbody>
                    </table>
                </Fragment>
            );
        } else{

            if (this.props.load === true){
                return <h3 className="mt-5 text-center">NO HAY COINICIDENCIAS</h3>
            } else{
                return <h3 className="mt-5 text-center">Loading ...</h3>
            }

        }
    }
}

const mapStateToProps = (state) => ({
  academic: state.academic.statistics,
  load: state.academic.load,
    auth: state.auth
});

export default connect(
    mapStateToProps, { get_academic_statistics  })(ListStatistics);