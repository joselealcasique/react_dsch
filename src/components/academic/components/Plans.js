import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { loadStudyPlan } from '../../../actions/academic/studyplans';


export class Plans extends Component {

    static propTypes = {
        study: PropTypes.object.isRequired,
        loadStudyPlan: PropTypes.func.isRequired,
        select_plan: PropTypes.string
    }

    componentDidMount() {
        if (this.props.study.study_plans === null){
            this.props.loadStudyPlan();
        }
    }

    render() {

        if (this.props.study.load === true){

            return (
                <select className="form-control" name="study_plan"
                        value={this.props.select_plan} onChange={this.props.onChange}>

                    {this.props.study.study_plans.map((departament) => (
                        <option key={departament.id} >{departament.name}</option>
                    ))};

                </select>
            );
        } else{
            return <h1 className="text-center">Loading...</h1>
        }

    }
}

const mapStateToProps = (state) => ({
    study: state.study
});

export default connect(
    mapStateToProps, { loadStudyPlan })(Plans);
