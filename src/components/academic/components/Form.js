import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Plans from "./Plans";
import Departaments from "./Departaments";
import Year from "./Year";
import Trimester from "./Trimester";
import register_academic_statistics from '../../../actions/academic/registeracademicstatisitcs';


const initial_year = new Date().getFullYear();

export class Form extends Component {

    state = {
        course: '',
        number_course: 0,
        number_hours: 0,
        number_student: 0,
        classroom: false,
        trimester: 'Invierno',
        year: initial_year.toString(),
        user: '',
        study_plan: 'Departamentos',
        departament: 'Humanidades'
    }

    static propTypes = {
        register_academic_statistics: PropTypes.func.isRequired,
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});

    onChecked = (e) => {
        this.setState({classroom: e.target.checked});
    }

    onSubmit = (e) => {
        e.preventDefault();
        this.props.register_academic_statistics(
            this.state.user, this.state.study_plan, this.state.departament, this.state.year,
            this.state.trimester, this.state.classroom, this.state.course, this.state.number_course,
            this.state.number_student, this.state.number_hours
        );

        this.setState({
            course: '',
            number_course: 0,
            number_hours: 0,
            number_student: 0,
            classroom: false,
            trimester: 'Invierno',
            year: initial_year.toString(),
            user: '',
            study_plan: 'Licenciatura',
            departament: 'Humanidades'
        });

    };

    render() {

        return (
            <div className="col-md-8">
                <div className="card card-body mt-4">
                    <h3 className="text-center">Registro por Formulario</h3>
                    <form onSubmit={this.onSubmit}>

                        <section className="filter-section-dsch-multi-form mt-3 section-dsch-form">
                            <div className="form-group col-3">
                                <label>Número Económico</label>
                                <input type="text" className="form-control" name="user" required
                                       onChange={this.onChange} value={this.state.user}/>
                            </div>

                            <div className="form-group col-6">
                                <span></span>
                            </div>

                            <div className="form-group col-3">
                                <button type="submit" className="btn btn-form-academic">
                                    Guardar Información
                                </button>
                            </div>
                        </section>

                        <section className="filter-section-dsch-multi-form mt-3 section-dsch-form">
                            <div className="form-group col-3">
                                <label>Plan de Estudio</label>
                                <Plans
                                    select_plan={this.state.study_plan}
                                    onChange={this.onChange}
                                />
                            </div>

                            <div className="form-group col-4">
                                <label>Departamento</label>
                                <Departaments
                                    select_departament={this.state.departament}
                                    onChange={this.onChange}
                                />
                            </div>

                            <div className="form-group col-2">
                                <label>Año</label>
                                <Year
                                    select_year={this.state.year}
                                    onChange={this.onChange}
                                />
                            </div>

                            <div className="form-group col-3">
                                <label>Trimestre</label>
                                <Trimester
                                    select_trimester={this.state.trimester}
                                    onChange={this.onChange}
                                />
                            </div>
                        </section>

                        <section className="filter-section-dsch-multi-form mt-3 section-dsch-form">
                            <div className="form-group col-7">
                                <label>Nombre del Curso</label>
                                <input type="text" className="form-control" required name="course"
                                       value={this.state.course} onChange={this.onChange} />
                            </div>
                            <div className="col-1"></div>

                            <div className="col-5">
                                ¿Curso dentro de Aula?
                                <br></br>
                                <div className="form-check col-10">
                                    <input type="checkbox" className="form-check-input" name="classroom"
                                    value={this.state.classroom} onChange={this.onChecked}/>
                                    <label className="form-check-label" htmlFor="classroom">
                                        Selecionar si es Afirmativo
                                    </label>
                                </div>
                            </div>
                        </section>

                        <section className="filter-section-dsch-multi-form mt-3 section-dsch-form">
                            <div className="form-group col-4">
                                <label>Número de Cursos</label>
                                <input type="number" className="form-control" required name="number_course"
                                       value={this.state.number_course} onChange={this.onChange} />
                            </div>

                            <div className="form-group col-4">
                                <label>Horas del Curso</label>
                                <input type="number" className="form-control" required name="number_hours"
                                       value={this.state.number_hours} onChange={this.onChange} />
                            </div>

                            <div className="form-group col-4">
                                <label>Número de Alumnos</label>
                                <input type="number" className="form-control" required name="number_student"
                                       value={this.state.number_student} onChange={this.onChange} />
                            </div>
                        </section>

                    </form>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({});

export default connect(
    mapStateToProps, { register_academic_statistics })(Form);