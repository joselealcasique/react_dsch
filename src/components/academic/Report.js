import React, { Component } from "react";

import ReportCourse from "./components/ReportCourse";
import ReportTotals from "./components/ReportTotals";
import ReportMultiPlans from "./components/ReportMultiPlans";
import ModalReport from "./components/ModalReport";


export class Report extends Component {


    render() {
        return (
            <div className="filter-section-dsch-multi-form pt-3">

                <section className="col-4">
                    <ReportMultiPlans />
                </section>

                <section className="col-4">
                    <ReportTotals />
                </section>

                <section className="col-4">
                    <ReportCourse />
                </section>


                <ModalReport />
            </div>
        );
    }
}


export default Report;
