import React, { Component, Fragment } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFileExcel, faFileCsv, faAddressCard } from '@fortawesome/free-solid-svg-icons';

import ProductionFile from "./ProductionFile";
import ProductionIndividual from "./ProductionIndividual";
import TypeReport from "./components/TypeReport";


export class CreateReport extends Component {

    state = {
        individual: false,
        multiple: false,
        report: 1
    }

    onGetViewFile = () => this.setState({ multiple: this.state.multiple ? false : true });
    onGetViewIndividual = () => this.setState({ individual: this.state.multiple ? false : true });
    onCancelView = () => this.setState({ individual: false, multiple: false });
    onChange = (e) => this.setState({ [e.target.name]: e.target.value});


    render() {

        if (!this.state.multiple & !this.state.individual) {

            return (
                <section className="mx-auto col-6 mt-5">
                    <div className="modal-header title-detail">
                        <h5 className="m-auto">
                            <strong>
                                Registro de informes anuales
                            </strong>
                        </h5>
                    </div>
                    <section className="modal-header b-filter">
                        <section className="filter-section-dsch-multi-form col-10 mx-auto pt-4 pb-3">
                            <div className="col-6 alert alert-success text-center py-3 mx-1 btn"
                                 onClick={this.onGetViewFile}>
                                <FontAwesomeIcon icon={faFileExcel} size="4x" color="green"/>
                                <h6 className="mt-3">
                                    <strong>Registro por archivo</strong>
                                </h6>
                            </div>
                            <div className="col-6 alert alert-primary text-center py-3 mx-1 btn"
                                 onClick={this.onGetViewIndividual}>
                                <FontAwesomeIcon icon={faAddressCard} size="4x" color="blue"/>
                                <h6 className="mt-3">
                                    <strong>Registro individual</strong>
                                </h6>
                            </div>
                        </section>
                    </section>
                </section>
            );
        } else {
            return (
                <Fragment>
                    <section className="mt-2 filter-section-dsch-multi-form">
                        <div className="col-6">
                            {
                                this.state.individual
                                    ?
                                    <div className="filter-section-dsch-multi-form mx-auto alert-warning">
                                        <h6 className="col-5 mt-3"><strong>Seleccionar tipo de informe</strong></h6>
                                        <TypeReport
                                            onChange={this.onChange}
                                            select_type={parseInt(this.state.report)}
                                        />
                                    </div>
                                    :
                                    null
                            }
                        </div>
                        <div className="col-6">
                            <button  className="btn btn-danger float-right" onClick={this.onCancelView}>
                                Cancelar Registro
                            </button>
                        </div>
                    </section>
                    <section>
                        {
                            this.state.multiple
                                ?
                                <ProductionFile />
                                :
                                this.state.individual
                                ?
                                <ProductionIndividual  report={parseInt(this.state.report)} />
                                :
                                <section className="text-center mt-5">
                                    <h5><strong>NO EXISTE OPCIÓN, CLIC EN CANCELAR</strong></h5>
                                </section>
                        }
                    </section>
                </Fragment>
            );
        }
    }
}

export default CreateReport;