import React, { Component, Fragment } from 'react';
import PropTypes from "prop-types";
import {connect} from "react-redux";

import TypeReport from "./components/TypeReport";
import ListTeacherTraining from "./components/list/ListTeacherTraining";
import ListExchangeActivity from "./components/list/ListExchangeActivity";
import LIstAcademicEvent from "./components/list/LIstAcademicEvent";
import ListAcademicCollaboration from "./components/list/ListAcademicCollaboration";
import ListProjectCollaboration from "./components/list/ListProjectCollaboration";
import ListMagazinePublication from "./components/list/ListMagazinePublication";
import ListElectronicJournals from "./components/list/ListElectronicJournals";
import ListNewsPaperPublication from "./components/list/ListNewsPaperPublication";
import ListPublishedBook from "./components/list/ListPublishedBook";
import ListChaptersBook from "./components/list/ListChaptersBook";
import ListBookReview from "./components/list/ListBookReview";
import ListPublishedLectures from "./components/list/ListPublishedLectures";
import ListUnpublishedLectures from "./components/list/ListUnpublishedLectures";
import ListResearchProject from "./components/list/ListResearchProject";

import { get_list_report } from "../../actions/production/get_list_report";


export class Production extends Component {

    state = {
        initial: new Date().getFullYear(),
        final: new Date().getFullYear(),
        humanities: false,
        sciences: false,
        studies: false,
        number: '',
        report: 1,
        view: <div className="text-center mt-5 alert-danger py-4 col-12 mx-auto">
            <h4><strong>ESPERANDO OPCIÓN</strong></h4>
        </div>
    }

    static propTypes = {
        years: PropTypes.object.isRequired,
        get_list_report: PropTypes.func.isRequired
    }

    onChecked = (e) => this.setState({[e.target.name]: e.target.checked});
    onChange = (e) => this.setState({ [e.target.name]: e.target.value});

    toTitleCase = (string) => {
        return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
    }

    get_full_name = (obj) => {
        let name = this.toTitleCase(obj.paternal_surname);
        name += ' '+this.toTitleCase(obj.maternal_surname);
        name += ' '+this.toTitleCase(obj.name);
        return name
    }

    onGetView = (e) => {
        switch (parseInt(this.state.report)) {

            case 1:
                this.setState({
                    view: <ListTeacherTraining get_full_name={this.get_full_name} />,
                });
                break;

            case 2:
                this.setState({
                    view: <ListExchangeActivity get_full_name={this.get_full_name} />,
                })
                break;

            case 3:
                this.setState({
                    view: <LIstAcademicEvent get_full_name={this.get_full_name} />,
                })
                break;

            case 4:
                this.setState({
                    view: <ListAcademicCollaboration get_full_name={this.get_full_name} />,
                })
                break;

            case 5:
                this.setState({
                    view: <ListProjectCollaboration get_full_name={this.get_full_name} />,
                })
                break;

            case 6:
                this.setState({
                    view: <ListMagazinePublication get_full_name={this.get_full_name} />,
                })
                break;

            case 7:
                this.setState({
                    view: <ListElectronicJournals get_full_name={this.get_full_name} />,
                })
                break;

            case 8:
                this.setState({
                    view: <ListNewsPaperPublication get_full_name={this.get_full_name} />,
                })
                break;

            case 9:
                this.setState({
                    view: <ListPublishedBook get_full_name={this.get_full_name} />,
                })
                break;

            case 10:
                this.setState({
                    view: <ListChaptersBook get_full_name={this.get_full_name} />,
                })
                break;

            case 11:
                this.setState({
                    view: <ListBookReview get_full_name={this.get_full_name} />,
                })
                break;

            case 12:
                this.setState({
                    view: <ListPublishedLectures get_full_name={this.get_full_name} />,
                })
                break;

            case 13:
                this.setState({
                    view: <ListUnpublishedLectures get_full_name={this.get_full_name} />,
                })
                break;

            case 14:
                this.setState({
                    view: <ListResearchProject get_full_name={this.get_full_name} />,
                })
                break;

            default:
                this.setState({
                    view: <div className="text-center mt-5 alert-danger col-6 mx-auto">
                        <h4><strong>NO EXISTE OPCIÓN</strong></h4>
                    </div>,
                    form: null
                });
                break;

        }
    }

    onSubmit = (e) => {
        e.preventDefault();

        if (!this.state.humanities && !this.state.studies && !this.state.sciences){
            alert('Para realizar la consulta, debe agregar mímino un departamento');
            return false;
        }

        let departament = [];
        let report_and_number = this.state.report;

        if (this.state.humanities) departament.push('Humanidades');
        if (this.state.studies) departament.push('Estudios Institucionales');
        if (this.state.sciences) departament.push('Ciencias Sociales');

        if (this.state.number !== ''){
            report_and_number += '-'+this.state.number;
        }

        this.props.get_list_report(
            departament, report_and_number, this.state.initial, this.state.final, this.state.report)
        this.onGetView(e);
    }


    render() {

        return (
            <Fragment>
                <div className="col-12 text-center mt-2 alert alert-warning">
                    <h4><strong>Búsqueda de Reportes</strong></h4>
                </div>

                <div className="filter-section-dsch-multi-form">
                    <form className="col-4" onSubmit={this.onSubmit} >

                        <div className="text-center alert alert-dark my-auto">
                            <label><strong>Seleccionar tipo de reporte</strong></label>
                            <TypeReport
                                select_type={parseInt(this.state.report)}
                                onChange={this.onChange}/>
                        </div>

                        <div className="alert alert-dark my-2">
                            <h6 className="text-center"><strong>Seleccionar departamentos</strong></h6>
                            <table className="col-12 mx-auto">
                                <tbody>
                                    <tr >
                                        <td className="text-left">
                                            <div className="form-check">
                                                <input type="checkbox" className="form-check-input"
                                                       name="humanities"
                                                       value={this.state.humanities}
                                                       onChange={this.onChecked}/>
                                                <label className="form-check-label"
                                                       htmlFor="humanities">
                                                    Humanidades
                                                </label>
                                            </div>
                                        </td>
                                        <td className="text-left">
                                            <div className="form-check">
                                                <input type="checkbox" className="form-check-input"
                                                       name="sciences"
                                                       value={this.state.sciences}
                                                       onChange={this.onChecked}/>
                                                <label className="form-check-label"
                                                       htmlFor="sciences">
                                                    Ciencias Sociales
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="text-left">
                                            <div className="form-check">
                                                <input type="checkbox" className="form-check-input"
                                                       name="studies"
                                                       value={this.state.studies}
                                                       onChange={this.onChecked}/>
                                                <label className="form-check-label"
                                                       htmlFor="studies">
                                                    Estudios Institucionales
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div className="py-2 mb-2 alert-dark filter-section-dsch-multi-form col-12">
                            <div className="col-6">
                                <h6 className="text-center"><strong>Año inicial</strong></h6>
                                {
                                    this.props.years.load ?
                                        <select className="form-control" name="initial"
                                            value={this.state.initial} onChange={this.onChange}>
                                        {
                                            this.props.years.years.map((year) => (
                                                <option key={year.id}>{year.year}</option>
                                            ))
                                        }
                                        </select>
                                        :
                                        <h6>loading...</h6>
                                }
                            </div>

                            <div className="col-6 ">
                                <h6 className="text-center"><strong>Año final</strong></h6>
                                {
                                    this.props.years.load ?
                                        <select className="form-control" name="final"
                                            value={this.state.final} onChange={this.onChange}>
                                        {
                                            this.props.years.years.map((year) => (
                                                <option key={year.id}>{year.year}</option>
                                            ))
                                        }
                                        </select>
                                        :
                                        <h6>loading...</h6>
                                }
                            </div>
                        </div>

                        <div className="my-auto">
                            <div className="filter-section-dsch-multi-form alert alert-dark">
                                <div className="col-7">
                                    <h6><strong>Número económico</strong></h6>
                                    <input className="form-control" placeholder="Valor opcional"
                                           value={this.state.number} name="number"
                                           onChange={this.onChange}
                                    />
                                </div>
                                <div className="pt-3 col-5">
                                    <button className="btn btn-dark mt-2">
                                        Ver reportes
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div className="col-8">
                        {this.state.view}
                    </div>
                </div>
            </Fragment>
        )
    }
}

const mapStateToProps = (state) => ({
    years: state.years,
});

export default connect(mapStateToProps, { get_list_report } )(Production);