import React, { Component } from 'react';
import PropTypes from "prop-types";
import {connect} from "react-redux";

import { FormatDate } from "../../../FormatDate";
import {SERVER} from "../../../../actions/server";
import FormNewspaperPublication from "../forms/FormNewspaperPublication";



export class ViewNewsPaperPublication extends Component {

    state = {
        edit: false,
    }

    static propTypes = {
        register: PropTypes.object,
        get_full_name: PropTypes.func.isRequired,
        permissions: PropTypes.array
    }

    onEnableEdit = (e) => this.setState({edit: this.state.edit ? false : true});
    onEditFalse = (e) => this.setState({edit: false});

    render() {
        let detailView = null;

        if (this.props.register != null){
            detailView = (
                <div className="col-11 m-auto">
                    <h6>
                        <strong>Profesor (s): </strong>
                        <ul>
                        {this.props.register.profile.map((user, index) =>(
                            <li  key={user.id}>{this.props.get_full_name(user)}</li>
                        ))}
                        </ul>
                    </h6>
                    <h6><strong>Título de actículo: </strong> {this.props.register.title}</h6>
                    <h6><strong>Título de publicación:  </strong>
                        { this.props.register.newspaper_name }
                    </h6>
                    <h6><strong>Fecha: </strong>{FormatDate(this.props.register.date)}</h6>
                    <h6><strong>Sección: </strong>
                        {this.props.register.section === null
                            ? 'NO REGISTRADO' : this.props.register.section }
                    </h6>
                    <h6><strong>Páginas: </strong>
                        { this.props.register.pages === null
                            ? 'NO REGISTRADO' : this.props.register.pages }
                    </h6>
                    <h6><strong>Tipo de trabajo: </strong>
                        {this.props.register.type_job === null
                            ? 'NO REGISTRADO' : this.props.register.type_job}
                    </h6>
                    {
                        this.props.register.file !== null
                            ?
                            <a target="_blank" rel="noopener noreferrer" className="btn btn-dsch"
                               href={`${SERVER}${this.props.register.file}`}>
                                <strong>Ver Probatorio: </strong>
                            </a>
                            :
                            <h6><strong>Probatorio: </strong> PENDIENTE </h6>
                    }

                    {
                        this.props.permissions.includes('authentication.admin_production')
                        |
                        this.props.permissions.includes('authentication.admin_root')
                            ?
                            <button onClick={this.onEnableEdit} className="btn btn-primary float-right">
                                Editar
                            </button>
                            :
                            null
                    }
                </div>
            )
        }

        return (
            <div className="modal fade" id="viewTeacherTraining" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalLongTitle" aria-hidden="true" data-backdrop="static">
                <div className={`${this.state.edit ? 'modal-lg' : 'modal-md'} modal-dialog`} role="document">
                    <div className="modal-content">
                        <div className="modal-header title-detail">
                            <h5 className="modal-title " id="exampleModalLongTitle">
                                Publicaciones en Revistas
                            </h5>
                            <button type="button" className="close" data-dismiss="modal"
                                    aria-label="Close" onClick={this.onEditFalse}>
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body alert-dark">
                            {
                                this.state.edit
                                    ?
                                    <FormNewspaperPublication
                                        obj={this.props.register}
                                        edit={this.state.edit}
                                        onEnableEdit={this.onEnableEdit}
                                    />
                                    :
                                    detailView
                            }
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    permissions: state.auth.permissions
});

export default connect(mapStateToProps, null  )(ViewNewsPaperPublication);