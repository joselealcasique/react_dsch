import React, { Component } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import { edit_unpublished  } from "../../../../actions/production/edit_unpublished";


export class FormUnpublishedLectures extends Component {

    state = {
        file: null,
        title: '',
        present: '',
        congress: '',
        city: '',
        country: '',
        institute: '',
        type_job: 'Conferencia magistral',
    }

    static propTypes = {
        obj: PropTypes.number.isRequired,
        edit: PropTypes.bool.isRequired,
        edit_unpublished: PropTypes.func.isRequired
    }

    componentDidMount() {
        this.setState({
            title: this.props.obj.title,
            present: this.props.obj.present,
            congress: this.props.obj.congress,
            city: this.props.obj.city,
            country: this.props.obj.country,
            institute: this.props.obj.institute,
            type_job: this.props.obj.type_job,
        })
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onChangeFile = (e) => this.setState({file: e.target.files[0]});

    onSubmit = (e) => {
        e.preventDefault();
        let data = new FormData()
        data.append('title', this.state.title);
        data.append('present', this.state.present);
        data.append('congress', this.state.congress);
        data.append('city', this.state.city);
        data.append('country', this.state.country);
        data.append('institute', this.state.institute);
        data.append('type_job', this.state.type_job);

        try {
            data.append('file', this.state.file, this.state.file.name);
            // envío de información
            this.props.edit_unpublished(this.props.obj.id, data);

        } catch (e){
            if (e.TypeError === undefined){
                if (window.confirm('No se adjuntó archivo. ¿Desea continuar con el registro?')){
                    // envío de información
                    this.props.edit_unpublished(this.props.obj.id, data);
                }
            }
        }
    }

    render() {
        return (
            <form className="col-12" onSubmit={this.onSubmit}>
                <div className="my-2 filter-section-dsch-multi-form" >
                    <div className="col-6 mx-auto">
                        <h6 className="ml-1"><strong>Título del Conferencia</strong></h6>
                        <textarea className="form-control" name="title" rows={3}
                               value={this.state.title} onChange={this.onChange} />
                    </div>

                    <div className="col-6 mx-auto">
                        <h6 className="ml-1"><strong>Lugar de presentación </strong></h6>
                        <textarea className="form-control" name="present" rows={3}
                               value={this.state.present} onChange={this.onChange} />
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form" >

                    <div className="col-6 mx-auto">
                        <h6 className="ml-1"><strong>Nombre de congreso</strong></h6>
                        <input type="text" className="form-control" name="congress"
                               value={this.state.congress} onChange={this.onChange} />
                    </div>

                    <div className="col-3 mx-auto">
                        <h6 className="ml-1"><strong>Cuidad</strong></h6>
                        <input type="text" className="form-control" name="city"
                               value={this.state.city} onChange={this.onChange} />
                    </div>

                    <div className="col-3 mx-auto">
                        <h6 className="ml-1"><strong>País </strong></h6>
                        <input type="text" className="form-control" name="country"
                               value={this.state.country} onChange={this.onChange} />
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form mb-4" >

                    <div className="col-4 mx-auto">
                        <h6 className="ml-1"><strong>Institución </strong></h6>
                        <input type="text" className="form-control" name="institute"
                               value={this.state.institute} onChange={this.onChange} />
                    </div>

                    <div className="col-4 mx-auto">
                        <h6 className="ml-1"><strong>Tipo de trabajo</strong></h6>
                        <select className="form-control" name="type_job"
                                value={this.state.type_job} onChange={this.onChange} >
                            <option key={0}>Conferencia</option>
                            <option key={1}>Conferencia magistral</option>
                            <option key={2}>Trabajo en evento especializado</option>
                        </select>
                    </div>

                    <div className="col-4 mx-auto">
                        <h6 className="mt-1"><strong>Seleccionar probatorio</strong></h6>
                        <input type="file" className="form-control" name="user" accept="application/pdf"
                               value={this.file} onChange={this.onChangeFile}/>
                    </div>
                </div>
                <div className="my-2 filter-section-dsch-multi-form mb-4" >
                    <div className="col-12 mx-auto mt-4 pt-2">
                        <button className="btn btn-primary float-right">
                            Guardar
                        </button>
                        <button onClick={this.props.onEnableEdit}
                                className="btn btn-danger float-right mr-3" >
                            Cancelar
                        </button>
                    </div>
                </div>
            </form>
        );
    }

}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps,  { edit_unpublished } )(FormUnpublishedLectures);