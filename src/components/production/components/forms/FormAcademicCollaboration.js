import React, { Component } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import { edit_a_collaboration } from '../../../../actions/production/edit_a_collaboration';


export class FormAcademicCollaboration extends Component {

    state = {
        file: null,
        name: '',
        activities: '',
        institutions: '',
    }

    static propTypes = {
        obj: PropTypes.number.isRequired,
        edit: PropTypes.bool.isRequired,
        edit_a_collaboration: PropTypes.func.isRequired
    }

    componentDidMount() {
        this.setState({
            name: this.props.obj.name,
            activities: this.props.obj.activities,
            institutions: this.props.obj.institutions,
        })
    }

    onSubmit = (e) => {
        e.preventDefault();
        let data = new FormData()
        data.append('name', this.state.name);
        data.append('activities', this.state.activities);
        data.append('institutions', this.state.institutions);

        try {
            data.append('file', this.state.file, this.state.file.name);
            // envío de información
            this.props.edit_a_collaboration(this.props.obj.id, data);

        } catch (e){
            if (e.TypeError === undefined){
                if (window.confirm('No se adjuntó archivo. ¿Desea continuar con el registro?')){
                    // envío de información
                   this.props.edit_a_collaboration(this.props.obj.id, data);
                }
            }
        }
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onChangeFile = (e) => this.setState({file: e.target.files[0]});

    render() {
        return (
            <form className="col-12" onSubmit={this.onSubmit}>
                <div className="my-3 filter-section-dsch-multi-form" >
                    <div className="col-6 mx-auto">
                        <label className="ml-1"><strong>Nombre de Red Académica </strong></label>
                        <input type="text" className="form-control" name="name"
                               value={this.state.name} onChange={this.onChange} />
                    </div>

                    <div className="col-6 mx-auto">
                        <h6 className="mt-1"><strong>Seleccionar probatorio</strong></h6>
                        <input type="file" className="form-control" name="user" accept="application/pdf"
                               value={this.file} onChange={this.onChangeFile}/>
                    </div>

                </div>

                <div className="my-2 filter-section-dsch-multi-form" >
                    <div className="col-6 mx-auto">
                        <h6 className="mt-1"><strong>Instituciones pertenecientes a la red</strong></h6>
                        <textarea className="form-control" name="institutions" rows={3}
                               value={this.state.institutions} onChange={this.onChange} />
                    </div>

                    <div className="col-6 mx-auto">
                        <label className="ml-1"><strong>Actividades Destacadas</strong></label>
                        <textarea className="form-control" name="activities" rows={3}
                               value={this.state.activities} onChange={this.onChange} />
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form mb-4" >

                    <div className="col-12 mx-auto mt-4 pt-2">
                        <button className="btn btn-primary float-right">
                            Guardar
                        </button>
                        <button onClick={this.props.onEnableEdit}
                                className="btn btn-danger float-right mr-3" >
                            Cancelar
                        </button>
                    </div>
                </div>
            </form>
        );
    }

}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, { edit_a_collaboration })(FormAcademicCollaboration);