import React, { Component } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import { edit_journals } from "../../../../actions/production/edit_journals";


export class FormElectronicJournals extends Component {

    state = {
        file: null,
        title: '',
        magazine_name: '',
        number: 0,
        volume: '',
        pages: '',
        doi: '',
        type_job: 'Artículo de investigación',
    }

    static propTypes = {
        obj: PropTypes.number.isRequired,
        edit: PropTypes.bool.isRequired,
        edit_journals: PropTypes.func.isRequired
    }

    componentDidMount() {
        this.setState({
            title: this.props.obj.title,
            magazine_name: this.props.obj.magazine_name,
            number: this.props.obj.number,
            volume: this.props.obj.volume,
            pages: this.props.obj.pages,
            doi: this.props.obj.doi,
            type_job: this.props.obj.type_job
        })
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onChangeFile = (e) => this.setState({file: e.target.files[0]});

    onSubmit = (e) => {
        e.preventDefault();

        let data = new FormData()
        data.append('title', this.state.title);
        data.append('magazine_name', this.state.magazine_name);
        data.append('number', this.state.number);
        data.append('volume', this.state.volume);
        data.append('pages', this.state.pages);
        data.append('organizations', this.state.organizations);
        data.append('doi', this.state.doi);
        data.append('type_job', this.state.type_job);

        try {
            data.append('file', this.state.file, this.state.file.name);
            // envío de información
            this.props.edit_journals(this.props.obj.id, data);

        } catch (e){
            if (e.TypeError === undefined){
                if (window.confirm('No se adjuntó archivo. ¿Desea continuar con el registro?')){
                    // envío de información
                    this.props.edit_journals(this.props.obj.id, data);
                }
            }
        }
    }

    render() {
        return (
            <form className="col-12" onSubmit={this.onSubmit}>
                <div className="my-2 filter-section-dsch-multi-form" >
                    <div className="col-5 mx-auto">
                        <h6 className="ml-1"><strong>Título de artículo </strong></h6>
                        <textarea className="form-control" name="title" rows={2}
                               value={this.state.title} onChange={this.onChange} />
                    </div>
                    <div className="col-5 mx-auto">
                        <h6 className="ml-1"><strong>Nombre de revista electrónica</strong></h6>
                        <textarea className="form-control" name="magazine_name" rows={2}
                               value={this.state.magazine_name} onChange={this.onChange} />
                    </div>

                    <div className="col-2 mx-auto">
                        <label className="ml-1"><strong>Número</strong></label>
                        <input type="number" className="form-control" name="number"
                               value={this.state.number} onChange={this.onChange} />
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form" >

                    <div className="col-2 mx-auto">
                        <h6 className="mt-1"><strong>Volumen</strong></h6>
                        <input type="text" className="form-control" name="volume"
                               value={this.state.volume} onChange={this.onChange} />
                    </div>

                    <div className="col-2 mx-auto">
                        <label className="ml-1"><strong>Páginas</strong></label>
                        <input type="text" className="form-control" name="pages"
                               value={this.state.pages} onChange={this.onChange} />
                    </div>

                    <div className="col-4 mx-auto">
                        <label className="ml-1"><strong>DOI o URL</strong></label>
                        <textarea  className="form-control" name="doi" rows={3}
                               value={this.state.doi} onChange={this.onChange} />
                    </div>

                    <div className="col-4 mx-auto">
                        <h6 className="mt-1"><strong>Tipo de trabajo</strong></h6>
                        <select className="form-control" name="type_job"
                                value={this.state.type_job} onChange={this.onChange} >
                            <option key={0}>Artículo de investigación</option>
                            <option key={1}>Artículo de divulgación</option>
                        </select>
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form mb-4" >
                    <div className="col-5 mx-auto">
                        <h6 className="mt-1"><strong>Seleccionar probatorio</strong></h6>
                        <input type="file" className="form-control" name="user" accept="application/pdf"
                               value={this.file} onChange={this.onChangeFile}/>
                    </div>
                    <div className="col-7 mx-auto mt-4 pt-2">
                        <button className="btn btn-primary float-right">
                            Guardar
                        </button>
                        <button onClick={this.props.onEnableEdit}
                                className="btn btn-danger float-right mr-3" >
                            Cancelar
                        </button>
                    </div>
                </div>
            </form>
        );
    }

}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps,  {edit_journals} )(FormElectronicJournals);