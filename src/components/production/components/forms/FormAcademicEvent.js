import React, { Component } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import { edit_event } from '../../../../actions/production/edit_event';


export class FormAcademicEvent extends Component {

    state = {
        file: null,
        name: '',
        type_activity: '',
        country: '',
        institute: '',
        range_date: '',
    }

    static propTypes = {
        obj: PropTypes.number.isRequired,
        edit: PropTypes.bool.isRequired,
        edit_event: PropTypes.func.isRequired
    }

    componentDidMount() {
        this.setState({
            name: this.props.obj.name,
            type_activity: this.props.obj.type_activity,
            country: this.props.obj.country,
            institute: this.props.obj.institute,
            range_date: this.props.obj.range_date,
        });
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onChangeFile = (e) => this.setState({file: e.target.files[0]});

    onSubmit = (e) => {
        e.preventDefault();
        let data = new FormData()
        data.append('name', this.state.name);
        data.append('type_activity', this.state.type_activity);
        data.append('institute', this.state.institute);
        data.append('country', this.state.country);

        try {
            data.append('file', this.state.file, this.state.file.name);
            // envío de información
            this.props.edit_event(this.props.obj.id ,data);

        } catch (e){
            if (e.TypeError === undefined){
                if (window.confirm('No se adjuntó archivo. ¿Desea continuar con el registro?')){
                    // envío de información
                    this.props.edit_event(this.props.obj.id ,data);
                }
            }
        }
    }

    render() {
        return (
            <form className="col-12" onSubmit={this.onSubmit}>
                <div className="my-3 filter-section-dsch-multi-form" >

                    <div className="col-6 mx-auto">
                        <label className="ml-1"><strong>Nombre de Evento</strong></label>
                        <textarea className="form-control" name="name" rows={2}
                               value={this.state.name} onChange={this.onChange} />
                    </div>

                    <div className="col-6 mx-auto">
                        <h6 className="mt-1"><strong>Instituto Sede </strong></h6>
                        <input type="text" className="form-control" name="institute"
                               value={this.state.institute} onChange={this.onChange} />
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form" >
                    <div className="col-4 mx-auto">
                        <label className="ml-1"><strong>País/Cuidad</strong></label>
                        <input type="text" className="form-control" name="country"
                               value={this.state.country} onChange={this.onChange} />
                    </div>

                    <div className="col-4 mx-auto">
                        <label className="ml-1"><strong>Fecha</strong></label>
                        <input type="text" className="form-control" name="range_date"
                               value={this.state.range_date} onChange={this.onChange} />
                    </div>

                    <div className="col-4 mx-auto">
                        <label className="ml-1"><strong>Tipo de Actividad</strong></label>
                        <input type="text" className="form-control" name="type_activity"
                               value={this.state.type_activity} onChange={this.onChange} />
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form mb-4" >
                    <div className="col-5 mx-auto">
                        <h6 className="mt-1"><strong>Seleccionar probatorio</strong></h6>
                        <input type="file" className="form-control" name="user" accept="application/pdf"
                               value={this.file} onChange={this.onChangeFile}/>
                    </div>
                    <div className="col-7 mx-auto mt-4 pt-2">
                        <button className="btn btn-primary float-right">
                            Guardar
                        </button>
                        <button onClick={this.props.onEnableEdit}
                                className="btn btn-danger float-right mr-3" >
                            Cancelar
                        </button>
                    </div>
                </div>
            </form>
        );
    }

}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, { edit_event })(FormAcademicEvent);