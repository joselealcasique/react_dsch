import React, { Component } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import Departaments from "../../../academic/components/Departaments";
import { edit_chapters } from "../../../../actions/production/edit_chapter";


export class FormChaptersBooks extends Component {

    state = {
        file: null,
        departament: 'Humanidades',
        name: '',
        bibliographic_file: '',
    }

    static propTypes = {
        obj: PropTypes.number.isRequired,
        edit: PropTypes.bool.isRequired,
        edit_chapters: PropTypes.func.isRequired
    }

    componentDidMount() {
        this.setState({
            departament: this.props.obj.departament,
            name: this.props.obj.name,
            bibliographic_file: this.props.obj.bibliographic_file,
        })
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onChangeFile = (e) => this.setState({file: e.target.files[0]});

    onSubmit = (e) => {
        e.preventDefault();

        let data = new FormData()
        data.append('departament', this.state.departament);
        data.append('name', this.state.name);
        data.append('bibliographic_file', this.state.bibliographic_file);

        try {
            data.append('file', this.state.file, this.state.file.name);
            // envío de información
            this.props.edit_chapters(this.props.obj.id, data);

        } catch (e){
            if (e.TypeError === undefined){
                if (window.confirm('No se adjuntó archivo. ¿Desea continuar con el registro?')){
                    // envío de información
                    this.props.edit_chapters(this.props.obj.id, data);
                }
            }
        }
    }

    render() {
        return (
            <form className="col-12" onSubmit={this.onSubmit}>
                <div className="my-2 filter-section-dsch-multi-form" >
                    <div className="col-6 mx-auto">
                        <h6 className="ml-1"><strong>Nombre de capítulo </strong></h6>
                        <textarea className="form-control" name="name" rows={5}
                               value={this.state.name} onChange={this.onChange} />
                    </div>
                    <div className="col-6 mx-auto">
                        <h6 className="ml-1"><strong>Ficha bibliográfica del libro </strong></h6>
                        <textarea className="form-control" name="bibliographic_file" rows={5}
                               value={this.state.bibliographic_file} onChange={this.onChange} />
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form" >
                    <div className="col-4 mx-auto">
                        <h6 className="ml-1"><strong>Seleccionar Departamento</strong></h6>
                        <Departaments
                            select_departament={this.state.departament.toString()}
                            onChange={this.onChange}
                        />
                    </div>
                    <div className="col-4 mx-auto">
                        <h6 className="mt-1"><strong>Seleccionar probatorio</strong></h6>
                        <input type="file" className="form-control" name="user" accept="application/pdf"
                               value={this.file} onChange={this.onChangeFile}/>
                    </div>
                    <div className="col-4 mx-auto mt-4 pt-2">
                        <button className="btn btn-primary float-right">
                            Guardar
                        </button>
                        <button onClick={this.props.onEnableEdit}
                                className="btn btn-danger float-right mr-3" >
                            Cancelar
                        </button>
                    </div>
                </div>
            </form>
        );
    }

}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps,  { edit_chapters } )(FormChaptersBooks);