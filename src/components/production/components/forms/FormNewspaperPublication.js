import React, { Component } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import { ConfigDateSave, FormatDate } from "../../../FormatDate";
import { edit_newspaper } from "../../../../actions/production/edit_newspaper";


export class FormNewspaperPublication extends Component {

    state = {
        file: null,
        title: '',
        newspaper_name: '',
        section: '',
        date: '',
        pages: '',
        type_job: 'Editorial',
    }

    static propTypes = {
        obj: PropTypes.number.isRequired,
        edit: PropTypes.bool.isRequired,
        add_newspaper_publication: PropTypes.func.isRequired
    }

    componentDidMount() {
        this.setState({
            title: this.props.obj.title,
            newspaper_name: this.props.obj.newspaper_name,
            section: this.props.obj.section,
            pages: this.props.obj.pages,
            type_job: this.props.obj.type_job,
        });
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onChangeFile = (e) => this.setState({file: e.target.files[0]});

    onSubmit = (e) => {
        e.preventDefault();
        let data = new FormData()
        data.append('title', this.state.title);
        data.append('newspaper_name', this.state.newspaper_name);
        data.append('section', this.state.section);
        data.append('pages', this.state.pages);
        data.append('type_job', this.state.type_job);

        if (this.state.date.trim() !== ''){
            data.append('date', ConfigDateSave(this.state.date));
        }

        try {
            data.append('file', this.state.file, this.state.file.name);
            // envío de información
            this.props.edit_newspaper(this.props.obj.id, data);

        } catch (e){
            if (e.TypeError === undefined){
                if (window.confirm('No se adjuntó archivo. ¿Desea continuar con el registro?')){
                    // envío de información
                    this.props.edit_newspaper(this.props.obj.id, data);
                }
            }
        }
    }

    render() {
        return (
            <form className="col-12" onSubmit={this.onSubmit}>
                <div className="my-2 filter-section-dsch-multi-form" >
                    <div className="col-5 mx-auto">
                        <h6 className="ml-1"><strong>Título de Artículo</strong></h6>
                        <textarea className="form-control" name="title" rows={2}
                               value={this.state.title} onChange={this.onChange} />
                    </div>
                    <div className="col-4 mx-auto">
                        <h6 className="ml-1"><strong>Nombre de Periódico</strong></h6>
                        <input type="text" className="form-control" name="newspaper_name"
                               value={this.state.newspaper_name} onChange={this.onChange} />
                    </div>
                    <div className="col-3 mx-auto">
                        <h6 className="ml-1"><strong>Sección (Letra)</strong></h6>
                        <input type="text" className="form-control" name="section"
                               value={this.state.section} onChange={this.onChange} />
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form" >
                    <div className="col-5 mx-auto">
                        <h6 className="ml-1"><strong>Fecha (Actual: {FormatDate(this.props.obj.date)})</strong></h6>
                        <input type="date" className="form-control" name="date"
                               value={this.state.date} onChange={this.onChange} />
                    </div>

                    <div className="col-2 mx-auto">
                        <label className="ml-1"><strong>Páginas</strong></label>
                        <input type="text" className="form-control" name="pages"
                               value={this.state.pages} onChange={this.onChange} />
                    </div>

                    <div className="col-5 mx-auto">
                        <h6 className="mt-1"><strong>Tipo de trabajo</strong></h6>
                        <select className="form-control" name="type_job"
                                value={this.state.type_job} onChange={this.onChange} >
                            <option key={0}>Reseña</option>
                            <option key={1}>Editorial</option>
                        </select>
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form mb-4" >
                    <div className="col-5 mx-auto">
                        <h6 className="mt-1"><strong>Seleccionar probatorio</strong></h6>
                        <input type="file" className="form-control" name="user" accept="application/pdf"
                               value={this.file} onChange={this.onChangeFile}/>
                    </div>
                    <div className="col-7 mx-auto mt-4 pt-2">
                        <button className="btn btn-primary float-right">
                            Guardar
                        </button>
                        <button onClick={this.props.onEnableEdit}
                                className="btn btn-danger float-right mr-3" >
                            Cancelar
                        </button>
                    </div>
                </div>
            </form>
        );
    }

}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps,  { edit_newspaper } )(FormNewspaperPublication);