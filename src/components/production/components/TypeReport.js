import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { get_list_types_report } from "../../../actions/production/listtypereport";


export class TypeReport extends Component {

    static propTypes = {
        reports: PropTypes.object.isRequired,
        get_list_types_report: PropTypes.func.isRequired,
        select_type: PropTypes.number
    }

    componentDidMount() {

        if (!this.props.reports.load){
            this.props.get_list_types_report();
        }
    }

    render() {

        if (this.props.reports.load){
            return (
                <select className="form-control" name="report"
                        value={this.props.select_type} onChange={this.props.onChange}>
                    {this.props.reports.list.map((report, index) => (
                        <option key={index} value={report.id}>{report.name}</option>
                        ))};
                </select>
            );
        } else {
            return <h1 className="text-center">Loading...</h1>
        }

    }
}

const mapStateToProps = (state) => ({
    reports: state.type_report
});

export default connect(
    mapStateToProps, { get_list_types_report })(TypeReport);
