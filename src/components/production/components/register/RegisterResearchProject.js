import React, { Component } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import Year from "../../../academic/components/Year";
import Departaments from "../../../academic/components/Departaments";
import { ConfigDateSave, FormatDate } from "../../../FormatDate";
import { add_research_projects } from '../../../../actions/production/add_research_projects';


export class RegisterResearchProject extends Component {

    state = {
        year: new Date().getFullYear(),
        user: '',
        file: null,
        departament:'Humanidades',
        name:'',
        participants:'',
        approval: '',
        validity:'',
        section:'',
        means:'',
        line:'',
    }

    static propTypes = {
        report: PropTypes.number.isRequired,
        add_research_projects: PropTypes.func.isRequired
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onChangeFile = (e) => this.setState({file: e.target.files[0]});
    onSubmit = (e) => {
        e.preventDefault();
        let data = new FormData()
        data.append('user', this.state.user);
        data.append('departament', this.state.departament);
        data.append('name', this.state.name);
        data.append('participants', this.state.participants);
        data.append('validity', this.state.validity);
        data.append('section', this.state.section);
        data.append('means', this.state.means);
        data.append('line', this.state.line);
        data.append('annual_report', this.state.year);

        if (this.state.approval !== ''){
            data.append('approval', FormatDate(ConfigDateSave(this.state.approval)));
        }

        try {
            data.append('file', this.state.file, this.state.file.name);
            // envío de información
            this.props.add_research_projects(data);

        } catch (e){
            if (e.TypeError === undefined){
                if (window.confirm('No se adjuntó archivo. ¿Desea continuar con el registro?')){
                    // envío de información
                    this.props.add_research_projects(data);
                }
            }
        }
    }

    render() {
        return (
            <form className="col-12" onSubmit={this.onSubmit}>
                <div className="my-1 filter-section-dsch-multi-form" >
                    <div className="col-6 mx-auto">
                        <h6 className="ml-1"><strong>Nombre de profesor responsable *</strong></h6>
                        <input type="text" className="form-control" name="user" required
                               value={this.state.user} onChange={this.onChange} />
                    </div>

                    <div className="col-6 mx-auto">
                        <h6 className="ml-1"><strong>Nombre de proyecto aprobado *</strong></h6>
                        <textarea className="form-control" name="name" required rows={2}
                               value={this.state.name} onChange={this.onChange} />
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form" >
                    <div className="col-5 mx-auto">
                        <h6 className="mt-1"><strong>Nombres de profesores participantes </strong></h6>
                        <textarea className="form-control" name="participants" rows={2}
                               value={this.state.participants} onChange={this.onChange} />
                    </div>

                     <div className="col-4 mx-auto">
                        <h6 className="ml-1"><strong>Fecha de aprobación *</strong></h6>
                        <input type="date" className="form-control" name="approval" required
                               value={this.state.approval} onChange={this.onChange} />
                    </div>

                    <div className="col-3 mx-auto">
                        <h6 className="ml-1"><strong>Vigencia</strong></h6>
                        <input type="text" className="form-control" name="validity"
                               value={this.state.validity} onChange={this.onChange} />
                    </div>

                </div>

                <div className="my-2 filter-section-dsch-multi-form" >
                    <div className="col-4 mx-auto">
                        <h6 className="ml-1"><strong>Sesión de consejo divisional</strong></h6>
                        <input type="text" className="form-control" name="section"
                               value={this.state.section} onChange={this.onChange} />
                    </div>

                    <div className="col-3 mx-auto">
                        <h6 className="ml-1"><strong>Monto de recursos</strong></h6>
                        <input type="text" className="form-control" name="means"
                               value={this.state.means} onChange={this.onChange} />
                    </div>

                    <div className="col-5 mx-auto">
                        <h6 className="mt-1"><strong>Líneas de Investigación </strong></h6>
                        <textarea className="form-control" name="line" rows={2}
                               value={this.state.line} onChange={this.onChange} />
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form mb-4" >
                    <div className="col-3 mx-auto">
                        <h6 className="ml-1"><strong>Seleccionar Año:</strong></h6>
                        <Year
                            select_year={this.state.year.toString()}
                            onChange={this.onChange}
                        />
                    </div>

                    <div className="col-4 mx-auto">
                        <h6 className="ml-1"><strong>Seleccionar Departamento:</strong></h6>
                        <Departaments
                            select_departament={this.state.departament.toString()}
                            onChange={this.onChange}
                        />
                    </div>

                    <div className="col-5 mx-auto">
                        <h6 className="mt-1"><strong>Seleccionar probatorio</strong></h6>
                        <input type="file" className="form-control" name="user" accept="application/pdf"
                               value={this.file} onChange={this.onChangeFile}/>
                    </div>

                    <div className="col-3 mx-auto mt-4 pt-2">
                        <button className="btn btn-primary float-right mr-3"> Guardar Informe </button>
                    </div>
                </div>
            </form>
        );
    }

}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, { add_research_projects })(RegisterResearchProject);