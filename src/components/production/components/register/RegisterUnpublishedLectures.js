import React, { Component } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import Year from "../../../academic/components/Year";
import axios from "axios";

import {SERVER} from "../../../../actions/server";
import { configAuthToken } from "../../../../actions/configAuthToken";
import { add_unpublished_lectures } from "../../../../actions/production/add_unpublished_lectures";


export class RegisterUnpublishedLectures extends Component {

    state = {
        year: new Date().getFullYear(),
        file: null,
        list_users: [],
        users: '',
        user: '',
        title: '',
        present: '',
        congress: '',
        city: '',
        country: '',
        institute: '',
        type_job: 'Conferencia magistral',
    }

    static propTypes = {
        report: PropTypes.number.isRequired,
        add_unpublished_lectures: PropTypes.func.isRequired
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onChangeFile = (e) => this.setState({file: e.target.files[0]});

    onClickAddName = (e) => {
        let name = this.state.users;

        if (name.trim() === ''){
            alert('Escriba el nombre del autor (profesor)');
            this.setState({ users: '' });
            return false;
        }

        if (this.state.list_users.includes(name)){
            alert(name+' Ya se agregó anteriormente');
            this.setState({ users: '' });
            return false;
        }

        axios.get(`${SERVER}/production/check/${name}/`, configAuthToken())
        .then((res) => {
            alert(res.data.detail);
            this.state.list_users.push(name);
            this.setState({user: this.state.user+','+name})
            this.setState({ users: '' });
            console.log(this.state.user);
        })
        .catch((err) => {
            try{
                switch (err.response.status){
                    case 400:
                        alert(
                            name+' '+err.response.data.detail+'. Verifique que el nombre este escrito correctamente');
                        this.setState({ users: '' });
                        break;

                    case 401:
                        alert('Su sesión caducó. Es necesario terminar la sesión');
                        break;

                    default:
                        console.log(err.response);
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    alert('NO HAY COMUNICACIÓN CON SERVIDOR');
                }
            }
        });

    }

    onSubmit = (e) => {
        e.preventDefault();
        if (this.state.list_users.length <= 0) {
            alert('Debe agregar mínimo un nombre de profesor');
            return false;
        }

        let data = new FormData()
        data.append('user', this.state.user);
        data.append('title', this.state.title);
        data.append('present', this.state.present);
        data.append('congress', this.state.congress);
        data.append('city', this.state.city);
        data.append('country', this.state.country);
        data.append('institute', this.state.institute);
        data.append('type_job', this.state.type_job);
        data.append('annual_report', this.state.year);

        try {
            data.append('file', this.state.file, this.state.file.name);
            // envío de información
            this.props.add_unpublished_lectures(data);

        } catch (e){
            if (e.TypeError === undefined){
                if (window.confirm('No se adjuntó archivo. ¿Desea continuar con el registro?')){
                    // envío de información
                    this.props.add_unpublished_lectures(data);
                }
            }
        }
    }

    render() {
        return (
            <form className="col-12" onSubmit={this.onSubmit}>
                <div className="my-3 filter-section-dsch-multi-form" >
                    <div className="col-6 mx-auto">
                        <label className="ml-1"><strong>Nombre de Autor (Empezar por apellidos)</strong></label>
                        <input type="text" className="form-control" name="users"
                               value={this.state.users} onChange={this.onChange} />
                    </div>

                    <div className="col-6 mx-auto filter-section-dsch-multi-form">
                        <ul className="col-9">
                            {this.state.list_users.map((obj, index) => (
                                <li key={index}>{obj}</li>
                            ))}
                        </ul>
                        <div className="col-4">
                            <button type="button" className="btn btn-warning float-right"
                                    onClick={this.onClickAddName}>
                                Agregar Autor
                            </button>
                        </div>
                    </div>

                </div>

                <div className="my-2 filter-section-dsch-multi-form" >
                    <div className="col-4 mx-auto">
                        <h6 className="ml-1"><strong>Título del Conferencia *</strong></h6>
                        <textarea className="form-control" name="title" rows={2} required
                               value={this.state.title} onChange={this.onChange} />
                    </div>

                    <div className="col-4 mx-auto">
                        <h6 className="ml-1"><strong>Lugar de presentación *</strong></h6>
                        <textarea className="form-control" name="present" rows={2}
                               value={this.state.present} onChange={this.onChange} />
                    </div>

                    <div className="col-4 mx-auto">
                        <h6 className="ml-1"><strong>Nombre de congreso</strong></h6>
                        <input type="text" className="form-control" name="congress"
                               value={this.state.congress} onChange={this.onChange} />
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form" >

                    <div className="col-4 mx-auto">
                        <h6 className="ml-1"><strong>Cuidad</strong></h6>
                        <input type="text" className="form-control" name="city"
                               value={this.state.city} onChange={this.onChange} />
                    </div>

                    <div className="col-4 mx-auto">
                        <h6 className="ml-1"><strong>País </strong></h6>
                        <input type="text" className="form-control" name="country"
                               value={this.state.country} onChange={this.onChange} />
                    </div>

                    <div className="col-4 mx-auto">
                        <h6 className="ml-1"><strong>Institución </strong></h6>
                        <input type="text" className="form-control" name="institute"
                               value={this.state.institute} onChange={this.onChange} />
                    </div>
                </div>

                <div className="my-2 filter-section-dsch-multi-form mb-4" >
                    <div className="col-3 mx-auto">
                        <h6 className="ml-1"><strong>Seleccionar año</strong></h6>
                        <Year
                            select_year={this.state.year.toString()}
                            onChange={this.onChange}
                        />
                    </div>

                    <div className="col-4 mx-auto">
                        <h6 className="ml-1"><strong>Tipo de trabajo</strong></h6>
                        <select className="form-control" name="type_job"
                                value={this.state.type_job} onChange={this.onChange} >
                            <option key={0}>Conferencia</option>
                            <option key={1}>Conferencia magistral</option>
                            <option key={2}>Trabajo en evento especializado</option>
                        </select>
                    </div>

                    <div className="col-5 mx-auto">
                        <h6 className="mt-1"><strong>Seleccionar probatorio</strong></h6>
                        <input type="file" className="form-control" name="user" accept="application/pdf"
                               value={this.file} onChange={this.onChangeFile}/>
                    </div>
                    <div className="col-3 mx-auto mt-4 pt-2">
                        <button className="btn btn-primary float-right"> Guardar Informe </button>
                    </div>
                </div>
            </form>
        );
    }

}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps,  { add_unpublished_lectures } )(RegisterUnpublishedLectures);