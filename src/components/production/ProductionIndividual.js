import React, { Component } from 'react';
import PropTypes from "prop-types";

import RegisterTeacherTraining from "./components/register/RegisterTeacherTraining";
import RegisterExchangeActivity from "./components/register/RegisterExchangeActivity";
import RegisterAcademicEvent from "./components/register/RegisterAcademicEvent";
import RegisterAcademicCollaboration from "./components/register/RegisterAcademicCollaboration";
import RegisterProjectCollaboration from "./components/register/RegisterProjectCollaboration";
import RegisterMagazinePublications from "./components/register/RegisterMagazinePublications";
import RegisterElectronicJournals from "./components/register/RegisterElectronicJournals";
import RegisterNewspaperPublication from "./components/register/RegisterNewspaperPublication";
import RegisterPublishedBooks from "./components/register/RegisterPublishedBooks";
import RegisterChaptersBooks from "./components/register/RegisterChaptersBooks";
import RegisterBookReviews from "./components/register/RegisterBookReviews";
import RegisterPublishedLectures from "./components/register/RegisterPublishedLectures";
import RegisterUnpublishedLectures from "./components/register/RegisterUnpublishedLectures";
import RegisterResearchProject from "./components/register/RegisterResearchProject";


export class ProductionIndividual extends Component {

    static propTypes = {
        report: PropTypes.number.isRequired
    }

    render() {
        return (
            <section className="filter-section-dsch-multi-form ">
                <section className="col-8 mx-auto">
                    <section className="card mt-3">
                        <div className="modal-header title-detail">
                            <h5 className="m-auto">
                                <strong>
                                    Registro de informe individual -
                                    {
                                        this.props.report === 1
                                            ? ' Actualización y Formación Docente'
                                            :
                                        this.props.report === 2
                                            ? ' Actividad de Intercambio'
                                            :
                                        this.props.report === 3
                                            ? ' Eventos Académicos'
                                            :
                                        this.props.report === 4
                                            ? ' Redes de Colaboración Académica'
                                            :
                                        this.props.report === 5
                                            ? ' Colaboración de Proyectos'
                                            :
                                        this.props.report === 6
                                            ? ' Publicaciones en revistas'
                                            :
                                        this.props.report === 7
                                            ? ' Revistas Electrónicas'
                                            :
                                        this.props.report === 8
                                            ? ' Publicaciones en Periódicos'
                                            :
                                        this.props.report === 9
                                            ? ' Libros Publicados'
                                            :
                                        this.props.report === 10
                                            ? ' Capítulos de Libros'
                                            :
                                        this.props.report === 11
                                            ? ' Reseñas de Libros'
                                            :
                                        this.props.report === 12
                                            ? ' Conferencias Publicadas'
                                            :
                                        this.props.report === 13
                                            ? ' Conferencias no Publicadas'
                                            :
                                        this.props.report === 14
                                            ? ' Proyectos de Investigación Aprobados'
                                            :
                                            null
                                    }
                                </strong>
                            </h5>
                        </div>
                        <div >
                            {
                                this.props.report === 1
                                    ? <RegisterTeacherTraining  report={this.props.report}/>
                                    :
                                this.props.report === 2
                                    ? <RegisterExchangeActivity report={this.props.report} />
                                    :
                                this.props.report === 3
                                    ? <RegisterAcademicEvent report={this.props.report} />
                                    :
                                this.props.report === 4
                                    ? <RegisterAcademicCollaboration  report={this.props.report}/>
                                    :
                                this.props.report === 5
                                    ? <RegisterProjectCollaboration report={ this.props.report} />
                                    :
                                this.props.report === 6
                                    ? <RegisterMagazinePublications report={this.props.report} />
                                    :
                                this.props.report === 7
                                    ? <RegisterElectronicJournals report={this.props.report} />
                                    :
                                this.props.report === 8
                                    ? <RegisterNewspaperPublication report={this.props.report} />
                                    :
                                this.props.report === 9
                                    ? <RegisterPublishedBooks report={this.props.report} />
                                    :
                                this.props.report === 10
                                    ? <RegisterChaptersBooks report={this.props.report} />
                                    :
                                this.props.report === 11
                                    ? <RegisterBookReviews report={this.props.report} />
                                    :
                                this.props.report === 12
                                    ? <RegisterPublishedLectures report={this.props.report} />
                                    :
                                this.props.report === 13
                                    ? <RegisterUnpublishedLectures report={this.props.report} />
                                    :
                                this.props.report === 14
                                    ? <RegisterResearchProject report={this.props.report}/>
                                    :
                                    null
                            }
                        </div>
                    </section>
                </section>
            </section>
        )
    }
}

export default ProductionIndividual;