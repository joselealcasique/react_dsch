import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import {get_list_scholarship_number} from '../../../actions/scholarship/get_list_scholarship-number';



export class ListScholarshipPersonal extends Component {

    static propTypes = {
        scholarship: PropTypes.array,
        get_list_scholarship_number: PropTypes.func.isRequired,
        user: PropTypes.object.isRequired,
        select_request_scholarship: PropTypes.string.isRequired
    }

    componentDidMount() {
        if (this.props.scholarship === null){
            this.props.get_list_scholarship_number(this.props.user.user, 5);
        }

    }

    render() {

        if (this.props.scholarship !== null){
            return (
                <select className="form-control" name="request_scholarship"
                        value={this.props.select_request_scholarship} onChange={this.props.onChange}>
                    <option key={0}>..................</option>
                    {this.props.scholarship.map((request) => (
                        <option key={request.id}>{request.number_request}</option>
                        ))};
                </select>
            );
        } else {
            return <h1 className="text-center">Loading...</h1>
        }

    }
}

const mapStateToProps = (state) => ({
    scholarship: state.history.scholarship,
    user: state.auth.user
});

export default connect(
    mapStateToProps, { get_list_scholarship_number })(ListScholarshipPersonal);
