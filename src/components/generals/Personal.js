import React , { Component, Fragment } from 'react';
import {connect} from "react-redux";
import PropTypes from "prop-types";

import PersonalHistoryAcademic from "./components/PersonalHistoryAcademic";
import Year from "../academic/components/Year";
import ListScholarshipPersonal from "./components/ListScholarshipPersonal";
import ViewRequestScholarship from "./components/ViewRequestScholarship";

import {
    get_history_academic_for_user_and_for_number
} from '../../actions/generals/get_history_academic_for_user_and_number'


export class Personal extends Component {

    state = {
        year: new Date().getFullYear(),
        request_scholarship: '',
        view_request: '',
    }

    static propTypes = {
        get_history_academic_for_user_and_for_number: PropTypes.func.isRequired,
    };

    onChange = (e) => {
        this.setState({[e.target.name]: e.target.value});
    }

    onLoadHistoryYear = (e) => {
        this.props.get_history_academic_for_user_and_for_number(this.state.year);
    }

    onViewScholarship = (e) => {
        this.setState({ view_request: this.state.request_scholarship });
    }

    render() {
        return (
            <Fragment>
                <div className="alert alert-info mt-2">
                    <h2 className="text-center">
                        Sistema de Información de la División de Ciencias Sociales y Humanidades
                    </h2>
                </div>
                <section className="mt-1 ml-auto mr-auto filter-section-dsch-multi-form">
                    <section className="col-4">
                        <div className="card card-body filter-section-dsch-multi-form alert-danger">
                            <h5 className="text-siz">
                                Seleccione el Año para Mostrar Historial
                            </h5>
                            <div className="filter-section-dsch-multi-form mt-3">
                                <Year
                                    select_year={this.state.year.toString()}
                                    onChange={this.onChange}
                                />
                                <div className="col-1"></div>
                                <button className="col-2 btn btn-dark" data-toggle="modal"
                                        data-target="#HistoryAcademic"
                                        onClick={this.onLoadHistoryYear}>
                                    Ver
                                </button>
                            </div>
                        </div>

                        <div className="card card-body filter-section-dsch-multi-form mt-3 alert-warning">
                            <h5>
                                Seleccione Solicitud de Beca a Mostar
                            </h5>
                            <div className="filter-section-dsch-multi-form mt-3">
                                <ListScholarshipPersonal
                                    select_request_scholarship={
                                        this.state.request_scholarship.toString()}
                                    onChange={this.onChange}
                                />
                                <div className="col-1"></div>
                                <button className="col-2 btn btn-dark" onClick={this.onViewScholarship}>
                                    Ver
                                </button>
                            </div>
                        </div>
                    </section>


                    <section
                        className="col-8 filter-section-dsch-multi-form card card-body alert-warning">
                        <ViewRequestScholarship
                            view_request={this.state.view_request}
                        />
                    </section>
                </section>
                <PersonalHistoryAcademic
                    viewYear={parseInt(this.state.year)}
                />
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
});


export default connect(
    mapStateToProps, {get_history_academic_for_user_and_for_number})(Personal);
