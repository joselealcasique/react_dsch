import React, { Component } from "react";
import PropTypes from "prop-types";
import CKEditor from "ckeditor4-react";

import { config } from "./CkeditorConfig";


export class Protocol extends Component {

    static propTypes = {
        onChangeView: PropTypes.func.isRequired,
        onChangeEditor: PropTypes.func.isRequired,
        onChecked: PropTypes.func.isRequired,
        onAddProduct: PropTypes.func.isRequired,
        onDeleteProduct: PropTypes.func.isRequired,
        onAddObjetive: PropTypes.func.isRequired,
        onDeleteObjetive: PropTypes.func.isRequired,
        title: PropTypes.string,
        objetive: PropTypes.string,
        hypothesis: PropTypes.string,
        question: PropTypes.string,
        initial: PropTypes.string,
        fin: PropTypes.string,
        hypothesis_check: PropTypes.bool,
        question_check: PropTypes.bool,
        duration: PropTypes.number,
        approach: PropTypes.string,
        data_project: PropTypes.string,
        justify_duration: PropTypes.string,
        methodology_check: PropTypes.bool,
        methodology: PropTypes.string,
        product: PropTypes.string,
        products: PropTypes.array,
        bonding_check: PropTypes.bool,
        bonding: PropTypes.string,
        sources: PropTypes.string,
        objetive_s: PropTypes.string,
        list_objective: PropTypes.array,
    }

    render () {
        return (
            <section className="mt-2 p-1">
                <div className="col-12 filter-section-dsch-multi-form">
                    <h4 className="text-right mb-4 col-8">
                        <strong>Protocolo de Investigación</strong>
                    </h4>

                    <div className="col-4 mx-auto">
                        <button className="btn-dsch float-right"
                                onClick={this.props.onChangeView.bind(this, 1)}>
                            Regresar Pre-Solicitud
                        </button>
                    </div>
                </div>

                <div className="col-12 mx-auto card mt-2 py-2 section-protocol">
                    <div className="col-12">
                        <h6><strong>Título de proyecto</strong></h6>
                        <textarea className="form-control py-1" name="title" rows={3}
                                  style={{'fontSize': '16px'}} onChange={this.props.onChange}
                                  autoFocus required value={this.props.title}/>
                    </div>

                    <div className="col-12 mt-4">
                        <h6><strong>Resumen</strong></h6>
                        <CKEditor
                            data={this.props.data_project}
                            onChange={this.props.onChangeEditor}
                            name="data_project"
                            config={ config }
                        />
                    </div>

                    <div className="col-12 mt-4">
                        <h6><strong>Planteamiento de problema</strong></h6>
                        <CKEditor
                            data={this.props.approach}
                            onChange={this.props.onChangeEditor}
                            name="approach"
                            config={ config }

                        />
                    </div>

                    <div className="col-12 mt-4">
                        <h6><strong>Objetivo</strong></h6>
                        <CKEditor
                            data={this.props.objetive}
                            onChange={this.props.onChangeEditor}
                            name="objetive"
                            config={ config }
                        />
                    </div>

                    <div className="col-12 filter-section-dsch-multi-form mt-4">
                        <div className="col-6 card py-2">
                            <h6>
                                <strong>
                                    Objetivos Específicos
                                </strong>
                            </h6>
                            <div className="filter-section-dsch-multi-form">
                                <textarea className="form-control py-1 col-10" rows={2} name="objetive_s"
                                          style={{'fontSize': '16px'}} onChange={this.props.onChange}
                                          value={this.props.objetive_s}
                                />
                                <div className="col-2">
                                    <button className="btn-dsch mt-3"
                                            onClick={this.props.onAddObjetive}>Agregar</button>
                                </div>
                            </div>
                        </div>

                        <div className="col-6 card pt-2 options-products">
                            {
                                this.props.list_objective.map((obj, index) => (
                                    <div key={index} className="card mb-1 py-1">
                                        <div className="py-1 filter-section-dsch-multi-form">
                                            <div className="col-10">{ obj }</div>
                                            <div className="col-2">
                                                <button className="btn btn-danger float-right"
                                                        onClick={
                                                            this.props.onDeleteObjetive.bind(this, obj)}>
                                                    X
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                ))
                            }
                        </div>
                    </div>

                    <div className="filter-section-dsch-multi-form mt-3">
                        <div className="col-8">
                            <h6><strong>Seleccionar opción para habilitar</strong></h6>
                            <div className="filter-section-dsch-multi-form">
                                <div className="form-check col-2">
                                    <input type="checkbox" className="form-check-input"
                                           name="hypothesis_check" value={this.props.hypothesis_check}
                                           onChange={this.props.onChecked}
                                           defaultChecked={this.props.hypothesis_check}/>
                                    <label className="form-check-label" htmlFor="hypothesis_check">
                                        Hipótesis
                                    </label>
                                </div>

                                <div className="form-check col-2">
                                    <input type="checkbox" className="form-check-input"
                                           name="question_check" value={this.props.question_check}
                                           onChange={this.props.onChecked}
                                           defaultChecked={this.props.question_check}/>
                                    <label className="form-check-label" htmlFor="question_check">
                                        Pregunta
                                    </label>
                                </div>

                                <div className="form-check col-2">
                                    <input type="checkbox" className="form-check-input"
                                           name="methodology_check" value={this.props.methodology_check}
                                           onChange={this.props.onChecked}
                                           defaultChecked={this.props.methodology_check}/>
                                    <label className="form-check-label" htmlFor="methodology_check">
                                        Metodología
                                    </label>
                                </div>

                                <div className="form-check col-4">
                                    <input type="checkbox" className="form-check-input"
                                           name="bonding_check" value={this.props.bonding_check}
                                           onChange={this.props.onChecked}
                                           defaultChecked={this.props.bonding_check}/>
                                    <label className="form-check-label" htmlFor="bonding_check">
                                        Vinculación con servicio social
                                    </label>
                                </div>
                            </div>

                        </div>
                    </div>

                    {
                        this.props.hypothesis_check
                            ?
                            <div className="filter-section-dsch-multi-form mt-2">
                                <div className="col-12">
                                    <h6><strong>Hipótesis de proyecto</strong></h6>
                                    <CKEditor
                                        data={this.props.hypothesis}
                                        onChange={this.props.onChangeEditor}
                                        name="hypothesis"
                                        config={ config }

                                    />
                                </div>
                            </div>
                            :
                            null
                    }

                    {
                        this.props.question_check
                            ?
                            <div className="filter-section-dsch-multi-form mt-2">
                                <div className="col-12 mx-auto">
                                    <h6><strong>Pregunta de investigación</strong></h6>
                                    <CKEditor
                                        data={this.props.question}
                                        onChange={this.props.onChangeEditor}
                                        name="question"
                                        config={ config }

                                    />
                                </div>
                            </div>
                            :
                            null
                    }

                    {
                        this.props.methodology_check
                            ?
                            <div className="filter-section-dsch-multi-form mt-2">
                                <div className="col-12">
                                    <h6><strong>Metodología de proyecto</strong></h6>
                                    <CKEditor
                                        data={this.props.methodology}
                                        onChange={this.props.onChangeEditor}
                                        name="methodology"
                                        config={ config }

                                    />
                                </div>
                            </div>
                            :
                            null
                    }

                    {
                        this.props.bonding_check
                            ?
                            <div className="filter-section-dsch-multi-form mt-2">
                                <div className="col-12">
                                    <h6><strong>Vinculación con servicio social</strong></h6>
                                    <CKEditor
                                        data={this.props.bonding}
                                        onChange={this.props.onChangeEditor}
                                        name="bonding"
                                        config={ config }

                                    />
                                </div>
                            </div>
                            :
                            null
                    }

                    <div className="col-12 mt-4">
                        <h6>
                            <strong>
                                Justificación de duración ({Math.round(this.props.duration)} años)
                            </strong>
                        </h6>
                        {
                            this.props.duration === 0
                                ?
                                <div className="alert alert-dark card">
                                    Para habilitar esta opción, primero debe definir
                                    fechas de inicio y finalización para el proyecto
                                </div>
                                :
                                <CKEditor
                                    data={this.props.justify_duration}
                                    onChange={this.props.onChangeEditor}
                                    name="justify_duration"
                                    config={ config }

                                />
                        }
                    </div>

                    <div className="col-12 filter-section-dsch-multi-form mt-2">
                        <div className="col-7 card py-2">
                            <h6>
                                <strong>
                                    Productos de investigación esperados
                                </strong>
                            </h6>
                            <div className="filter-section-dsch-multi-form">
                                <textarea className="form-control py-1 col-10" rows={2} name="product"
                                          style={{'fontSize': '16px'}} onChange={this.props.onChange}
                                          value={this.props.product}
                                          placeholder={
                                              this.props.duration === 0
                                                  ?
                                                  'Para habilitar esta opción, primero debe definir ' +
                                                  'fechas de inicio y finalización para el proyecto'
                                                  :
                                                  ''
                                          }
                                          disabled={this.props.duration === 0 ? true : false}
                                />
                                <div className="col-2">
                                    <button className="btn-dsch mt-3"
                                            onClick={this.props.onAddProduct}>Agregar</button>
                                </div>
                            </div>
                            <div className="ml-2 text-danger">
                                <small>
                                    {
                                        this.props.products.length > 0
                                            ?
                                            'Productos agregados actualmente: ' +
                                            this.props.products.length.toString()
                                            :
                                            null
                                    }
                                </small>
                            </div>
                        </div>
                        <div className="col-5 card pt-2 options-products">
                            {
                                this.props.products.map((obj, index) => (
                                    <div key={index} className="card mb-1 py-1">
                                        <div className="py-1 filter-section-dsch-multi-form">
                                            <div className="col-10">{ obj }</div>
                                            <div className="col-2">
                                                <button className="btn btn-danger float-right"
                                                        onClick={
                                                            this.props.onDeleteProduct.bind(this, obj)}>
                                                    X
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                ))
                            }
                        </div>
                    </div>

                    <div className="filter-section-dsch-multi-form mt-4">
                        <div className="col-12">
                            <h6><strong>Fuentes de Financiamiento</strong></h6>
                            <CKEditor
                                data={this.props.sources}
                                onChange={this.props.onChangeEditor}
                                name="sources"
                                config={ config }
                            />
                        </div>

                    </div>

                </div>
            </section>
        )
    }
}

export default Protocol;
