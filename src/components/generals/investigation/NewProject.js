import React, { Component } from "react";
import {connect} from "react-redux";
import moment from "moment";
import PropTypes from "prop-types";
import store from "../../../store";

import {AUTH_ERROR, CREATE_PROJECT_INVESTIGATION, SERVER_CONNECTION_REFUSED} from "../../../actions/types";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import { faUsers, faFileWord, faFilePdf, faCalendarCheck} from "@fortawesome/free-solid-svg-icons";

import Project from "./Project";
import Protocol from "./Protocol";
import Managers from "./Managers";
import Schedule from "./Schedule";
import ModalViewReport from "./ModalViewReport";

import { get_project_investigation } from "../../../actions/generals/get_project_investigation";
import { send_project_investigation } from "../../../actions/generals/send_project_investigation";
import {ConfigDateSave} from "../../FormatDate";
import axios from "axios";
import {SERVER} from "../../../actions/server";
import {configAuthToken} from "../../../actions/configAuthToken";
import {createMessage, returnErrors} from "../../../actions/messages";


export class NewProject extends Component {

    state = {
        view: 1,
        pk: 0,
        hypothesis_check: false,
        question_check: false,
        title: '',
        objetive: '',
        hypothesis: '',
        question: '',
        data_project: '',
        initial: '',
        fin: '',
        duration: 0,
        justify_duration: '',
        approach: '',
        methodology_check: false,
        methodology: '',
        product: '',
        products: [],
        bonding_check: false,
        bonding: '',
        sources: '',
        users: '',
        list_users: [],
        grade: '',
        list_grade: [],
        participant_internal: '',
        list_participant_internal: [],
        participant_internal_grade: '',
        list_participant_internal_grade: [],
        participant_external_grade: '',
        participant_external_institution: '',
        participant_external_name: '',
        participants_external: [],
        collaborator_name: '',
        collaborator_institution: '',
        list_collaborators: [],
        date_initial: 0,
        date_final: 0,
        name_schedule: '',
        list_schedule: [],
        phone: '',
        objetive_s: '',
        list_objective: []
    }

    static propTypes = {
        user: PropTypes.object,
        investigation: PropTypes.object.isRequired,
        get_project_investigation: PropTypes.func.isRequired,
        send_project_investigation: PropTypes.func.isRequired,
    }

    componentDidMount() {
        const params = this.props.location.state;
        if (params.edit){
            if (!params.is_sketch & !params.obj.is_editable){
                alert('Esta Solicitud fue enviada, no es editable');
                return this.props.history.push('/project-list');

            } else {
                this.setDataFromPropsToState(params.obj);
                alert('Información cargada correctamente');
            }

        } else {
            let grade_initial = '';
            let phone_initial = '';

            while (true){
                grade_initial = window.prompt('Escriba su Grado Académico');
                if (grade_initial !== '' & grade_initial !== null) break;
            }

            while (true){
                phone_initial = window.prompt('Escriba su número telefónico');
                if (phone_initial !== '' & phone_initial !== null) break;
            }

            this.state.list_users.push(this.props.user);
            this.state.list_grade.push({
                number: this.props.user.user, grade: grade_initial, phone: phone_initial
            })
        }
    }

    setDataFromPropsToState = (obj) => {

        this.setState({
            pk: obj.id,
            hypothesis_check: obj.hypothesis === "" ? false : true,
            question_check: obj.question === "" ? false : true,
            title: obj.title,
            objetive: obj.objetive,
            hypothesis: obj.hypothesis,
            question: obj.question,
            initial: obj.initial === null ? '' : obj.initial,
            fin: obj.fin === null ? '' : obj.fin,
            date_initial: new Date(obj.initial).getFullYear(),
            date_final: new Date(obj.fin).getFullYear(),
            duration: parseInt(obj.duration),
            justify_duration: obj.protocol.justify_duration,
            approach: obj.protocol.approach,
            methodology_check: obj.protocol.methodology === "" ? false : true,
            methodology: obj.protocol.methodology,
            products: obj.protocol.products,
            bonding_check: obj.protocol.bonding === "" ? false : true,
            bonding: obj.protocol.bonding,
            sources: obj.protocol.sources,
            list_users: obj.author,
            list_grade: obj.grade_author,
            list_participant_internal: obj.participants_internal,
            list_participant_internal_grade: obj.grade_participants_internal,
            participants_external: obj.participants_external,
            list_collaborators: obj.protocol.collaborators,
            list_objective: obj.protocol.objetive_s,
            list_schedule: obj.protocol.schedule,
            data_project: obj.protocol.data_project
        });
    }

    onChangeView = (option) => this.setState({ view: option});
    onChangeEditor = (e) => {
        this.setState({ [e.editor.name]: e.editor.getData()});
    }

    onChange = (e) => {
        this.setState({[e.target.name]: e.target.value});

        if (e.target.name === 'initial' | e.target.name === 'fin'){
            if (e.target.name === 'initial' & this.state.fin.trim() !== ''){
                this.onCalculateDuration(e.target.value, this.state.fin);

            } else if (e.target.name === 'fin' & this.state.initial.trim() !== ''){
                this.onCalculateDuration(this.state.initial, e.target.value);
            }
        }
    }
    onChecked = (e) => {
        this.setState({[e.target.name]: e.target.checked});

        if (e.target.name === 'hypothesis_check' & !e.target.checked){
            this.setState({ hypothesis: '' });
        }

        if (e.target.name === 'question_check' & !e.target.checked){
            this.setState({ question: '' });
        }

        if (e.target.name === 'methodology_check' & !e.target.checked){
            this.setState({ methodology: '' });
        }

        if (e.target.name === 'bonding_check' & !e.target.checked){
            this.setState({ bonding: '' });
        }
    }

    onAddObjetive = () => {
        if (this.state.objetive_s.trim() !== ''){
            this.state.list_objective.push(this.state.objetive_s.toString());
            this.setState({ objetive_s: ''});

        } else {
            alert('No se agregó información');
        }
    }

    onDeleteObjetive = (name) => {
        let new_array = this.state.list_objective.filter( function (item, index){
            if (item !== name) {
                return item;
            }
            return null

        });
        this.setState({ list_objective: new_array});
    }

    onAddProduct = () => {
        if (this.state.product.trim() !== ''){
            this.state.products.push(this.state.product.toString());

        } else {
            alert('No se agregó información');
        }
        this.setState({ product: ''});
    }

    onDeleteProduct = (name) => {
        let new_array = this.state.products.filter( function (item, index){
            if (item !== name) {
                return item;
            }
            return null

        });
        this.setState({ products: new_array});
    }

    onCalculateDuration = (initial, fin) => {
        let date_initial = moment(initial);
        let date_fin = moment(fin);
        let months = date_fin.diff(date_initial, 'months')
        let years = months / 12;
        this.setState({
            duration: years,
            date_initial: parseInt(new Date(initial).getFullYear()),
            date_final: parseInt(new Date(fin).getFullYear())
        });

        let new_array = this.state.list_schedule.filter(
            function (item, index){
                if (item.final >= parseInt(new Date(initial).getFullYear())) {
                    return item;
                }
                return null
        });

        this.setState({ list_schedule: new_array});

    }

    onSearchExistUser = (obj) => {
        if (this.state.list_users.length <= 0){
            return true;

        } else {
            let validated =  this.state.list_users.find(element => element.user === obj.user)

            if (typeof validated === 'undefined'){
                return true;

            } else {
                return false;
            }
        }
    }
    onSearchExistUserParticipant = (obj) => {
        if (this.state.list_participant_internal.length <= 0){
            return true;

        } else {
            let validated =  this.state.list_participant_internal.find(
                element => element.user === obj.user)

            if (typeof validated === 'undefined'){
                return true;

            } else {
                return false;
            }
        }
    }
    onSearchExistUserExternal = (name) => {
        if (this.state.participants_external.length <= 0){
            return true;

        } else {
            let validated =  this.state.participants_external.find(
                element => element.name === name)

            if (typeof validated === 'undefined'){
                return true;

            } else {
                return false;
            }
        }
    }
    onSearchExistUserCollaborator = (name) => {
        if (this.state.list_collaborators.length <= 0){
            return true;

        } else {
            let validated =  this.state.list_collaborators.find(
                element => element.name === name)

            if (typeof validated === 'undefined'){
                return true;

            } else {
                return false;
            }
        }
    }

    onClickAddNameAuthor = (e) => {
        let name = this.state.users;

        if (name.trim() === ''){
            alert('Escriba el nombre del autor (profesor)');
            this.setState({ users: '' });
            return false;
        }

        if (this.state.grade.trim() === ''){
            alert('Agregar Grado académico');
            this.setState({ grade: '' });
            return false;
        }

        if (this.state.phone.trim() === ''){
            alert('Agregar Número de contacto');
            this.setState({ phone: '' });
            return false;
        }

        axios.get(`${SERVER}/investigation/check/${name}/`, configAuthToken())
            .then((res) => {
                let option = this.onSearchExistUser(res.data);

                if (option){
                    alert('Usuario Agregado');
                    this.state.list_users.push(res.data);
                    this.state.list_grade.push(
                        { number: res.data.user, grade: this.state.grade, phone: this.state.phone }
                    );
                    this.setState({ users: '', grade: '', phone: '' });

                } else {
                    alert('Usuario agregado anteriormente');
                    this.setState({ users: '', grade: '', phone: '' });
                }
        })
        .catch((err) => {
            try{
                switch (err.response.status){
                    case 400:
                        alert(
                            name+' '+err.response.data.detail+'. Verifique que el nombre este escrito correctamente');
                        this.setState({ users: '', grade: '' });
                        break;

                    case 401:
                        if (err.response.data.detail) store.dispatch(
                            returnErrors(err.response.data.detail, 401));
                        store.dispatch({type: AUTH_ERROR})
                        break;

                    default:
                        console.log(err.response);
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    store.dispatch({type: SERVER_CONNECTION_REFUSED})
                }
            }
        });
    }
    onDeleteNameAuthor = (number) => {
        let new_array = this.state.list_users.filter( function (item, index){
            if (item.user !== number) {
                return item;
            }
            return null

        });

        let new_array_grade = this.state.list_grade.filter( function (item, index){
            if (item.number !== number) {
                return item;
            }
            return null

        });
        this.setState({ list_users: new_array, list_grade: new_array_grade});
    }

    onClickAddNameParticipant = (e) => {
        let name = this.state.participant_internal;

        if (name.trim() === ''){
            alert('Escriba el nombre del autor (profesor)');
            this.setState({ participant_internal: '' });
            return false;
        }

        if (this.state.participant_internal_grade.trim() === ''){
            alert('Agregar Grado académico');
            this.setState({ participant_internal_grade: '' });
            return false;
        }

        axios.get(`${SERVER}/investigation/check/${name}/`, configAuthToken())
            .then((res) => {
                let option = this.onSearchExistUserParticipant(res.data);

                if (option){
                    alert('Participante Agregado');
                    this.state.list_participant_internal.push(res.data);
                    this.state.list_participant_internal_grade.push(
                        { number: res.data.user, grade: this.state.participant_internal_grade });
                    this.setState({ participant_internal: '', participant_internal_grade: '' });

                } else {
                    alert('Participante agregado anteriormente');
                    this.setState({ participant_internal: '', participant_internal_grade: '' });
                }
        })
        .catch((err) => {
            try{
                switch (err.response.status){
                    case 400:
                        alert(
                            name+' '+err.response.data.detail+'. Verifique que el nombre este escrito correctamente');
                        this.setState({ participant_internal: '', participant_internal_grade: '' });
                        break;

                    case 401:
                        if (err.response.data.detail) store.dispatch(
                            returnErrors(err.response.data.detail, 401));
                        store.dispatch({type: AUTH_ERROR})
                        break;

                    default:
                        console.log(err.response);
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    store.dispatch({type: SERVER_CONNECTION_REFUSED})
                }
            }
        });

    }
    onDeleteNameParticipant = (number) => {
        let new_array = this.state.list_participant_internal.filter(
            function (item, index){
                if (item.user !== number) {
                    return item;
                }
                return null
        });

        let new_array_grade = this.state.list_participant_internal_grade.filter(
            function (item, index){
                if (item.number !== number) {
                    return item;
                }
                return null
            });
        this.setState({
            list_participant_internal: new_array,
            list_participant_internal_grade: new_array_grade
        });
    }

    onClickAddNameExternal = (e) => {
        if (this.state.participant_external_name.trim() === ''){
            alert('Escriba el nombre del autor (profesor)');
            this.setState({ participant_external_name: '' });
            return false;
        }

        if (this.state.participant_external_grade.trim() === ''){
            alert('Agregar Grado académico');
            this.setState({ participant_internal_grade: '' });
            return false;
        }

        if (this.state.participant_external_institution.trim() === ''){
            alert('Agregar Grado académico');
            this.setState({ participant_external_institution: '' });
            return false;
        }

        let option = this.onSearchExistUserExternal(this.state.participant_external_name);

        if (option){
            alert('Participante Externo Agregado');
            let external = ({
                name: this.state.participant_external_name,
                institution: this.state.participant_external_institution,
                grade: this.state.participant_external_grade
            })
            this.state.participants_external.push(external);
            this.setState({
                participant_external_name: '',
                participant_external_institution: '' ,
                participant_external_grade: ''
            });

        } else {
            alert('Participante externo agregado anteriormente');
            this.setState({
                participant_external_name: '',
                participant_external_institution: '' ,
                participant_external_grade: ''
            });
        }
    }
    onDeleteNameExternal = (name) => {
        let new_array = this.state.participants_external.filter(
            function (item, index){
                if (item.name !== name) {
                    return item;
                }
                return null
        });

        this.setState({ participants_external: new_array});
    }

    onClickAddNameCollaborator = (e) => {
        if (this.state.collaborator_name.trim() === ''){
            alert('Escriba el nombre del colaborador');
            this.setState({ collaborator_name: '' });
            return false;
        }

        if (this.state.collaborator_institution.trim() === ''){
            alert('Escriba la institución');
            this.setState({ collaborator_institution: '' });
            return false;
        }

        let option = this.onSearchExistUserCollaborator(this.state.collaborator_name);

        if (option){
            alert('Colaborador Agregado');
            let external = ({
                name: this.state.collaborator_name,
                institution: this.state.collaborator_institution,
            })
            this.state.list_collaborators.push(external);
            this.setState({
                collaborator_name: '',
                collaborator_institution: ''
            });

        } else {
            alert('Colaborador agregado anteriormente');
            this.setState({
                collaborator_name: '',
                collaborator_institution: ''
            });
        }
    }
    onDeleteNameCollaborator = (name) => {
        let new_array = this.state.list_collaborators.filter(
            function (item, index){
                if (item.name !== name) {
                    return item;
                }
                return null
        });

        this.setState({ list_collaborators: new_array});
    }

    onSearchProductoInSchedule = (name) => {
        if (this.state.list_schedule.length <= 0){
            return true;

        } else {
            let validated =  this.state.list_schedule.find(
                element => element.name === name)

            if (typeof validated === 'undefined'){
                return true;

            } else {
                return false;
            }
        }
    }
    onAddProductoForSchedule = (e) => {
        e.preventDefault();

        if (this.state.date_initial > this.state.date_final) {
            alert("La fecha inicial no puede ser mayor que la fecha final");
            return false;
        }

        let option = this.onSearchProductoInSchedule(this.state.name_schedule);

        if (option) {
            this.state.list_schedule.push({
                name: this.state.name_schedule,
                initial: parseInt(this.state.date_initial),
                final: parseInt(this.state.date_final),
            })
            alert("Actividad agregada correctamente");
        } else {
            alert("Esta actividad ya fue agregada anteriormente");
        }
        this.setState({
            name_schedule: '',
        });
    }
    onDeleteProductForSchedule = (name) => {
        let new_array = this.state.list_schedule.filter(
            function (item, index){
                if (item.name !== name) {
                    return item;
                }
                return null
        });

        this.setState({ list_schedule: new_array});
    }

    onConfigDataJSON = () => {
        const data = JSON.stringify({
            title: this.state.title,
            objetive: this.state.objetive,
            hypothesis: this.state.hypothesis,
            question: this.state.question,
            initial: this.state.initial !== '' ? ConfigDateSave(this.state.initial) : null,
            fin: this.state.fin !== '' ? ConfigDateSave(this.state.fin) : null,
            duration: Math.round(this.state.duration),
            justify_duration: this.state.justify_duration,
            approach: this.state.approach,
            methodology: this.state.methodology,
            products: this.state.products,
            bonding: this.state.bonding,
            sources: this.state.sources,
            author: this.state.list_users,
            grade_author: this.state.list_grade,
            participants_internal: this.state.list_participant_internal,
            grade_participants_internal: this.state.list_participant_internal_grade,
            participants_external: this.state.participants_external,
            collaborators: this.state.list_collaborators,
            objetive_s: this.state.list_objective,
            schedule: this.state.list_schedule,
            data_project: this.state.data_project,
        });
        return data;
    }

    onSendDataContext = (pk) => {
        this.setState({ pk: pk});
    }

    onSendData = (e) => {
        const data = this.onConfigDataJSON();
        axios.post(`${SERVER}/investigation/add/${this.state.pk}/`, data, configAuthToken())
        .then((res) => {
            this.onSendDataContext(res.data.id)
            store.dispatch({
                type: CREATE_PROJECT_INVESTIGATION,
                payload: res.data
            });
            store.dispatch(createMessage({registerFile: 'Guardado Correctamente'}));
        })
        .catch((err) => {
            try {
                switch (err.response.status){
                    case 400:
                        if (err.response.data.title) store.dispatch(
                            returnErrors(
                                'Para poder registrar un proyecto, es necesario definir el título',
                                401));
                        break

                    case 401:
                        if (err.response.data.detail) store.dispatch(
                            returnErrors(err.response.data.detail, 401));
                            store.dispatch({ type: AUTH_ERROR });
                        break

                    default:
                        console.log(err.response);
                }
            } catch (e) {
                if (e instanceof TypeError) {
                    store.dispatch({type: SERVER_CONNECTION_REFUSED});
                    store.dispatch(createMessage({serverConnection: 'NO HAY COMUNICACIÓN CON SERVIDOR'}));
                }
            }
        });
    }

    onSendDataRequest = () => {
        if (this.state.pk === 0){
            alert('No es posible enviar una Pre-Solicitud sin contenido');
            return false;
        }
        
        if (window.confirm('Al enviar la Pre-Solicitud ya no podrá hacer cambios. ' +
            '¿Continuar con envío?')){
            this.props.send_project_investigation(this.state.pk);
            window.setTimeout(this.onRedirectListProjects, 600)

        } else {
            alert('Pre-Solicitud no enviada');
        }
    }

    onRedirectListProjects = () => {
        return this.props.history.push('/project-list')
        //return <Redirect to={{ pathname: '/project-list' }} />

    }

    render () {
        if (this.state.view === 1) {
            return (
                <section className="col-11 mx-auto card mt-4 pt-4 pb-3 px-5">
                    <h5 className="text-justify">
                        Para enviar una <strong>Pre-Solicitud</strong> de un proyecto de
                        investigación, es necesario completar las siguientes secciones con la
                        información correspondiente.
                        <strong className="text-danger"> NOTA: Después de enviar la pre-solicitud,
                        no podrá hacer cambios en la información agregada.
                        </strong>
                    </h5>

                    <div className="filter-section-dsch-multi-form mt-3">
                        <section className="mx-auto col-3">
                            <div className="modal-header bg-primary text-white">
                                <h5 className="m-auto">
                                    <strong>
                                        Hoja de Resumen
                                    </strong>
                                </h5>
                            </div>
                            <section className="modal-header b-filter">
                                <div className="col-8 alert alert-primary text-center py-3 mx-1 btn mx-auto"
                                     onClick={this.onChangeView.bind(this, 2)}>
                                    <FontAwesomeIcon icon={faFileWord} size="4x" color="blue"/>
                                    <h6 className="mt-3 text-primary">
                                        <strong>Agregar información</strong>
                                    </h6>
                                </div>
                            </section>
                        </section>

                        <section className="mx-auto col-3">
                            <div className="modal-header bg-dark text-white">
                                <h5 className="m-auto">
                                    <strong>
                                        Profesores
                                    </strong>
                                </h5>
                            </div>
                            <section className="modal-header b-filter">
                                <div className="col-8 alert alert-dark text-center py-3 mx-1 btn mx-auto"
                                     onClick={this.onChangeView.bind(this, 4)}>
                                    <FontAwesomeIcon icon={faUsers} size="4x" color="black"/>
                                    <h6 className="mt-3 text-dark">
                                        <strong>Agregar información</strong>
                                    </h6>
                                </div>
                            </section>
                        </section>

                        <section className="mx-auto col-3">
                            <div className="modal-header bg-danger text-white">
                                <h5 className="m-auto">
                                    <strong>
                                        Protocolo
                                    </strong>
                                </h5>
                            </div>
                            <section className="modal-header b-filter">
                                <div className="col-8 alert alert-danger text-center py-3 mx-1 btn mx-auto"
                                     onClick={this.onChangeView.bind(this, 3)} >
                                    <FontAwesomeIcon icon={faFilePdf} size="4x" color="red"/>
                                    <h6 className="mt-3 text-danger">
                                        <strong>Agregar información</strong>
                                    </h6>
                                </div>
                            </section>
                        </section>

                        <section className="mx-auto col-3">
                            <div className="modal-header bg-info text-white">
                                <h5 className="m-auto">
                                    <strong>
                                        Cronograma
                                    </strong>
                                </h5>
                            </div>
                            <section className="modal-header b-filter">
                                <div className="col-8 alert alert-info text-center py-3 mx-1 btn mx-auto"
                                     onClick={this.onChangeView.bind(this, 5)}>
                                    <FontAwesomeIcon icon={faCalendarCheck} size="4x" color="black"/>
                                    <h6 className="mt-3 text-info">
                                        <strong>Agregar información</strong>
                                    </h6>
                                </div>
                            </section>
                        </section>
                    </div>

                    <section className="col-9 mx-auto mt-5 filter-section-dsch-multi-form">

                        <div className="col-4">
                            <a className="btn btn-dark btn-block"
                               href={
                                   this.state.pk !== 0
                                       ?
                                       `${SERVER}/investigation/pdf/${this.state.pk}/1/`
                                       :
                                       "#"
                               } target={this.state.pk !== 0 ? "_blank" : ''} >
                                <h5><strong>Ver Avance</strong></h5>
                            </a>
                        </div>

                        <div className="col-4">
                            <button className="btn btn-primary btn-block" onClick={this.onSendData}>
                                <h5><strong>Guardar Información</strong></h5>
                            </button>
                        </div>

                        <div className="col-4">
                            <button className="btn btn-danger btn-block"
                                    onClick={this.onSendDataRequest}>
                                <h5><strong>Enviar Pre-Solicitud</strong></h5>
                            </button>
                        </div>
                    </section>

                    <ModalViewReport
                        obj={this.state}
                    />
                </section>
            );
        } else if (this.state.view === 2){
            return <Project
                onChangeView={this.onChangeView}
                onChangeEditor={this.onChangeEditor}
                title={this.state.title}
                hypothesis={this.state.hypothesis}
                question={this.state.question}
                objetive={this.state.objetive}
                fin={this.state.fin}
                initial={this.state.initial}
                hypothesis_check={this.state.hypothesis_check}
                question_check={this.state.question_check}
                onChange={this.onChange}
                onChecked={this.onChecked}
            />;
        } else if (this.state.view === 3){
            return <Protocol
                onChangeView={this.onChangeView}
                onChangeEditor={this.onChangeEditor}
                title={this.state.title}
                hypothesis={this.state.hypothesis}
                question={this.state.question}
                objetive={this.state.objetive}
                fin={this.state.fin}
                initial={this.state.initial}
                hypothesis_check={this.state.hypothesis_check}
                question_check={this.state.question_check}
                duration={this.state.duration}
                approach={this.state.approach}
                justify_duration={this.state.justify_duration}
                methodology_check={this.state.methodology_check}
                methodology={this.state.methodology}
                product={this.state.product}
                products={this.state.products}
                bonding_check={this.state.bonding_check}
                bonding={this.state.bonding}
                sources={this.state.sources}
                objetive_s={this.state.objetive_s}
                list_objective={this.state.list_objective}
                data_project={this.state.data_project}

                onChange={this.onChange}
                onChecked={this.onChecked}
                onAddProduct={this.onAddProduct}
                onDeleteProduct={this.onDeleteProduct}
                onAddObjetive={this.onAddObjetive}
                onDeleteObjetive={this.onDeleteObjetive}
            />;
        } else if (this.state.view === 4){
            return <Managers
                onChangeView={this.onChangeView}
                onChecked={this.onChecked}
                onChange={this.onChange}
                onClickAddNameAuthor={this.onClickAddNameAuthor}
                onDeleteNameAuthor={this.onDeleteNameAuthor}
                onClickAddNameParticipant={this.onClickAddNameParticipant}
                onDeleteNameParticipant={this.onDeleteNameParticipant}
                onClickAddNameExternal={this.onClickAddNameExternal}
                onDeleteNameExternal={this.onDeleteNameExternal}
                onClickAddNameCollaborator={this.onClickAddNameCollaborator}
                onDeleteNameCollaborator={this.onDeleteNameCollaborator}

                users={this.state.users}
                list_users={this.state.list_users}
                grade={this.state.grade}
                list_grade={this.state.list_grade}
                participant_internal={this.state.participant_internal}
                list_participant_internal={this.state.list_participant_internal}
                participant_internal_grade={this.state.participant_internal_grade}
                list_participant_internal_grade={this.state.list_participant_internal_grade}
                participant_external_grade={this.state.participant_external_grade}
                participant_external_institution={this.state.participant_external_institution}
                participant_external_name={this.state.participant_external_name}
                participants_external={this.state.participants_external}
                collaborator_name={this.state.collaborator_name}
                collaborator_institution={this.state.collaborator_institution}
                list_collaborators={this.state.list_collaborators}
                phone={this.state.phone}

            />
        } else if (this.state.view === 5){
            return <Schedule
                onChangeView={this.onChangeView}
                onChecked={this.onChecked}
                onChange={this.onChange}
                onAddProductoForSchedule={this.onAddProductoForSchedule}
                onDeleteProductForSchedule={this.onDeleteProductForSchedule}
                initial={this.state.initial}
                fin={this.state.fin}
                duration={this.state.duration}
                date_initial={this.state.date_initial}
                date_final={this.state.date_final}
                name_schedule={this.state.name_schedule}
                list_schedule={this.state.list_schedule}
            />
        }
    }
}

const mapStateToProps = (state) => ({
    user: state.auth.user,
    investigation: state.investigation
});

export default connect(
    mapStateToProps,  {
        get_project_investigation,
        send_project_investigation
    })(NewProject);
