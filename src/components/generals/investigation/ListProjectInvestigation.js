import React, { Component } from "react";
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import { FormatDate } from "../../FormatDate";


export class ListProjectInvestigation extends Component {

    static propTypes = {
        investigation: PropTypes.object.isRequired,
        option: PropTypes.number.isRequired,
    }

    render () {
        return (
            <div className="col-12 mx-auto mt-3">
                {
                    this.props.investigation.list.length > 0
                        ?
                        <table className="table table-striped">
                            <thead>
                            <tr>
                                <th width={350} className="text-center">Título de proyecto</th>
                                <th width={250} className="text-center">Profesores Responsables</th>
                                <th width={240} className="text-center">Fecha de Inicio</th>
                                <th width={120} className="text-center">Duración</th>
                                <th width={150} className="text-center">Estado</th>
                                <th width={100} className="text-center"></th>
                            </tr>
                            </thead>
                            <tbody className="table-scroll">
                            {
                                this.props.investigation.list.map((obj, index) => (
                                    <tr key={index} className="alert alert-primary">
                                        <td width={350} className="text-center">{obj.title}</td>
                                        <td width={250} className="text-center">
                                            {
                                                obj.author.map((user, index) => (
                                                    <li key={index} className="list-profile">
                                                        + {user.name} {user.paternal_surname} {user.maternal_surname}
                                                    </li>
                                                ))
                                            }
                                        </td>
                                        <td width={240} className="text-center">
                                            {
                                                obj.initial === null
                                                    ? 'Fecha pendiente' : FormatDate(obj.initial)
                                            }
                                        </td>
                                        <td width={120} className="text-center">
                                            {
                                                obj.duration !== '0'
                                                    ? obj.duration + ' año (s)'
                                                    : 'Pendiente'
                                            }
                                        </td>
                                        <td width={150} className="text-center">
                                            {
                                                obj.status
                                            }
                                        </td>
                                        <td width={100}>
                                            {
                                                obj.is_sketch
                                                    ?
                                                    <Link to={{
                                                        pathname: '/pre-project',
                                                        state: {
                                                            pk: obj.id,
                                                            edit: true,
                                                            obj: obj,
                                                            is_sketch: obj.is_sketch,
                                                            option: this.props.option
                                                        }
                                                    }}>
                                                        <button className="btn-dsch float-right">
                                                            Editar
                                                        </button>
                                                    </Link>
                                                    :
                                                    <Link to={{
                                                        pathname:
                                                        obj.finalized
                                                            ?
                                                            '/project-investigation-history'
                                                            :
                                                            '/view-project',
                                                        state: {
                                                            pk: obj.id,
                                                            obj: obj,
                                                            option: this.props.option
                                                        }
                                                    }}>
                                                        <button className="btn-dsch float-right">
                                                            Detalle
                                                        </button>
                                                    </Link>
                                            }

                                        </td>
                                    </tr>
                                ))
                            }
                            </tbody>
                        </table>
                        :
                        <section
                            className="mt-5 alert alert-danger col-8 mx-auto text-center py-5">
                            <h4><strong>NO HAY PROYECTOS REGISTRADOS</strong></h4>
                        </section>
                }
            </div>
        )
    }

}

const mapStateToProps = (state) => ({
    investigation: state.investigation
});

export default connect( mapStateToProps, null )(ListProjectInvestigation);