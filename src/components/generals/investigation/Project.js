import React, { Component } from "react";
import PropTypes from "prop-types";
import { FormatDate, ConfigDateSave } from "../../FormatDate";
import CKEditor from "ckeditor4-react";

import { config } from "./CkeditorConfig";


export class Project extends Component {

    static propTypes = {
        onChangeView: PropTypes.func.isRequired,
        onChecked: PropTypes.func.isRequired,
        onChangeEditor: PropTypes.func.isRequired,
        title: PropTypes.string,
        objetive: PropTypes.string,
        hypothesis: PropTypes.string,
        question: PropTypes.string,
        initial: PropTypes.string,
        fin: PropTypes.string,
        hypothesis_check: PropTypes.bool,
        question_check: PropTypes.bool,
    }

    render () {
        return (
            <section className="mt-2 p-1">
                <div className="col-12 filter-section-dsch-multi-form">
                    <h4 className="text-right mb-4 col-7"><strong>Hoja de Resumen</strong></h4>

                    <div className="col-5 mx-auto">
                        <button className="btn-dsch float-right"
                                onClick={this.props.onChangeView.bind(this, 1)}>
                            Regresar Pre-Solicitud
                        </button>
                    </div>
                </div>

                <div className="col-12 mx-auto card mt-2 py-2 section-protocol">
                    <div className="filter-section-dsch-multi-form mt-3">
                        <div className="col-8">
                            <h6><strong>Título de Proyecto</strong></h6>
                            <textarea className="form-control py-1" name="title" rows={2}
                                      style={{'fontSize': '16px'}} onChange={this.props.onChange}
                                      autoFocus required value={this.props.title}/>
                        </div>
                        <div className="col-1"></div>
                        <div className="col-3">
                            <h6><strong>Seleccionar opción para habilitar</strong></h6>
                            <div className="filter-section-dsch-multi-form">
                                <div className="form-check col-6">
                                    <input type="checkbox" className="form-check-input"
                                           name="hypothesis_check" value={this.props.hypothesis_check}
                                           onChange={this.props.onChecked}
                                           defaultChecked={this.props.hypothesis_check}/>
                                    <label className="form-check-label" htmlFor="hypothesis_check">
                                        Hipótesis
                                    </label>
                                </div>

                                <div className="form-check col-6">
                                    <input type="checkbox" className="form-check-input"
                                           name="question_check" value={this.props.question_check}
                                           onChange={this.props.onChecked}
                                           defaultChecked={this.props.question_check}/>
                                    <label className="form-check-label" htmlFor="question_check">
                                        Pregunta
                                    </label>
                                </div>
                            </div>

                        </div>
                    </div>

                    {
                        this.props.question_check
                            ?
                            <div className="col-12 mt-3">
                                <h6><strong>Pregunta de Investigación</strong></h6>
                                <CKEditor
                                    data={this.props.question}
                                    onChange={this.props.onChangeEditor}
                                    name="question"
                                    config={ config }
                                />
                            </div>
                            :
                            null
                    }

                    {
                        this.props.hypothesis_check
                            ?
                            <div className="col-12 mt-3">
                                <h6><strong>Hipótesis de Proyecto</strong></h6>
                                <CKEditor
                                    data={this.props.hypothesis}
                                    onChange={this.props.onChangeEditor}
                                    name="hypothesis"
                                    config={ config }
                                />
                            </div>
                            :
                            null
                    }

                    <div className="col-12 mt-3">
                        <h6><strong>Objetivo</strong></h6>
                        <CKEditor
                            data={this.props.objetive}
                            onChange={this.props.onChangeEditor}
                            name="objetive"
                            config={ config }
                        />
                    </div>

                    <div className="filter-section-dsch-multi-form mt-3">
                        <div className="col-4">
                            <h6><strong>Fecha de inicio de proyecto</strong></h6>
                            <input type="date" className="form-control py-1" name="initial"
                                   style={{'fontSize': '16px'}} onChange={this.props.onChange}
                                   defaultValue={this.props.initial} value={this.props.i} />
                            <div className="ml-2 text-danger">
                                <small>
                                    {
                                        this.props.initial !== ''
                                            ?
                                            <strong>
                                                Fecha Seleccionada: {
                                                FormatDate(ConfigDateSave(this.props.initial))
                                            }
                                            </strong>
                                            :
                                            null
                                    }
                                </small>
                            </div>
                        </div>

                        <div className="col-4">
                            <h6><strong>Fecha planeada para finalizar</strong></h6>
                            <input type="date" className="form-control py-1" name="fin"
                                   style={{'fontSize': '16px'}} onChange={this.props.onChange}
                                   defaultValue={this.props.fin} value={this.props.fin} />
                            <div className="ml-2 text-danger">
                                <small>
                                    {
                                        this.props.fin !== ''
                                            ?
                                            <strong>
                                                Fecha Seleccionada: {
                                                FormatDate(ConfigDateSave(this.props.fin))
                                            }
                                            </strong>
                                            :
                                            null
                                    }
                                </small>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default Project;
