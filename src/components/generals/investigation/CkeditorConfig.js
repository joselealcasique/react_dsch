export const config = {

    toolbar: [
        {
            name: 'clipboard', items: [
                'Cut', 'Copy', 'Paste', 'PasteFromWord', '-', 'Undo', 'Redo'
            ]
        },
        {
            name: 'editing', items: [
                'Scayt'
            ]
        },
        {
            name: 'basicstyles', items: [
                'Bold', 'Italic', 'Strike', 'RemoveFormat', 'Subscript', 'Superscript'
            ]
        },
        {
            name: 'paragraph', items: [
                'NumberedList', 'BulletedList', 'Outdent', 'Indent'
            ]
        },
        {
            name: 'styles', items: [
                'Styles', 'Format'
            ]
        },
        {
            name: 'tools', items: [ 'Maximize' ]
        },
        {
            name: 'colors', items: [ 'colors' ]
        }
    ],
}