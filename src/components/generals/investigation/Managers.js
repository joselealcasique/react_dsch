import React, { Component } from "react";
import PropTypes from "prop-types";


export class Managers extends Component {

    static propTypes = {
        onChangeView: PropTypes.func.isRequired,
        onChecked: PropTypes.func.isRequired,
        onClickAddNameAuthor: PropTypes.func.isRequired,
        onDeleteNameAuthor: PropTypes.func.isRequired,
        onClickAddNameParticipant: PropTypes.func.isRequired,
        onDeleteNameParticipant: PropTypes.func.isRequired,
        onClickAddNameExternal: PropTypes.func.isRequired,
        onDeleteNameExternal: PropTypes.func.isRequired,
        onClickAddNameCollaborator: PropTypes.func.isRequired,
        onDeleteNameCollaborator: PropTypes.func.isRequired,
        users: PropTypes.string,
        list_users: PropTypes.array,
        grade: PropTypes.string,
        list_grade: PropTypes.array,
        participant_internal: PropTypes.string,
        list_participant_internal: PropTypes.array,
        participant_internal_grade: PropTypes.string,
        list_participant_internal_grade: PropTypes.array,
        participant_external_grade: PropTypes.string,
        participant_external_institution: PropTypes.string,
        participant_external_name: PropTypes.string,
        participants_external: PropTypes.array,
        collaborator_name: PropTypes.string,
        collaborator_institution: PropTypes.string,
        list_collaborators: PropTypes.array,
        phone: PropTypes.string,
    }

    render () {
        return (
            <section className="mt-2 p-1">
                <div className="col-12 filter-section-dsch-multi-form">
                    <h4 className="text-right mb-4 col-8 pr-5">
                        <strong>Registro de Participantes</strong>
                    </h4>

                    <div className="col-4 mx-auto">
                        <button className="btn-dsch float-right"
                                onClick={this.props.onChangeView.bind(this, 1)}>
                            Regresar Pre-Solicitud
                        </button>
                    </div>
                </div>

                <div className="section-protocol pr-1 mt-2 pb-4">
                    <section className="col-12 card pt-1">
                        <label><strong>Responsable (s)</strong></label>
                        <div className="my-1 filter-section-dsch-multi-form" >
                            <div className="col-2 mx-auto">
                                <h6 className="ml-1">
                                    Grado académico
                                </h6>
                                <input type="text" className="form-control" name="grade"
                                       onChange={this.props.onChange} value={this.props.grade}/>

                                <h6 className="ml-1 mt-3">
                                    Número Telefónico
                                </h6>
                                <input type="text" className="form-control" name="phone"
                                       onChange={this.props.onChange} value={this.props.phone}
                                       maxLength={10} />
                            </div>
                            <div className="col-3 mx-auto">
                                <h6 className="ml-1">
                                    Profesor (Empezar por apellidos)
                                </h6>
                                <input type="text" className="form-control" name="users"
                                       onChange={this.props.onChange} value={this.props.users}/>
                            </div>
                            <div className="col-1 pt-2">
                                <button type="button" className="btn btn-warning mt-3"
                                        onClick={this.props.onClickAddNameAuthor}>
                                    Agregar
                                </button>
                            </div>
                            <div className="col-6 mx-auto filter-section-dsch-multi-form ">
                                <section className="col-12 options-products alert alert-warning py-0">
                                {
                                    this.props.list_users.map((obj, index) => (
                                        <div className="col-12 card py-1 mt-1"
                                             key={index}>
                                            <div className="filter-section-dsch-multi-form">
                                                <div className="col-10">
                                                    <h6>
                                                        <strong>
                                                            {
                                                                this.props.list_grade.find(
                                                                    element => element.number === obj.user
                                                                ).grade
                                                            }
                                                        </strong>
                                                        {' ' + obj.name}
                                                        {' ' + obj.paternal_surname}
                                                        {
                                                            obj.maternal_surname !== null
                                                            |
                                                            obj.maternal_surname !== ''
                                                                ? ' ' + obj.maternal_surname : null
                                                        }
                                                    </h6>
                                                    <h6>
                                                        <strong>Departamento: </strong> {obj.area}
                                                    </h6>
                                                    <h6>
                                                        <strong>Contacto: </strong>
                                                        {
                                                            this.props.list_grade.find(
                                                                element => element.number === obj.user
                                                            ).phone
                                                        }
                                                    </h6>
                                                </div>
                                                <div className="col-2 text-center pt-2">
                                                    <button className="btn btn-danger"
                                                            onClick={
                                                                this.props.onDeleteNameAuthor.bind(this, obj.user )
                                                            }>
                                                        Quitar
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    ))
                                }
                                </section>
                            </div>
                        </div>
                    </section>

                    <section className="col-12 card mt-4 pt-1">
                        <label><strong>Participantes Internos</strong></label>
                        <div className="my-1 filter-section-dsch-multi-form" >
                            <div className="col-2 mx-auto">
                                <h6 className="ml-1">
                                    Grado académico
                                </h6>
                                <input type="text" className="form-control"
                                       name="participant_internal_grade"
                                       onChange={this.props.onChange}
                                       value={this.props.participant_internal_grade}/>
                            </div>
                            <div className="col-3 mx-auto">
                                <h6 className="ml-1">
                                    Profesor (Empezar por apellidos)
                                </h6>
                                <input type="text" className="form-control" name="participant_internal"
                                       onChange={this.props.onChange}
                                       value={this.props.participant_internal}/>
                            </div>
                            <div className="col-1 pt-2">
                                <button type="button" className="btn btn-warning mt-3"
                                        onClick={this.props.onClickAddNameParticipant}>
                                    Agregar
                                </button>
                            </div>

                            <div className="col-6 mx-auto filter-section-dsch-multi-form ">
                                <section className="col-12 options-products alert alert-warning py-0">
                                {
                                    this.props.list_participant_internal.map((obj, index) => (
                                        <div className="col-12 card py-1 mt-1"
                                             key={index}>
                                            <div className="filter-section-dsch-multi-form">
                                                <div className="col-10">
                                                    <h6>
                                                        <strong>
                                                            {
                                                                this.props.list_participant_internal_grade.find(
                                                                    element => element.number === obj.user
                                                                ).grade
                                                            }
                                                        </strong>
                                                        {' ' + obj.name}
                                                        {' ' + obj.paternal_surname}
                                                        {
                                                            obj.maternal_surname !== null
                                                            |
                                                            obj.maternal_surname !== ''
                                                                ? ' ' + obj.maternal_surname : null
                                                        }
                                                    </h6>
                                                    <h6>
                                                        <strong>Departamento: </strong> {obj.area}
                                                    </h6>
                                                </div>
                                                <div className="col-2 text-center pt-2">
                                                    <button className="btn btn-danger"
                                                            onClick={
                                                                this.props.onDeleteNameParticipant.bind(this, obj.user )
                                                            }>
                                                        Quitar
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    ))
                                }
                                </section>
                            </div>
                        </div>
                    </section>

                    <section className="col-12 card mt-4">
                        <label><strong>Participantes Externos</strong></label>
                        <div className="my-1 filter-section-dsch-multi-form" >
                            <div className="col-2 mx-auto">
                                <h6 className="ml-1">
                                    Grado académico
                                </h6>
                                <input type="text" className="form-control"
                                       name="participant_external_grade"
                                       onChange={this.props.onChange}
                                       value={this.props.participant_external_grade}/>
                            </div>
                            <div className="col-2 mx-auto">
                                <h6 className="ml-1">
                                    Participante
                                </h6>
                                <input type="text" className="form-control"
                                       name="participant_external_name"
                                       onChange={this.props.onChange}
                                       value={this.props.participant_external_name}/>
                            </div>
                            <div className="col-2 mx-auto">
                                <h6 className="ml-1">
                                    Institución
                                </h6>
                                <input type="text" className="form-control"
                                       name="participant_external_institution"
                                       onChange={this.props.onChange}
                                       value={this.props.participant_external_institution}/>
                            </div>

                            <div className="col-1 pt-2">
                                <button type="button" className="btn btn-warning mt-3"
                                        onClick={this.props.onClickAddNameExternal}>
                                    Agregar
                                </button>
                            </div>

                            <div className="col-5 mx-auto filter-section-dsch-multi-form ">
                                <section className="col-12 options-products alert alert-warning py-0">
                                {
                                    this.props.participants_external.map((obj, index) => (
                                        <div className="col-12 card py-1 mt-1"
                                             key={index}>
                                            <div className="filter-section-dsch-multi-form">
                                                <div className="col-9">
                                                    <h6>
                                                        <strong>
                                                            {
                                                                obj.grade
                                                            }
                                                        </strong>
                                                        {' ' + obj.name}
                                                    </h6>
                                                    <h6>
                                                        <strong>Institución: </strong> {obj.institution}
                                                    </h6>
                                                </div>
                                                <div className="col-3 text-center pt-2">
                                                    <button className="btn btn-danger"
                                                            onClick={
                                                                this.props.onDeleteNameExternal.bind(
                                                                    this, obj.name )
                                                            }>
                                                        Quitar
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    ))
                                }
                                </section>
                            </div>
                        </div>
                    </section>

                    <section className="col-12 card mt-4">
                        <label><strong>Colaboradores</strong></label>
                        <div className="my-1 filter-section-dsch-multi-form" >
                            <div className="col-3 mx-auto">
                                <h6 className="ml-1">
                                    Colaborador
                                </h6>
                                <input type="text" className="form-control" name="collaborator_name"
                                       onChange={this.props.onChange}
                                       value={this.props.collaborator_name}/>
                            </div>
                            <div className="col-3 mx-auto">
                                <h6 className="ml-1">
                                    Institución
                                </h6>
                                <input type="text" className="form-control"
                                       name="collaborator_institution"
                                       onChange={this.props.onChange}
                                       value={this.props.collaborator_institution}/>
                            </div>

                            <div className="col-1 pt-2">
                                <button type="button" className="btn btn-warning mt-3"
                                        onClick={this.props.onClickAddNameCollaborator}>
                                    Agregar
                                </button>
                            </div>

                            <div className="col-5 mx-auto filter-section-dsch-multi-form ">
                                <section className="col-12 options-products alert alert-warning py-0">
                                {
                                    this.props.list_collaborators.map((obj, index) => (
                                        <div className="col-12 card py-1 mt-1"
                                             key={index}>
                                            <div className="filter-section-dsch-multi-form">
                                                <div className="col-9">
                                                    <h6>
                                                        <strong>Nombre: </strong> {obj.name}
                                                    </h6>
                                                    <h6>
                                                        <strong>Institución: </strong> {obj.institution}
                                                    </h6>
                                                </div>
                                                <div className="col-3 text-center pt-2">
                                                    <button className="btn btn-danger"
                                                            onClick={
                                                                this.props.onDeleteNameCollaborator.bind(
                                                                    this, obj.name )
                                                            }>
                                                        Quitar
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    ))
                                }
                                </section>
                            </div>
                        </div>
                    </section>
                </div>

            </section>
        )
    }
}

export default Managers;
