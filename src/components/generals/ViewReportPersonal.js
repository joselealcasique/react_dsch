import React, { Component, Fragment } from 'react';
import PropTypes from "prop-types";
import {connect} from "react-redux";

import TypeReport from "../production/components/TypeReport";
import Year from "../academic/components/Year";
import ListTeacherTraining from "../production/components/list/ListTeacherTraining";
import ListExchangeActivity from "../production/components/list/ListExchangeActivity";
import LIstAcademicEvent from "../production/components/list/LIstAcademicEvent";
import ListAcademicCollaboration from "../production/components/list/ListAcademicCollaboration";
import ListProjectCollaboration from "../production/components/list/ListProjectCollaboration";
import ListMagazinePublication from "../production/components/list/ListMagazinePublication";
import ListElectronicJournals from "../production/components/list/ListElectronicJournals";
import ListNewsPaperPublication from "../production/components/list/ListNewsPaperPublication";
import ListPublishedBook from "../production/components/list/ListPublishedBook";
import ListChaptersBook from "../production/components/list/ListChaptersBook";
import ListBookReview from "../production/components/list/ListBookReview";
import ListPublishedLectures from "../production/components/list/ListPublishedLectures";
import ListUnpublishedLectures from "../production/components/list/ListUnpublishedLectures";
import ListResearchProject from "../production/components/list/ListResearchProject";

import { get_report_personal } from "../../actions/generals/get_report_personal";


export class ViewReportPersonal extends Component {

    state = {
        report: 1,
        year: new Date().getFullYear(),
        view: null
    }

    static propTypes = {
        user: PropTypes.object,
        production: PropTypes.object.isRequired,
        get_report_personal: PropTypes.func.isRequired
    };

    onChange = (e) => this.setState({[e.target.name]: e.target.value});

    onGetPersonalReport = (e) => {
        this.props.get_report_personal(this.state.year, this.props.user.id);
    }

    get_full_name = (obj) => {
        return obj.paternal_surname + ' ' + obj.maternal_surname + ' ' + obj.name;
    }

    onGetView = () => {
        switch (parseInt(this.state.report)) {

            case 1:
                this.setState({
                    view: <ListTeacherTraining get_full_name={this.get_full_name} /> });
                break;

            case 2:
                this.setState({
                    view: <ListExchangeActivity get_full_name={this.get_full_name} /> });
                break;

            case 3:
                this.setState({
                    view: <LIstAcademicEvent get_full_name={this.get_full_name} /> });
                break;

            case 4:
                this.setState({
                    view: <ListAcademicCollaboration get_full_name={this.get_full_name} /> });
                break;

            case 5:
                this.setState({
                    view: <ListProjectCollaboration get_full_name={this.get_full_name} /> });
                break;

            case 6:
                this.setState({
                    view: <ListMagazinePublication get_full_name={this.get_full_name} /> });
                break;

            case 7:
                this.setState({
                    view: <ListElectronicJournals get_full_name={this.get_full_name} /> });
                break;

            case 8:
                this.setState({
                    view: <ListNewsPaperPublication get_full_name={this.get_full_name} /> });
                break;

            case 9:
                this.setState({
                    view: <ListPublishedBook get_full_name={this.get_full_name} /> });
                break;

            case 10:
                this.setState({
                    view: <ListChaptersBook get_full_name={this.get_full_name} /> });
                break;

            case 11:
                this.setState({
                    view: <ListBookReview get_full_name={this.get_full_name} /> });
                break;

            case 12:
                this.setState({
                    view: <ListPublishedLectures get_full_name={this.get_full_name} /> });
                break;

            case 13:
                this.setState({
                    view: <ListUnpublishedLectures get_full_name={this.get_full_name} /> });
                break;

            case 14:
                this.setState({
                    view: <ListResearchProject get_full_name={this.get_full_name} /> });
                break;

            default:
                this.setState({
                    view: <div className="text-center my-5 alert-danger py-4 mx-auto">
                        <h4><strong>NO EXISTE OPCIÓN</strong></h4>
                    </div>,
                });
                break;
        }
    }

    render() {

        return (
            <Fragment>
                <div className="col-12 text-center mt-2 alert alert-warning">
                    <h3><strong>Producción Académica - Mis Reportes</strong></h3>
                </div>

                <div className="filter-section-dsch-multi-form">
                    <section className="col-4" >
                        <div className="text-center alert alert-dark my-auto">
                            <label><strong>Seleccionar tipo de reporte</strong></label>

                            <div className="col-12 filter-section-dsch-multi-form">
                                <div className="col-9 my-2 pb-1">
                                    <Year
                                        select_year={this.state.year.toString()}
                                        onChange={this.onChange}/>
                                </div>
                                <div className="col-2 my-2 pb-1">
                                    <button className="btn-dsch" onClick={this.onGetPersonalReport}>
                                        Aplicar
                                    </button>
                                </div>
                            </div>
                        </div>

                        {
                            this.props.production.personal
                                ?
                                <div className="text-center alert alert-dark mt-2">
                                    <div className="pt-2">
                                        <section className="col-12 text-center">
                                            <label>
                                                <strong>
                                                    Seleccionar Tipo de Reporte
                                                </strong>
                                            </label>
                                        </section>
                                        <section className="filter-section-dsch-multi-form">
                                            <div className="col-10 mx-auto">
                                                <TypeReport
                                                    select_type={parseInt((this.state.report))}
                                                    onChange={this.onChange}/>
                                            </div>
                                            <button className="btn-dsch col-2"
                                                    onClick={this.onGetView}>
                                                Ver
                                            </button>
                                        </section>
                                    </div>
                                </div>
                                :
                                null
                        }
                    </section>
                    <section className="col-8">
                        {this.state.view}
                    </section>
                </div>
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    user: state.auth.user,
    production: state.production
});

export default connect(
    mapStateToProps, {get_report_personal})(ViewReportPersonal);
