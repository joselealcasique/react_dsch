import React, { Component } from 'react';
import {Link} from "react-router-dom";


export class MenuAdminScholarshipConcourse extends Component {
    render() {
        return (
            <ul className="navbar-nav ml-auto mt-2 mt-lg-0">
                <li className="nav-item mr-3 link-dsch">
                    <div className="btn-group ">
                        <button className="nav-link btn-dsch-link dropdown-toggle" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            Becas
                        </button>
                        <div className="dropdown-menu">
                            <Link to="/scholarship" className="dropdown-item">
                                Registrar Solicitud
                            </Link>
                            <Link to="/scholarship-year" className="dropdown-item">
                                Ver Solicitudes por Años
                            </Link>
                            <Link to="/scholarship-number" className="dropdown-item">
                                Ver Solicitudes por Profesor
                            </Link>
                        </div>
                    </div>
                </li>
                <li className="nav-item mr-3 link-dsch">
                    <div className="btn-group">
                        <button className="nav-link btn-dsch-link dropdown-toggle" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            Concursos
                        </button>
                        <div className="dropdown-menu">
                            <Link to="/concourse" className="dropdown-item">
                                Registrar Concuro
                            </Link>
                            <Link to="/concourse-list" className="dropdown-item">
                                Ver Concursos
                            </Link>
                        </div>
                    </div>
                </li>
            </ul>
        )
    }
}

export default MenuAdminScholarshipConcourse;