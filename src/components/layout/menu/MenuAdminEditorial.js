import React, { Component } from 'react';
import {Link} from "react-router-dom";
import PropTypes from "prop-types";


export class MenuAdminEditorial extends Component {

    static propTypes = {
        privileges: PropTypes.array.isRequired
    };

    render() {

        return (
            <ul>
                <h5><strong>Editorial</strong></h5>

                {
                    this.props.privileges.includes('authentication.admin_editorial')
                        |
                    this.props.privileges.includes('authentication.admin_root')
                        ?
                        <li>
                            <Link to="/editorial">+ Registrar Solicitud</Link>
                        </li>
                        :
                        null
                }

                {
                    this.props.privileges.includes('authentication.view_editorial_advice')
                        |
                    this.props.privileges.includes('authentication.admin_root')
                        ?
                        <li>
                            <Link to="/editorial-view">+ Ver Solicitudes (Consejo)</Link>
                        </li>
                        :
                        null
                }

                {
                    this.props.privileges.includes('authentication.admin_editorial')
                        |
                    this.props.privileges.includes('authentication.admin_root')
                        ?
                        <li>
                            <Link to="/editorial-list">+ Ver Solicitudes</Link>
                        </li>
                        :
                        null
                }
            </ul>
        );
    }
}

export default MenuAdminEditorial;