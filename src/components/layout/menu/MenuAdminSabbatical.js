import React, { Component } from 'react';
import {Link} from "react-router-dom";
import PropTypes from "prop-types";


export class MenuAdminSabbatical extends Component {

    static propTypes = {
        privileges: PropTypes.array
    };

    checkPrivileges = () => {
        return (
            this.props.privileges.includes('investigation.admin_leader_departament')
            ||
            this.props.privileges.includes('investigation.admin_evaluation_commission'))
    }

    render() {

        return (
            <ul>
                <h5><strong>Sabáticos</strong></h5>

                {
                    this.checkPrivileges()
                        ?
                        <li>
                            <Link to="/approved-sabbatical">+ Periodos Sabáticos Aprobados</Link>
                        </li>
                        :
                        null
                }

                {
                    this.checkPrivileges()
                        ?
                        <li>
                            <Link to="/request-sabbatical">+ Solicitud de Periodos Sabáticos</Link>
                        </li>
                        :
                        null
                }

                <li>
                    <Link to="/sabbatical">+ Mis Periodos Sabáticos</Link>
                </li>
            </ul>

        );
    }
}

export default MenuAdminSabbatical;