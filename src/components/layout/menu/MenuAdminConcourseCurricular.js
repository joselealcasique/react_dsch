import React, { Component } from 'react';
import {Link} from "react-router-dom";
import PropTypes from "prop-types";


export class MenuAdminConcourseCurricular extends Component {

    static propTypes = {
        privileges: PropTypes.array
    };

    render() {


        return (
            <ul>
                <h5><strong>Educación Curricular</strong></h5>
                {
                    this.props.privileges.includes('authentication.admin_concourse_curricular')
                    |
                    this.props.privileges.includes('authentication.admin_root')
                        ?
                        <li>
                            <Link to="/curricular">
                                + Registrar Concurso
                            </Link>
                        </li>
                        :
                        null
                }
                <li>
                    <Link to="/curricular-list">
                        + Ver Concursos
                    </Link>
                </li>
            </ul>
        )
    }
}

export default MenuAdminConcourseCurricular;