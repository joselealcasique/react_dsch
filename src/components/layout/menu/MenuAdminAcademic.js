import React, { Component } from 'react';
import {Link} from "react-router-dom";
import PropTypes from "prop-types";


export class MenuAdminAcademic extends Component {

    static propTypes = {
        privileges: PropTypes.array
    };

    render() {

        return (
            <ul>
                <h5><strong>Docencia</strong></h5>

                {
                    this.props.privileges.includes('authentication.admin_academic')
                    |
                    this.props.privileges.includes('authentication.admin_root')
                        ?
                        <li>
                            <Link to="/academic">+ Registrar Información</Link>
                        </li>
                        :
                        null
                }

                <li>
                    <Link to="/academic-trimester">+ Ver Registros Trimestrales</Link>
                </li>
                <li>
                    <Link to="/academic-totals">+ Ver Registros Anuales</Link>
                </li>
            </ul>

        );
    }
}

export default MenuAdminAcademic;