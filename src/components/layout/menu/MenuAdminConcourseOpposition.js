import React, { Component } from 'react';
import {Link} from "react-router-dom";
import PropTypes from "prop-types";


export class MenuAdminConcourseOpposition extends Component {

    static propTypes = {
        privileges: PropTypes.array
    };

    render() {


        return (
            <ul>
                <h5><strong>Oposición</strong></h5>
                {
                    this.props.privileges.includes('authentication.admin_concourse_curricular')
                    |
                    this.props.privileges.includes('authentication.admin_root')
                        ?
                        <li>
                            <Link to="/opposition">+ Registrar Concurso</Link>
                        </li>
                        :
                        null
                        }

                <li>
                    <Link to="/opposition-list">+ Ver Concursos</Link>
                </li>
            </ul>
        )
    }
}

export default MenuAdminConcourseOpposition;