import React, { Component } from 'react';
import {Link} from "react-router-dom";


export class MenuAdminScholarShip extends Component {

    render() {
        return (
            <ul>
                <h5><strong>Becas</strong></h5>
                <li>
                    <Link to="/scholarship">+ Registrar Solicitud</Link>
                </li>
                <li>
                    <Link to="/scholarship-year">+ Ver Solicitudes por Años</Link>
                </li>
                <li>
                    <Link to="/scholarship-number">+ Ver Solicitudes por Profesor</Link>
                </li>
            </ul>
        );
    }
}

export default MenuAdminScholarShip;