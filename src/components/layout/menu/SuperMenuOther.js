import React from "react"
import PropTypes from "prop-types";

import MenuAdminEditorial from "./MenuAdminEditorial";
import MenuAdminInvestigation from "./MenuAdminInvestigation";
import MenuAdminSabbatical from "./MenuAdminSabbatical";


export class SuperMenuConcourse extends  React.Component {

     static propTypes = {
        privileges: PropTypes.array,
         user: PropTypes.object,
    };

    render() {
        return (
            <li className="submenu">
                <a style={{"cursor": "pointer"}}
                   title="Autorías - Facultad de Ciencias Sociales y Humanidades">
                    Autorías
                </a>
                <ul className="megamenu">

                    {
                        this.props.privileges.includes('authentication.admin_editorial')
                            |
                        this.props.privileges.includes('authentication.view_editorial_advice')
                            |
                        this.props.privileges.includes('authentication.admin_root')
                            ?
                            <MenuAdminEditorial privileges={this.props.privileges} />
                            :
                            null
                    }

                    {
                        this.props.privileges.includes('investigation.admin_leader_departament')
                            |
                        this.props.privileges.includes('investigation.admin_evaluation_commission')
                            |
                        this.props.privileges.includes('authentication.admin_root')
                            |
                        this.props.user.is_indeterminate
                            ?
                            <MenuAdminSabbatical privileges={this.props.privileges}/>
                            :
                            null
                    }

                    {
                        this.props.privileges.includes('investigation.admin_leader_departament')
                            |
                        this.props.privileges.includes('investigation.admin_evaluation_commission')
                            |
                        this.props.privileges.includes('authentication.admin_root')
                            |
                        this.props.user.is_indeterminate
                            ?
                            <MenuAdminInvestigation
                                privileges={this.props.privileges}
                                user={this.props.user} />
                            :
                            null
                    }
                </ul>
            </li>
        )
    }

}

export default SuperMenuConcourse;