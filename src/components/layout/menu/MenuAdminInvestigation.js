import React, { Component } from 'react';
import {Link} from "react-router-dom";
import PropTypes from "prop-types";


export class MenuAdminInvestigation extends Component {

    static propTypes = {
        privileges: PropTypes.array.isRequired,
        user: PropTypes.object.isRequired,
    };

    checkPrivileges = () => {
        if (
            this.props.privileges.includes('investigation.admin_leader_departament')
            |
            this.props.privileges.includes('investigation.admin_evaluation_commission')
            |
            this.props.privileges.includes('authentication.admin_root')
        ){
            return true;
        } else {
            return false;
        }
    }

    render() {

        return (
            <ul>
                <h5><strong>Proyectos de Investigación</strong></h5>

                {
                    this.checkPrivileges()
                        ?
                        <li>
                            <Link to="/project-request">
                                + Ver Solicitudes de proyectos
                            </Link>
                        </li>
                        :
                        null
                }

                {
                    this.checkPrivileges()
                        ?
                        <li>
                            <Link to="/project-approved">
                                + Ver Proyectos Aprobados
                            </Link>
                        </li>
                        :
                        null
                }

                {
                    this.props.user !== null ?
                        this.props.user.is_indeterminate
                            |
                        this.props.privileges.includes('authentication.admin_root')
                            ?
                            <li>
                                <Link to="/project-list">
                                + Mis Proyectos de Investigación
                                </Link>
                            </li>
                            :
                            null
                        :
                        null
                }

                {
                    this.checkPrivileges()
                        ?
                        <li>
                            <Link to="/reports-investigation">
                                + Reportes proyectos de investigación
                            </Link>
                        </li>
                        :
                        null
                }

            </ul>
        );
    }
}

export default MenuAdminInvestigation;