import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import "./style/style.css"
import SuperMenuConcourse from "./menu/SuperMenuConcourse";
import SuperMenuAcademic from "./menu/SuperMenuAcademic";
import SuperMenuOther from "./menu/SuperMenuOther";



export class NavBar extends Component {

    static propTypes = {
        auth: PropTypes.object.isRequired,
        permissions: PropTypes.array.isRequired,
        user: PropTypes.object,
    };


    render() {
        return (
            <nav>
                <ul>
                    {
                        this.props.permissions.includes(
                            'authentication.admin_concourse_curricular')
                            |
                        this.props.permissions.includes(
                            'authentication.admin_concourse_chair')
                            |
                        this.props.permissions.includes(
                            'authentication.admin_concourse_opposition')
                            |
                        this.props.permissions.includes(
                            'authentication.admin_concourse_visitor')
                            |
                        this.props.permissions.includes(
                            'authentication.admin_root')
                            |
                        this.props.permissions.includes(
                            'authentication.view_concourse')
                            ?
                            <SuperMenuConcourse privileges={ this.props.permissions } />
                            :
                            null

                    }

                    {
                        this.props.permissions.includes('authentication.admin_academic')
                            |
                        this.props.permissions.includes('authentication.admin_root')
                            |
                        this.props.permissions.includes('authentication.view_academic_humanities')
                            |
                        this.props.permissions.includes('authentication.view_academic_social_sciences')
                            |
                        this.props.permissions.includes('authentication.view_academic_institutional_studies')
                            |
                        this.props.permissions.includes('authentication.view_academic_general')
                            |
                        this.props.permissions.includes('authentication.admin_production')
                            |
                        this.props.permissions.includes('authentication.admin_scholarship')
                            |
                        this.props.permissions.includes('authentication.admin_adjustments')
                            ?
                            <SuperMenuAcademic privileges={ this.props.permissions } />
                            :
                            null
                    }

                    {
                        this.props.user !== null
                            ?
                            this.props.permissions.includes('authentication.admin_editorial')
                                |
                            this.props.permissions.includes('authentication.view_editorial_advice')
                                |
                            this.props.permissions.includes('authentication.admin_root')
                                |
                            this.props.permissions.includes('investigation.admin_leader_departament')
                                |
                            this.props.permissions.includes('investigation.admin_evaluation_commission')
                                |
                            this.props.user.is_indeterminate
                                ?
                                <SuperMenuOther privileges={ this.props.permissions } user={this.props.user} />
                                :
                                null
                            :
                            null
                    }

                </ul>
            </nav>
        )
    }
}

const mapStateToProps = (state) => ({
    auth: state.auth,
    user: state.auth.user,
    permissions: state.auth.permissions
});

export default connect(mapStateToProps, null )(NavBar);
