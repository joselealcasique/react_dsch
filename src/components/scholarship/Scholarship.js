import React, { Component, Fragment } from 'react';

import FormScholarship from "./components/FormScholarship";


export class Scholarship extends Component {

    state = {
        request: 1,
    }

    render() {
        return (
            <Fragment>
                <section className="mt-4 col-11 card card-body mx-auto">
                    <h3 className=" col-3 mx-auto">Registar Solicitud</h3>
                    <FormScholarship />
                </section>
            </Fragment>
        );
    }
}

export default Scholarship;
