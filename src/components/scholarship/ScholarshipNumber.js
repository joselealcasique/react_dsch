import React, { Component, Fragment} from 'react';

import OptionRequest from './components/OptionRequest';
import ListScholarshipNumber from './components/ListScholarshipNumber';
import {
    get_list_scholarship_number
} from '../../actions/scholarship/get_list_scholarship-number';

import { connect } from "react-redux";
import PropTypes from 'prop-types';


export class ScholarshipNumber extends Component {

    state = {
        number: ' ',
        request: 5
    }

    static propTypes = {
        get_list_scholarship_number: PropTypes.func.isRequired
    };

    componentDidMount() {
        this.props.get_list_scholarship_number(this.state.number, this.state.request);
    }

    onChange = (e) => {
        this.setState({[e.target.name]: e.target.value});
    }

    onSubmit = (e) => {
        e.preventDefault();
        this.props.get_list_scholarship_number(this.state.number, this.state.request);
    }

    render() {
        return (
            <Fragment>
                <form className="card card-body mt-3" onSubmit={this.onSubmit}>
                    <h3 className="mx-1 text-center">
                        Registre el Número Económico y Selecione el Tipo de Solicitud a Visualizar
                    </h3>
                    <div className="filter-section-dsch-multi-form mt-2 text-center">
                        <section className="col-2 mt-2">
                            <input type="number" className="form-control" name="number" required
                                   value={this.state.number} onChange={this.onChange}/>
                        </section>
                        <section className="col-9">
                            <OptionRequest
                                select_option={parseInt(this.state.request)}
                                onChange={this.onChange}
                            />
                        </section>
                        <section className="col-1 mt-2">
                            <button className="btn btn-dark">Buscar</button>
                        </section>
                    </div>
                </form>
                <section>
                    <ListScholarshipNumber/>
                </section>
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
});

export default connect(
    mapStateToProps, { get_list_scholarship_number })(ScholarshipNumber);
