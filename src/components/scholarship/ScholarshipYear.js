import React, { Component, Fragment} from 'react';
import { connect } from "react-redux";
import PropTypes from 'prop-types';

import Year from '../academic/components/Year';
import OptionRequest from './components/OptionRequest';
import ListScholarshipYear from './components/ListScholarshipYear';
import ModelReportExcel from './components/ModelReportExcel';

import { get_list_scholarship_year } from '../../actions/scholarship/get_list_scholarship_year';



export class ScholarshipYear extends Component {

    state = {
        year: new Date().getFullYear(),
        request: 5
    }

    static propTypes = {
        get_list_scholarship_year: PropTypes.func.isRequired
    };

    componentDidMount() {
        this.props.get_list_scholarship_year(this.state.year, this.state.request);
    }

    onChange = (e) => {
        this.setState({[e.target.name]: e.target.value});
    }

    onSubmit = (e) => {
        e.preventDefault();
        this.props.get_list_scholarship_year(this.state.year, this.state.request);
    }

    render() {
        return (
            <Fragment>
                <form className="card card-body mt-3" onSubmit={this.onSubmit}>
                    <h3 className="mx-1 text-center">
                        Selecione el Año y Tipo de Solicitud a Visualizar
                    </h3>
                    <div className="filter-section-dsch-multi-form mt-2 text-center">
                        <section className="col-2 mt-2">
                            <Year
                                select_year={this.state.year.toString()}
                                onChange={this.onChange}
                            />
                        </section>
                        <section className="col-8">
                            <OptionRequest
                                select_option={parseInt(this.state.request)}
                                onChange={this.onChange}
                            />
                        </section>
                        <section className="col-1 mt-2">
                            <button className="btn btn-dark">Buscar</button>
                        </section>

                        <section className="col-1 mt-2">
                            <ModelReportExcel />
                        </section>
                    </div>
                </form>
                <section>
                    <ListScholarshipYear/>
                </section>
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
});

export default connect(
    mapStateToProps, { get_list_scholarship_year })(ScholarshipYear);