import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';


export class OptionRequest extends Component {

    static propTypes = {
        select_option: PropTypes.number,
        options: PropTypes.array
    }

    render() {
        return (
            <select className="form-control" name="request"
                    value={this.props.select_option} onChange={this.props.onChange}>
                <option key={5} value={5}> Todos Los Registros </option>
                <option key={1} value={1}> Promoción </option>
                <option key={2} value={2}> Estímulo a la Docencia e Investigación </option>
                <option key={3} value={3}>
                    Estímulo a la Trayectoria Académica Sobresaliente
                </option>
                <option key={4} value={4}> Beca de Apoyo a la Permanecia </option>
            </select>
        );
    }
}

const mapStateToProps = (state) => ({
    options: state.scholarship.options
});

export default connect(
    mapStateToProps, null )(OptionRequest);
