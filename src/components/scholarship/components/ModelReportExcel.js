import React, {Component} from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Workbook from "react-excel-workbook";


export class ModelReportExcel extends Component{

    state = {
        name: new Date().getFullYear()
    }

    static propTypes = {
        scholarship: PropTypes.object,
    };

    render() {

        let space_report = null;
        if (this.props.scholarship.load === true & this.props.scholarship.scholarship !== null) {
            space_report = (
                <table className="table table-striped mt-3" id="reportExcelTableScholarship">
                    <thead className="text-center">
                    <tr>
                        <th>No. Económico</th>
                        <th>Nombre de Trabajador</th>
                        <th>No. Solicitud</th>
                        <th style={{display: "none"}}>Se Solicitó EDI</th>
                        <th style={{display: "none"}}>Se Otorgó EDI</th>
                        <th style={{display: "none"}}>Se Solicitó ETAS</th>
                        <th style={{display: "none"}}>Se Otorgó ETAS</th>
                        <th style={{display: "none"}}>Se Solicitó BAP</th>
                        <th style={{display: "none"}}>Se Otorgó BAP</th>
                        <th style={{display: "none"}}>Se Solicitó Promoc</th>
                        <th style={{display: "none"}}>Se Otorgó Promoc</th>
                    </tr>
                    </thead>
                    <tbody className="text-center">
                    {this.props.scholarship.scholarship.map((scholar) => (
                        <tr key={scholar.id}>
                            <td >{scholar.user}</td>
                            <td >{scholar.full_name}</td>
                            <td >{scholar.number_request}</td>

                            <td style={{display: "none"}}>
                                {scholar.stimulus === true ? 'SI' : 'NO'}
                            </td>
                            <td style={{display: "none"}}>
                                {scholar.result_stimulus === 1 ? 'SI' : 'NO'}
                            </td>

                            <td style={{display: "none"}}>
                                {scholar.trajectory === true ? 'SI' : 'NO'}
                            </td>
                            <td style={{display: "none"}}>
                                {scholar.result_trajectory === 1 ? 'SI' : 'NO'}
                            </td>

                            <td style={{display: "none"}}>
                                {scholar.permanence === true ? 'SI' : 'NO'}
                            </td>
                            <td style={{display: "none"}}>
                                {scholar.result_permanence === 1 ? 'SI' : 'NO'}
                            </td>

                            <td style={{display: "none"}}>
                                {scholar.promotion === true ? 'SI' : 'NO'}
                            </td>
                            <td style={{display: "none"}}>
                                {scholar.result_promotion === 1 ? 'SI' : 'NO'}
                            </td>
                        </tr>
                    ))}
                    </tbody>
                </table>
            );
        } else{
            space_report = (
                <h3 className="mt-5 text-center">Loading...</h3>
            );
        }

        const data2 = [
  {
    aaa: 1,
    bbb: 2,
    ccc: 3
  },
  {
    aaa: 4,
    bbb: 5,
    ccc: 6
  }
]


        return (
            <Workbook filename={`Reporte de Solicitudes del Año ${this.state.name}.xlsx`}
                      element={
                          <button className="btn btn-warning">
                              Reporte
                          </button>
                      }>
                <Workbook.Sheet data={this.props.scholarship.scholarship} name="Sheet A">
                    <Workbook.Column label="No. Económico" value="user" />
                    <Workbook.Column label="Nombre de Trabajador" value="full_name"/>
                    <Workbook.Column label="No. Solicitud" value="number_request"/>

                    <Workbook.Column label="Se Solicitó EDI"
                                     value={row => row.stimulus === true ? 'Si' : 'NO'}
                    />
                    <Workbook.Column label="Se Otorgó EDI"
                                     value={row => row.result_stimulus === true ? 'Si' : 'NO'}
                    />
                    <Workbook.Column label="Se Solicitó ETAS"
                                     value={row => row.trajectory === true ? 'Si' : 'NO'}
                    />
                    <Workbook.Column label="Se Otorgó ETAS"
                                     value={row => row.result_trajectory === true ? 'Si' : 'NO'}
                    />
                    <Workbook.Column label="Se Solicitó BAP"
                                     value={row => row.permanence === true ? 'Si' : 'NO'}
                    />
                    <Workbook.Column label="Se Otorgó BAP"
                                     value={row => row.result_permanence === true ? 'Si' : 'NO'}
                    />
                    <Workbook.Column label="Se Solicitó Promoc"
                                     value={row => row.promotion === true ? 'Si' : 'NO'}
                    />
                    <Workbook.Column label="Se Otorgó Promoc"
                                     value={row => row.result_promotion === true ? 'Si' : 'NO'}
                    />
                </Workbook.Sheet>
            </Workbook>
        );
    }
}


const mapStateToProps = (state) => ({
    scholarship: state.scholarship
});

export default connect(mapStateToProps, null)(ModelReportExcel);