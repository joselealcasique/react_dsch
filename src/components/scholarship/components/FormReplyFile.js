import React, { Component } from 'react';
import PropType from "prop-types";
import {connect} from "react-redux";
import {SERVER} from "../../../actions/server";

import {register_reply} from '../../../actions/scholarship/register_reply';


class FormReplyFile extends Component{

    state = {
        file_reply: null,
        number_reply: '',
    }

    static propTypes = {
        register: PropType.object,
        register_reply: PropType.func.isRequired
    }

    onChange = (e) => {
        this.setState({[e.target.name]: e.target.value});
    }

    onLoadFile = (e) => {
        this.setState({file_reply: e.target.files[0]});
        const reader = new FileReader();
        reader.onload = () => {
            if (reader.readyState === 2) this.setState({viewFile: reader.result});
        }
        reader.readAsDataURL(e.target.files[0]);
    }

    onSubmit = (e) => {
        e.preventDefault();
        const data = new FormData();
        data.append('number_reply', this.state.number_reply);
        data.append('file_reply', this.state.file_reply, this.state.file_reply.name);
        this.props.register_reply(data, this.props.register.id);
    }

    render() {

        if (this.props.register.file_reply === null){
            if (this.props.register.finalized === false){
                if (this.props.register.file_resolution !== null){
                    return (
                        <div className="card col-6 card-form-file">
                            <form className="m-auto" onSubmit={this.onSubmit} encType="multipart/form-data">
                                <div className="form-group">
                                    <label>Folio de Respuesta de Comisión</label>
                                    <input type="text" className="form-control" name="number_reply"
                                           required onChange={this.onChange}
                                           value={this.state.number_reply}/>
                                </div>
                                <div className="form-group">
                                    <label>Archivo de Respuesta de Comisión</label>
                                    <input type="file" className="form-control" name="file_reply"
                                           required onChange={this.onLoadFile}
                                           value={this.file_reply}/>
                                </div>
                                <div className="form-group text-center">
                                    <button className="btn btn-file-academic m-auto">Guardar</button>
                                </div>
                            </form>
                        </div>
                    );
                } else {
                    return (
                        <div className="col-6 card-form-file">
                            <div className="message-depend">
                                <h4 className="text-center">Pendiente</h4>
                            </div>
                        </div>
                    );
                }
            } else{
                return (
                    <div className="col-6 card-form-file">
                        <div className="message-depend">
                            <h4 className="text-center">NO APLICA</h4>
                        </div>
                    </div>
                );
            }
        } else{
            return (
                <div className="card col-6 card-form-file">
                    <section className="card-body">
                        <h6 className="text-center">
                            <strong>Folio de Respuesta de Comisión </strong>
                            {this.props.register.number_reply}
                        </h6>
                        <section className="alert alert-dark text-center">
                            <a target="_blank" rel="noopener noreferrer"
                               href={`${SERVER}${this.props.register.file_reply}`}>
                                Ver Archivo
                            </a>
                        </section>
                    </section>
                </div>
            );
        }
    }

}

const mapStateToProps = (state) => ({
    register: state.complete.register
});

export default connect(
    mapStateToProps, {register_reply})(FormReplyFile);