import React, { Component } from 'react';
import PropType from "prop-types";
import {connect} from "react-redux";
import {SERVER} from "../../../actions/server";
import {register_bis} from '../../../actions/scholarship/register_bis';


class FormBisFile extends Component{

    state = {
        file_bis: null,
        number_bis: '',
        edit: false
    }

    static propTypes = {
        register: PropType.object,
        register_bis: PropType.func.isRequired
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onLoadFile = (e) => this.setState({file_bis: e.target.files[0]});

    onSubmit = (e) => {
        e.preventDefault();
        const data = new FormData();
        data.append('number_bis', this.state.number_bis);
        data.append('file_bis', this.state.file_bis, this.state.file_bis.name);
        this.props.register_bis(data, this.props.register.id);
        this.setState({edit: false});
    }

    onEditFormFile = (e) => {
        this.setState({
            edit: this.state.edit === false ? true: false
        })
    }

    render() {

        const cancelButton = (
            <button className="btn btn-danger m-auto col-4" onClick={this.onEditFormFile}>
                Cancelar
            </button>
        );

        const editButton = (
            <button className="btn btn-primary btn-edit-files col-3 m-auto" onClick={this.onEditFormFile}>
                Editar
            </button>
        );

        const addButton = (
            <button className="btn btn-warning btn-edit-files col-3 m-auto">
                Agregar
            </button>
        );

        const formBIS = (
            <form className="m-auto" onSubmit={this.onSubmit} encType="multipart/form-data">
                <div className="form-group">
                    <label>Folio de Acuse BIS</label>
                    <input type="text" className="form-control" name="number_bis"
                           onChange={this.onChange} value={this.state.number_bis}/>
                </div>
                <div className="form-group">
                    <label>Archivo de Acuse BIS</label>
                    <input type="file" className="form-control" name="file_bis"
                           required onChange={this.onLoadFile} accept="application/pdf"
                           value={this.file_bis}/>
                </div>
                <div className="form-group text-center">
                    {this.state.edit === true ? cancelButton: null}
                    <button className="btn btn-file-academic m-auto">Guardar</button>
                </div>
            </form>
        );

        const formBISRequired = (
            <form className="m-auto" onSubmit={this.onSubmit} encType="multipart/form-data">
                <div className="form-group">
                    <label>Folio de Acuse BIS</label>
                    <input type="text" className="form-control" name="number_bis"
                           required onChange={this.onChange} value={this.state.number_bis}/>
                </div>
                <div className="form-group">
                    <label>Archivo de Acuse BIS</label>
                    <input type="file" className="form-control" name="file_bis"
                           required onChange={this.onLoadFile} accept="application/pdf"
                           value={this.file_bis}/>
                </div>
                <div className="form-group text-center">
                    <button className="btn btn-file-academic m-auto">Guardar</button>
                </div>
            </form>
        );

        if (this.props.register.file_bis === null) {
            if (this.props.register.finalized === true){
                return (
                    <div className="col-6 card-form-file">
                        <div className="message-depend">
                            <h5 className="text-center">
                                NO APLICA - DICTAMEN BIS
                            </h5>
                        </div>
                    </div>
                );
            } else {
                return (
                    <div className="card col-6 card-form-file">
                        {formBISRequired}
                    </div>
                );
            }
        } else if (this.state.edit === true) {
            return (
                <div className="card col-6 card-form-file">
                    {formBIS}
                </div>
            );
        } else {
            return (
                <div className="card col-6 card-form-file">
                    <section className="card-body">
                        <h6 className="text-center">
                            <strong>DICTAMEN BIS </strong>
                            {this.props.register.number_bis}
                        </h6>
                        <section className="filter-section-dsch-multi-form">
                            <section className="alert alert-dark text-center col-6 m-auto">
                                <a target="_blank" rel="noopener noreferrer"
                                   href={`${SERVER}${this.props.register.file_bis}`}>
                                    Ver Archivo
                                </a>
                            </section>
                            {this.props.register.finalized === false ? editButton : null}
                            {/*this.props.register.finalized === false ? addButton : null*/}
                        </section>
                    </section>
                </div>
            );
        }
    }
}

const mapStateToProps = (state) => ({
    register: state.complete.register
});

export default connect(
    mapStateToProps, {register_bis})(FormBisFile);