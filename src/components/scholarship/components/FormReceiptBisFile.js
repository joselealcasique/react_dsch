import React, { Component } from 'react';
import PropType from "prop-types";
import {connect} from "react-redux";
import {SERVER} from "../../../actions/server";
import {register_receipt_bis} from '../../../actions/scholarship/register_receipt_bis';


class FormReceiptBisFile extends Component{

    state = {
        file_receipt_bis: null,
        edit: false
    }

    static propTypes = {
        register: PropType.object,
        register_receipt_bis: PropType.func.isRequired
    }

    onChange = (e) => {
        this.setState({[e.target.name]: e.target.value});
    }

    onLoadFile = (e) => {
        this.setState({file_receipt_bis: e.target.files[0]});
        const reader = new FileReader();
        reader.onload = () => {
            if (reader.readyState === 2) this.setState({viewFile: reader.result});
        }
        reader.readAsDataURL(e.target.files[0]);
    }

    onSubmit = (e) => {
        e.preventDefault();
        const data = new FormData();
        data.append('number_receipt_bis', this.props.register.number_bis);
        data.append('file_receipt_bis', this.state.file_receipt_bis, this.state.file_receipt_bis.name);
        this.props.register_receipt_bis(data, this.props.register.id);
        this.setState({edit: false});
    }

    onEditFormFile = (e) => {
        this.setState({
            edit: this.state.edit === false ? true: false
        })
    }

    render() {

        const cancelButton = (
            <button className="btn btn-danger m-auto col-4" onClick={this.onEditFormFile}>
                Cancelar
            </button>
        );

        const editButton = (
            <button className="btn btn-primary btn-edit-files col-3 m-auto" onClick={this.onEditFormFile}>
                Editar
            </button>
        );

        const addButton = (
            <button className="btn btn-warning btn-edit-files col-3 m-auto">
                Agregar
            </button>
        );

        if (this.props.register.file_receipt_bis === null | this.state.edit === true) {
            if (this.props.register.finalized === true){
                return (
                    <div className="col-6 card-form-file">
                        <div className="message-depend">
                            <h5 className="text-center">
                                NO APLICA - ACUSE DE DICTAMEN BIS
                            </h5>
                        </div>
                    </div>
                );
            } else {
                return (
                    <div className="card col-6 card-form-file">
                        <form className="m-auto" onSubmit={this.onSubmit} encType="multipart/form-data">
                            <div className="form-group">
                                <label>Archivo de Acuse de Recibido de Dictamen BIS:</label>
                                <input type="file" className="form-control" name="file_receipt_bis"
                                       required onChange={this.onLoadFile} accept="application/pdf"
                                       value={this.file_receipt_bis}/>
                            </div>
                            <div className="form-group text-center">
                                {this.state.edit === true ? cancelButton: null}
                                <button className="btn btn-file-academic m-auto">Guardar</button>
                            </div>
                        </form>
                    </div>
                );
            }
        } else {
            return (
                <div className="card col-6 card-form-file">
                    <section className="card-body">
                        <h6 className="text-center">
                            <strong>ACUSE DE DICTAMEN BIS </strong>
                            {this.props.register.number_receipt_bis}
                        </h6>

                        <section className="filter-section-dsch-multi-form">
                            <section className="alert alert-dark text-center col-6 m-auto">
                                <a target="_blank" rel="noopener noreferrer"
                                   href={`${SERVER}${this.props.register.file_receipt_bis}`}>
                                    Ver Archivo
                                </a>
                            </section>
                            {this.props.register.finalized === false ? editButton : null}
                            {/*this.props.register.finalized === false ? addButton : null*/}
                        </section>
                    </section>
                </div>
            );
        }
    }
}

const mapStateToProps = (state) => ({
    register: state.complete.register
});

export default connect(
    mapStateToProps, {register_receipt_bis})(FormReceiptBisFile);