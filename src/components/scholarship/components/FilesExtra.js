import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropType from 'prop-types';
import {SERVER} from "../../../actions/server";

import { set_file_additional } from '../../../actions/scholarship/set_file_additional';
import { deleted_file_additional } from '../../../actions/scholarship/delete_file_additional';


class FilesExtra extends Component {

    state = {
        number_file: '',
        file: null
    }

    static propTypes = {
        files: PropType.array.isRequired,
        register: PropType.object,
        set_file_additional: PropType.func.isRequired,
        deleted_file_additional: PropType.func.isRequired
    }

    onLoadFileAdditional = (e) => {
        this.setState({file: e.target.files[0]});
        const reader = new FileReader();
        reader.onload = () => {
            if (reader.readyState === 2) this.setState({viewFile: reader.result});
        }
        reader.readAsDataURL(e.target.files[0]);
    }

    onChange = (e) => {
        this.setState({[e.target.name]: e.target.value});
    }

    onSubmit = (e) => {
        e.preventDefault();
        const data = new FormData();
        data.append('number_file', this.state.number_file);
        data.append('file', this.state.file, this.state.file.name);
        data.append('scholarship', this.props.register.id);
        this.props.set_file_additional(data);
        this.setState({number_file: ''});
    }

    onDeleteFileAdditional = (id) => {
        if (window.confirm('Desea Eliminar este Archivo')){
            this.props.deleted_file_additional(id);
        }
    }

    render() {

        let files_additional = null;
        let form_additional = null;

        if (this.props.register.finalized === false & this.props.files.length < 6){
            form_additional = (
                <form className="m-auto" onSubmit={this.onSubmit} encType="multipart/form-data">
                    <div className="form-group col-12 m-auto form-files-additional">
                        <input type="text" className="form-control" name="number_file" required
                               value={this.state.number_file} onChange={this.onChange}
                               placeholder="Nombre de Archivo"/>
                    </div>
                    <div className="form-group col-12 m-auto form-files-additional">
                        <input type="file" className="form-control" name="file" required
                               value={this.file} accept="application/pdf"
                               onChange={this.onLoadFileAdditional} />
                    </div>
                    <div className="form-group text-center form-files-additional">
                        <button className="btn btn-file-academic m-auto">
                            Guardar
                        </button>
                    </div>
                </form>
            );
        }

       if (this.props.files !== null){

           files_additional = (
                this.props.files.map((file) => (
                    <section className="section-button col-5 m-auto" key={file.id}>
                        <section className="filter-section-dsch-multi-form">
                            <h6 className="text-center m-auto">
                                <strong className="text-center">{file.number_file}</strong>
                            </h6>
                            { this.props.register.finalized === false ?
                                <button type="button" className="btn-remove-file float-right"
                                        onClick={this.onDeleteFileAdditional.bind(this, file.id)}>
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                : null
                            }
                        </section>
                        <section className="card section-button">
                            <a target="_blank" rel="noopener noreferrer"
                               href={`${SERVER}${file.file}`}>
                                Ver Archivo
                            </a>
                        </section>
                    </section>
                )));
       }
       return (
           <div className="modal fade" id="FilesExtra" tabIndex="-1" role="dialog"
                aria-labelledby="exampleModalLabel" aria-hidden="true">
               <div className="modal-dialog" role="document">
                   <div className="modal-content">
                       <div className="modal-header">
                           <h5 className="modal-title" id="exampleModalLabel">
                               Documentación Relacionada a la Soliciutd
                           </h5>
                           <button type="button" className="close" data-dismiss="modal"
                                   aria-label="Close">
                               <span aria-hidden="true">&times;</span>
                           </button>
                       </div>
                       {form_additional}
                       <div className="modal-footer alert-dark">
                           {files_additional}
                       </div>
                   </div>
               </div>
           </div>
       );
    }
}

const mapStateToProps = (state) => ({
    files: state.complete.files,
    register: state.complete.register
});


export default connect(
    mapStateToProps, {
        set_file_additional, deleted_file_additional})(FilesExtra);