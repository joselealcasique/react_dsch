import React, { Component } from 'react';
import PropType from "prop-types";
import {connect} from "react-redux";
import {SERVER} from "../../../actions/server";

import { register_notification } from '../../../actions/scholarship/register_notification';


class FormNotificationFile extends Component{

    state = {
        file_notification: null,
        number_notification: '',
        edit: false
    }

    static propTypes = {
        register: PropType.object,
        register_notification: PropType.func.isRequired
    }

    onChange = (e) => {
        this.setState({[e.target.name]: e.target.value});
    }

    onLoadFile = (e) => {
        this.setState({file_notification: e.target.files[0]});
        const reader = new FileReader();
        reader.onload = () => {
            if (reader.readyState === 2) this.setState({viewFile: reader.result});
        }
        reader.readAsDataURL(e.target.files[0]);
    }

    onSubmit = (e) => {
        e.preventDefault();
        const data = new FormData();
        data.append('number_notification', this.state.number_notification);
        data.append('file_notification', this.state.file_notification, this.state.file_notification.name);
        this.props.register_notification(data, this.props.register.id);
        this.setState({edit: false});
    }

    onEditFormFile = (e) => {
        this.setState({
            edit: this.state.edit === false ? true: false
        })
    }

    render() {

        const cancelButton = (
            <button className="btn btn-danger m-auto col-4" onClick={this.onEditFormFile}>
                Cancelar
            </button>
        );

        const editButton = (
            <button className="btn btn-primary btn-edit-files col-3 m-auto" onClick={this.onEditFormFile}>
                Editar
            </button>
        );

        const addButton = (
            <button className="btn btn-warning btn-edit-files col-3 m-auto">
                Agregar
            </button>
        );

        const formNotificationRequired = (
            <form className="m-auto" onSubmit={this.onSubmit} encType="multipart/form-data">
                <div className="form-group">
                    <label>Folio de Recurso Interpuesto</label>
                    <input type="text" className="form-control" name="number_notification"
                           required onChange={this.onChange} value={this.state.number_notification}/>
                </div>
                <div className="form-group">
                    <label>Archivo de Recurso Interpuesto</label>
                    <input type="file" className="form-control" name="file_notification" required
                           onChange={this.onLoadFile} accept="application/pdf" value={this.file_notification}/>
                </div>
                <div className="form-group text-center">
                    <button className="btn btn-file-academic m-auto">Guardar</button>
                </div>
            </form>
        );

        const formNotification = (
            <form className="m-auto" onSubmit={this.onSubmit} encType="multipart/form-data">
                <div className="form-group">
                    <label>Folio de Recurso Interpuesto</label>
                    <input type="text" className="form-control" name="number_notification"
                           onChange={this.onChange} value={this.state.number_notification}/>
                </div>
                <div className="form-group">
                    <label>Archivo de Recurso Interpuesto</label>
                    <input type="file" className="form-control" name="file_notification" required
                           onChange={this.onLoadFile} accept="application/pdf" value={this.file_notification}/>
                </div>
                <div className="form-group text-center">
                    {this.state.edit === true ? cancelButton: null}
                    <button className="btn btn-file-academic m-auto">Guardar</button>
                </div>
            </form>
        );

        if (this.props.register.file_notification === null) {
            if (this.props.register.finalized === true){
                return (
                    <div className="col-6 card-form-file">
                        <div className="message-depend">
                            <h5 className="text-center">
                                NO APLICA - RECURSO INTERPUESTO
                            </h5>
                        </div>
                    </div>
                );
            } else {
                return (
                    <div className="card col-6 card-form-file">
                        {formNotificationRequired}
                    </div>
                );
            }
        } else if (this.state.edit === true){
            return (
                <div className="card col-6 card-form-file">
                    {formNotification}
                </div>
            );
        }else{
            return (
                <div className="card col-6 card-form-file">
                    <section className="card-body">
                        <h6 className="text-center">
                            <strong>RECURSO INTERPUESTO </strong>
                            {this.props.register.number_notification}
                        </h6>

                        <section className="filter-section-dsch-multi-form">
                            <section className="alert alert-dark text-center col-5 m-auto">
                                <a target="_blank" rel="noopener noreferrer"
                                   href={`${SERVER}${this.props.register.file_notification}`}>
                                    Ver Archivo
                                </a>
                            </section>
                            {this.props.register.finalized === false ? editButton : null}
                            {/*this.props.register.finalized === false ? addButton : null*/}
                        </section>
                    </section>
                </div>
            );
        }
    }
}

const mapStateToProps = (state) => ({
    register: state.complete.register
});

export default connect(
    mapStateToProps, {register_notification})(FormNotificationFile);