import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import ViewModification from "./ViewModification";
import { finalized_modification } from "../../../actions/adjustments/finalized_modification";


export class ListModification extends Component {

    state = {
        objetView: null
    }

    static propTypes = {
        modifications: PropTypes.array,
        finalized_modification: PropTypes.func.isRequired,
    }

    getObjectView = (obj) => this.setState({ objetView: obj});

    onFinalizedModification = (pk) => {
        this.props.finalized_modification(pk);
    }

    render() {

        if (this.props.modifications.length > 0){
            return (
                <Fragment>
                <table  className="table">
                    <tbody className="table-scroll">
                    {
                        this.props.modifications.map((modification, index) => (
                            <tr className="mx-auto" tabIndex={index} key={modification.id}>
                                <td width={100}>
                                    {
                                        index === 0
                                            ?
                                            <div className={`${ modification.is_active ? '' : 'mt-4'} alert alert-dark text-center`}>
                                                <h6>Creación</h6>
                                                {
                                                    modification.is_active
                                                        ?
                                                        <div className="text-center">
                                                           <button
                                                               className="btn btn-dark btn-block"
                                                               onClick={
                                                                   this.onFinalizedModification.bind(
                                                                       this, modification.id
                                                                   )}>
                                                               Terminar
                                                           </button>
                                                       </div>
                                                        :
                                                        <div className="text-center">
                                                           <button className="btn btn-dsch btn-block"
                                                                   onClick={
                                                                   this.onFinalizedModification.bind(
                                                                       this, modification.id
                                                                   )}>
                                                               Activar
                                                           </button>
                                                       </div>
                                                }
                                            </div>
                                            :
                                            <div className={`${ modification.is_active ? '' : 'mt-4'} alert alert-dark text-center`} >
                                                <h6>Mod. { index }</h6>
                                                {
                                                    modification.is_active
                                                        ?
                                                        <div className="text-center">
                                                           <button className="btn btn-dark btn-block"
                                                                   onClick={
                                                                   this.onFinalizedModification.bind(
                                                                       this, modification.id
                                                                   )}>
                                                               Terminar
                                                           </button>
                                                       </div>
                                                        :
                                                        <div className="text-center">
                                                           <button className="btn btn-dsch btn-block"
                                                                   onClick={
                                                                   this.onFinalizedModification.bind(
                                                                       this, modification.id
                                                                   )}>
                                                               Activar
                                                           </button>
                                                       </div>
                                                }
                                            </div>
                                    }
                                </td>
                                {
                                    modification.probative.map((obj, number) => (
                                       <td width={number === 0 ? 300 : 250} key={number}>
                                           <div className="alert alert-warning text-center">
                                               {
                                                   obj.collegiate === 1
                                                       ?
                                                       'Comisión de Planes y Programas de estudio'
                                                       :
                                                   obj.collegiate === 2
                                                       ?
                                                       'Consejo Divisional'
                                                       :
                                                   obj.collegiate === 3
                                                       ?
                                                       'Consejo Académico'
                                                       :
                                                   obj.collegiate === 4
                                                       ?
                                                       'Colegio Académico'
                                                       :
                                                   obj.collegiate === 5
                                                       ?
                                                       'Plan de estudios '
                                                       :
                                                       null

                                               }
                                               <div className="text-center">
                                                   <button className="btn btn-warning btn-block"
                                                           data-toggle="modal"
                                                           data-target="#viewModification"
                                                           onClick={
                                                            this.getObjectView.bind(
                                                                this, obj)} >
                                                    Ver Detalle
                                                   </button>
                                               </div>
                                           </div>
                                       </td>
                                    ))
                                }
                            </tr>
                        ))
                    }
                    </tbody>
                </table>
                    <ViewModification obj={this.state.objetView} />
                </Fragment>
            );
        } else{
            return (
                <div className="alert col-8 mx-auto alert alert-danger mt-5 pb-5">
                    <h2 className="text-center mt-5"><strong>NO HAY MODIFICACIONES</strong></h2>
                </div>
            );
        }

    }
}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, { finalized_modification } )(ListModification);
