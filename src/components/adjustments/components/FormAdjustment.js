import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from "prop-types";

import PlansAdjustments from "./PlansAdjustments";
import { ConfigDateSave } from '../../FormatDate';
import { add_approach } from "../../../actions/adjustments/add_approach";


export class FormAdjustment extends Component {

    state = {
        academic_curricula: 1,
        file: '',
        code: '',
        date: '',
        description: ''
    }

    static propTypes = {
        add_approach: PropTypes.func.isRequired
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onChangeFile = (e) =>  this.setState({file: e.target.files[0]});

    onSubmit = (e) => {
        e.preventDefault();
        const data = new FormData();
        data.append('code', this.state.code);
        data.append('description', this.state.description);
        data.append('academic_curricula', this.state.academic_curricula);

        if (this.state.date !== ''){
            data.append('date', ConfigDateSave(this.state.date));
        }

        try {
            data.append('file', this.state.file, this.state.file.name);
            // envío de información
            this.props.add_approach(data);

        } catch (e){
            if (e.TypeError === undefined){
                if (window.confirm('No se adjuntó archivo. ¿Desea continuar con el registro?')){
                    // envío de información
                    this.props.add_approach(data);
                }
            }
        }

        this.setState({
            academic_curricula: 1,
            file: '',
            code: '',
            date: '',
            description: ''
        });
        document.getElementById("form").reset();
    }

    render() {
        return (
            <div className="modal fade" id="AddAdjustment" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header title-detail">
                            <h5 className="modal-title" id="exampleModalLabel">
                                Registro de Adecuaciones  y
                                Modificaciones para Planes y Programas DCSH
                            </h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <form className="mx-auto col-11" onSubmit={this.onSubmit} id="form">
                                <div className="filter-section-dsch-multi-form mb-3">
                                    <label>Número de Sesión:</label>
                                    <input type="text" className="form-control text-center col-9"
                                           name="code" value={this.state.code}
                                           onChange={this.onChange} required/>
                                </div>

                                <div className="filter-section-dsch-multi-form mb-3">
                                    <label className="pt-2">Descripción:</label>
                                    <input type="text" className="form-control text-center col-9"
                                           name="description" value={this.state.description}
                                           onChange={this.onChange} required />
                                </div>

                                <div className="filter-section-dsch-multi-form mb-3">
                                    <label className="pt-2">Fecha de Propuesta:</label>
                                    <input type="date" className="form-control text-center col-9"
                                           name="date" value={this.state.date}
                                           onChange={this.onChange} required/>
                                </div>

                                <div className="filter-section-dsch-multi-form mb-3">
                                    <label className="pt-2">Órgano Colegiado:</label>
                                    <input type="file" className="form-control text-center col-9"
                                           name="file" value={this.file} accept="application/pdf"
                                           onChange={this.onChangeFile} required/>
                                </div>

                                <div className="filter-section-dsch-multi-form mb-3">
                                    <label className="pt-2">Plan de Estudio:</label>
                                    <PlansAdjustments
                                        onChange={this.onChange}
                                        select_plan={this.state.academic_curricula.toString()}
                                    />
                                </div>


                                <div className="mb-3">
                                    <button className="btn btn-primary float-right">Guardar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, { add_approach } )(FormAdjustment);
