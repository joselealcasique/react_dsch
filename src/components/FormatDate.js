 export function ConfigDateSave(data)  {
    let date = new Date(data.toString());
    let userTimezoneOffset = date.getTimezoneOffset() * 60000;
    let d = new Date(date.getTime() + userTimezoneOffset);
    let date_final = new Date(Date.parse(d.toString())).toISOString()
    return date_final;
}


export const FormatDate = (date) => {
    let format = new Date(date);
    return get_Format(format);
}

const get_Format = (date) => {
    return date.getDate().toString() +' de '+ get_Month(date) +' del '+ date.getFullYear().toString();
};

const get_Month = (date) => {
    let months = [
        "Enero", "Febrero", "Marzo", "Abril",
        "Mayo", "Junio", "Julio", "Agosto",
        "Septiembre", "Octubre", "Noviembre", "Diciembre",
    ];
    return months[date.getMonth()];
};
