import React,{ Component } from 'react';
import {connect} from "react-redux";
import PropType from "prop-types";

import { add_observation } from '../../../actions/concourse/addobservation'


export class Observation extends Component {

    state = {
        edit: false,
        observation: '',
        start_date: null,
        end_date: null
    }

    static propTypes = {
        concourse: PropType.object,
        add_observation: PropType.func.isRequired

    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onEdit = (e) => this.setState({edit: true});
    onCancel = (e) => this.setState({edit: false, observation: ''});
    onSubmit = (e) => {

        if (this.props.concourse.is_active === true){
            if (this.state.observation.trim() === ''){
                    alert("Agregar Observaciones");
                    this.setState({observation: ''})
                    return false;

            }else{
                this.props.add_observation(
                    this.state.observation, this.state.start_date, this.state.end_date, this.props.concourse.id)
            }

        } else {
            alert('Cambios no realizados. La convocatoria no está activa')
        }
        this.setState({edit: false, observation: ''});
    }

    render() {

        let formObservation = null;

        if (this.props.concourse.observation === null | this.state.edit === true){

            if (this.props.concourse.is_active === true ) {
                formObservation = (
                    <div className="text-center">
                    <textarea rows={2} value={this.state.observation} className="form-control"
                              onChange={this.onChange} name="observation"
                    />
                        <button className="btn btn-primary" onClick={this.onSubmit}>Guardar</button>
                        {this.props.concourse.observation === null ? null
                            :
                            <button className="btn btn-warning ml-2" onClick={this.onCancel}>
                                Cancelar
                            </button>
                        }

                    </div>
                );
            } else {
                formObservation = (
                    <section className="text-center">
                        <h2>NO APLICA</h2>
                    </section>
                );
            }
        } else{
            formObservation = (
                <section className="ml-3 text-center">
                    <h6><strong>Descripción: </strong>{this.props.concourse.observation}</h6>
                    {
                        this.props.concourse.is_active === true
                            ?
                            <button className="btn btn-warning" onClick={this.onEdit}>
                                Editar
                            </button>
                            :
                            null
                        }
                </section>

            );
        }

        return (
            <section className="card col-4 p-2 m-1">
                <h5 className="text-center"><strong>Observaciones Generales</strong></h5>
                <hr></hr>
                <div className=" mt-1 mx-2">
                    {formObservation}
                </div>
            </section>
        )
    }
}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, { add_observation })(Observation);