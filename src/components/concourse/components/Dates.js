import React,{ Component } from 'react';
import {connect} from "react-redux";
import PropType from "prop-types";

import { add_observation } from '../../../actions/concourse/addobservation';
import {ConfigDateSave, FormatDate} from '../../FormatDate';


export class Dates extends Component {

    state = {
        edit: false,
        observation: null,
        start_date: '',
        end_date: ''
    }

    static propTypes = {
        concourse: PropType.object,
        add_observation: PropType.func.isRequired,
        type_concourse: PropType.number.isRequired,
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onEdit = (e) => this.setState({edit: true});
    onCancel = (e) => this.setState({edit: false, start_date: null, end_date: null});

    onSubmit = (e) => {
        if (this.props.concourse.is_active === true) {

            if (this.state.start_date !== '' | this.state.end_date !== ''){
                let start = null;
                let end = null;

                if (this.state.start_date !== ''){
                    start = ConfigDateSave(this.state.start_date);
                }

                if (this.state.end_date !== ''){
                    end = ConfigDateSave(this.state.end_date);
                }
                this.props.add_observation(this.state.observation, start, end, this.props.concourse.id);

            } else {
                alert('Se debe agregar por lo menos una fecha')
            }

        } else {
            alert('Cambios no realizados. La convocatoria no está activa')
        }
        this.setState({
            edit: false,
            observation: null,
            start_date: '',
            end_date: ''
        });
    }


    render() {

        let formDates = null;

        if (this.state.edit === true){
            formDates = (
                <div className="text-center">
                    <div>
                        <strong>Fecha de Inicio</strong>
                        <input type="date" value={this.state.start_date} className="form-control"
                               onChange={this.onChange} name="start_date"
                        />
                    </div>
                    {
                        this.props.type_concourse == 3
                            ?
                            null
                            :
                            <div className="mt-2 mb-2">
                               <strong>Fecha de Fin</strong>
                               <input type="date" value={this.state.end_date} className="form-control"
                                      onChange={this.onChange} name="end_date"
                                />
                           </div>
                    }
                    <button className="btn btn-primary" onClick={this.onSubmit}>Guardar</button>
                        { this.props.concourse.start_date === null ? null
                            :
                            <button className="btn btn-warning ml-2" onClick={this.onCancel}>
                                Cancelar
                            </button>
                        }

                </div>
            );
        } else{
            formDates = (
                <section className="ml-3 text-center">
                    {this.props.concourse.start_date === null ? null :
                        <h6><strong>Inicio: </strong>
                            {FormatDate(this.props.concourse.start_date)}</h6>
                    }
                    {this.props.concourse.end_date === null | this.props.type_concourse === 3 ? null :
                        <h6><strong>Fin: </strong>
                            {FormatDate(this.props.concourse.end_date)}</h6>}
                    {
                        this.props.concourse.is_active === false
                            ?
                            null
                            :
                            this.props.concourse.start_date === null & this.props.concourse.end_date === null
                            ?
                                <button className="btn btn-info" onClick={this.onEdit}>
                                Agregar Fechas
                                </button>
                                :
                                <button className="btn btn-warning" onClick={this.onEdit}>
                                    Editar
                                </button>
                    }
                </section>

            );
        }

        return (
            <section className="card col-4 p-2 m-1 mt-1">
                <h5 className="text-center">
                    <strong>
                        {
                            this.props.type_concourse === 3
                                ? 'Fecha de Inicio de Labores'
                                :
                            this.props.type_concourse === 4
                                ? 'Fechas de Contratación'
                                :
                                'Fechas de Convocatoria'
                        }
                    </strong>
                </h5>
                <hr></hr>
                <div className=" mt-1 mx-2">
                    {formDates}
                </div>
            </section>
        )
    }
}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, { add_observation })(Dates);