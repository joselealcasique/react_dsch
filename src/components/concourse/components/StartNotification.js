import React, { Component } from 'react';
import PropType from "prop-types";
import {connect} from "react-redux";

import { add_dictum_or_notification } from '../../../actions/concourse/adddictumornotification';
import {SERVER} from "../../../actions/server";


export class StartNotification extends Component{

    state = {
        file: null,
        edit: false
    }

    static propTypes = {
        notification: PropType.object,
        is_active: PropType.bool.isRequired,
        pk: PropType.number.isRequired,
        add_dictum_or_notification: PropType.func.isRequired
    }

    onFileNotification = (e) => this.setState({file: e.target.files[0]});
    onEdit = (e) => this.setState({edit: true})
    onCancel = (e) => this.setState({edit: false})

    onSubmit = (e) => {
        if (this.props.is_active === true){
            if (this.state.file === null) {
                alert('Se debe adjuntar un Archivo');
                return;
            }

            const data = new FormData();
            data.append('file', this.state.file, this.state.file.name);
            this.props.add_dictum_or_notification(data, this.props.pk, 4);

        } else {
            alert('Cambios no realizados. La convocatoria no está activa')
        }
        this.setState({edit: false, file: null});
    }


    render() {

        let formNotification = null;

        if (this.props.is_active === true) {

            formNotification = (
                <div>
                    <input type="file" className="form-control" name="file"
                           value={this.file} onChange={this.onFileNotification} accept="application/pdf"
                    />
                    <div className="text-center mt-3">
                        <button className="btn btn-primary m-auto" onClick={this.onSubmit}>
                            Guardar
                        </button>
                        {
                            this.props.notification !== null ?
                                <button className="btn btn-warning ml-2" onClick={this.onCancel}>
                                    Cancelar
                                </button>
                                :
                                null
                        }
                    </div>
                </div>
            );
        } else {
            formNotification = (
                <section className="text-center">
                    <h2>NO APLICA</h2>
                </section>
            );
        }

        let viewDictum = null;

        if (this.props.notification !== null){
            viewDictum = (
                <section className="text-center mt-2 mb-2">
                    <a target="_blank" rel="noopener noreferrer" className="btn btn-dsch"
                       href={`${SERVER}${this.props.notification.file}`}>
                        <strong>Ver Notificación: </strong>
                    </a>
                    {this.props.is_active === true ?
                        <button className="btn btn-warning ml-3" onClick={this.onEdit}>
                            Editar Notificación
                        </button>
                        :
                        null
                    }
                </section>
            )

            if (this.state.edit === true){
                viewDictum = formNotification;
            }

        } else{
            viewDictum = formNotification;
        }

        return (
            <section className="card col-6 p-2 m-1">
                <h5 className="text-center"><strong>Notificación de Inicio de Labores</strong></h5>
                <hr></hr>
                <div className=" mt-1 mx-2">
                    {viewDictum}
                </div>
            </section>
        )
    }
}

const mapStateToProps = (state) => ({});

export default connect(
    mapStateToProps, { add_dictum_or_notification, } )(StartNotification);