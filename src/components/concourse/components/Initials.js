import React, { Component } from 'react';
import PropType from "prop-types";
import {connect} from "react-redux";

import Causal from "./Causal";
import FileExtend from "./FileExtend";
import Publication from "./Publication";
import Observation from "./Observation";
import Dates from "./Dates";
import Number from "./Number";


export class Initials extends  Component {

    static propTypes = {
        concourse: PropType.object,
        type_concourse: PropType.number.isRequired,
    }

    render() {
        return (
            <section className="modal-footer">
                <section className="filter-section-dsch-multi-form">
                    {
                        this.props.type_concourse === 1 | this.props.type_concourse === 4
                            ? <Causal
                                causal={this.props.concourse.causal}
                                is_active={this.props.concourse.is_active}
                                pk={this.props.concourse.id}
                            />
                            :
                            null
                    }

                    <Dates
                        concourse={ this.props.concourse } type_concourse={this.props.type_concourse}
                    />

                    {
                        this.props.type_concourse === 4
                            ?
                            <Observation
                                concourse={this.props.concourse}
                            />
                            :
                            <FileExtend
                                concourse={this.props.concourse}
                                type_concourse={this.props.type_concourse}
                            />

                    }

                </section>

                <section className="filter-section-dsch-multi-form">
                    {
                        this.props.type_concourse === 4
                            ? null
                            :
                            <Publication
                                concourse={this.props.concourse}
                            />
                    }

                    {
                        this.props.type_concourse === 4
                            ? null
                            :
                            <Observation
                                concourse={this.props.concourse}
                            />

                    }

                    {
                        this.props.type_concourse === 4
                            ? null
                            :
                            <Number
                                concourse={this.props.concourse}
                            />
                    }
                </section>

            </section>
        )
    }
}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, null)(Initials);