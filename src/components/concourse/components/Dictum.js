import React, { Component } from 'react';
import PropType from "prop-types";
import {connect} from "react-redux";

import { add_dictum_or_notification } from '../../../actions/concourse/adddictumornotification';
import {SERVER} from "../../../actions/server";
import { ConfigDateSave, FormatDate } from '../../FormatDate'


export class Dictum extends Component{

    state = {
        file: null,
        edit: false,
        date: ''
    }

    static propTypes = {
        dictum: PropType.object,
        is_active: PropType.bool.isRequired,
        pk: PropType.number.isRequired,
        add_dictum_or_notification: PropType.func.isRequired
    }

    onFileDictum = (e) => this.setState({file: e.target.files[0]});
    onEdit = (e) => this.setState({edit: true})
    onChange = (e) => this.setState({[e.target.name]: e.target.value})
    onCancel = (e) => this.setState({edit: false})

    onSubmit = (e) => {

        if (this.props.is_active === true){
            if (this.props.dictum === null){
                if (this.state.file === null) {
                    alert('Se debe adjuntar un Archivo');
                    return;
                }

                if (this.state.date === ''){
                    alert('Se require agregar fecha para continuar');
                    return;
                }

                const data = new FormData();
                data.append('file', this.state.file, this.state.file.name);
                data.append('date', ConfigDateSave(this.state.date));
                this.props.add_dictum_or_notification(data, this.props.pk, 1);

            } else {
                const data = new FormData();

                if (this.state.file !== null) {
                    data.append('file', this.state.file, this.state.file.name);
                }

                if (this.state.date !== ''){
                    data.append('date', ConfigDateSave(this.state.date));
                }

                this.props.add_dictum_or_notification(data, this.props.pk, 1);
            }

        } else {
            alert('Cambios no realizados. La convocatoria no está activa')
        }

        this.setState({edit: false, file: null, date: ''});
    }


    render() {

        let formDictum = null;

        if(this.props.is_active === true) {
             formDictum = (
                <div>
                    <label>
                        Fecha de Dictamen {
                        this.state.edit === true ?
                            '(Actual - ' + FormatDate(this.props.dictum.date_created) + ')'
                            :
                            null }
                    </label>
                    <input type="date" className="form-control" name="date"
                           value={this.state.date} onChange={this.onChange}
                    />
                    <input type="file" className="form-control mt-4" name="file"
                           value={this.file} onChange={this.onFileDictum} accept="application/pdf"
                    />
                    <div className="text-center mt-3">
                        <button className="btn btn-primary m-auto" onClick={this.onSubmit}>
                            Guardar
                        </button>
                        {
                            this.props.dictum !== null ?
                                <button className="btn btn-warning ml-2" onClick={this.onCancel}>
                                    Cancelar
                                </button>
                                :
                                null
                        }
                    </div>
                </div>
            );
        } else {
            formDictum = (
                <section className="text-center">
                    <h2>NO APLICA</h2>
                </section>
            );
        }

        let viewDictum = null;

        if (this.props.dictum !== null){
            viewDictum = (
                <section className="text-center mb-2">
                    <h6>
                        Fecha de dictamen: {
                        this.props.dictum.date_created !== null
                            ?
                            FormatDate(this.props.dictum.date_created)
                            :
                            <strong>Fecha Pendiente</strong>
                    }
                    </h6>
                    <a target="_blank" rel="noopener noreferrer" className="btn btn-dsch"
                       href={`${SERVER}${this.props.dictum.file}`}>
                        <strong>Ver Dictamen: </strong>
                    </a>
                    {this.props.is_active === true ?
                        <button className="btn btn-warning ml-3" onClick={this.onEdit}>
                            Editar Dictamen
                        </button>
                        :
                        null
                    }
                </section>
            )

            if (this.state.edit === true){
                viewDictum = formDictum;
            }

        } else{
            viewDictum = formDictum;
        }

        return (
            <section className="card col-6 p-2 m-1">
                <h5 className="text-center"><strong>Dictamen </strong></h5>
                <hr></hr>
                <div className=" mt-1 mx-2">
                    {viewDictum}
                </div>
            </section>
        )
    }
}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, { add_dictum_or_notification, } )(Dictum);