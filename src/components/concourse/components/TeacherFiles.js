import React, { Component } from 'react';
import PropType from "prop-types";
import {connect} from "react-redux";
import {SERVER} from "../../../actions/server";

import { add_teacher_file } from "../../../actions/concourse/add_teache_file";


export class TeacherFiles extends  Component {

    state = {
        editFile: false,
        editGrade: false,
        editAnnexed: false,
        file: null,
        grade: null,
        annexed: null,
    }

    static propTypes = {
        teacher: PropType.object,
        add_teacher_file: PropType.func.isRequired
    }

    onFileUpload = (e) => this.setState({file: e.target.files[0]});
    onGradeUpload= (e) => this.setState({grade: e.target.files[0]});
    onAnnexedUpload = (e) => this.setState({annexed: e.target.files[0]});

    onEditFile = (e) => this.setState({editFile: this.state.editFile ? false : true})
    onEditGrade = (e) => this.setState({editGrade: this.state.editGrade ? false : true})
    onEditAnnexed = (e) => this.setState({editAnnexed: this.state.editAnnexed ? false : true})

    onSubmitFile = (e) => {
        e.preventDefault();
        let data = new FormData()
        try {
            data.append('file', this.state.file, this.state.file.name);
            this.props.add_teacher_file(data, this.props.teacher.id, 1);

        } catch (e){ if (e.TypeError === undefined){}}
        document.getElementById('formFile').reset();
        this.setState({editFile: false});
    }

    onSubmitGrade = (e) => {
        e.preventDefault();
        let data = new FormData()
        try {
            data.append('grade', this.state.grade, this.state.grade.name);
            this.props.add_teacher_file(data, this.props.teacher.id, 2);

        } catch (e){ if (e.TypeError === undefined){}}
        document.getElementById('formGrade').reset();
        this.setState({editGrade: false })
    }

    onSubmitAnnexed = (e) => {
        e.preventDefault();
        let data = new FormData()
        try {
            data.append('annexed', this.state.annexed, this.state.annexed.name);
            this.props.add_teacher_file(data, this.props.teacher.id, 3);

        } catch (e){ if (e.TypeError === undefined){}}
        document.getElementById('formAnnexed').reset();
        this.setState({editAnnexed: false});
    }

    render() {

        return (
            <section>
                <section className="filter-section-dsch-multi-form">
                    <div className="col-4 p-1">
                        <div className="card py-3 px-2 text-center">
                            <h6><strong>Solicitud</strong></h6>
                            {
                                this.props.teacher.file === null | this.state.editFile
                                    ?
                                    <form onSubmit={this.onSubmitFile} id="formFile">
                                        <input type="file" className="form-control mt-4" name="file"
                                               value={this.file} onChange={this.onFileUpload}
                                               accept="application/pdf" required
                                        />
                                        <div className="mt-2">
                                        {
                                            this.props.teacher.file !== null
                                                ?
                                                <button onClick={this.onEditFile}
                                                        className="btn btn-danger mr-3">
                                                    Cancelar
                                                </button>
                                                :
                                                null
                                        }
                                        <button className="btn btn-dark">Guardar</button>
                                        </div>
                                    </form>
                                    :
                                    <section>

                                        <a target="_blank" rel="noopener noreferrer"
                                           className="btn btn-dsch mr-3"
                                           href={`${SERVER}${this.props.teacher.file}`}>
                                            <strong>Ver: </strong>
                                        </a>
                                        <button onClick={this.onEditFile} className="btn btn-warning">
                                            Editar
                                        </button>
                                    </section>
                            }
                        </div>
                    </div>

                    <div className="col-4 p-1">
                        <div className="card py-3 px-2 text-center">
                            <h6><strong>Grado Académico</strong></h6>
                            {
                                this.props.teacher.grade === null | this.state.editGrade
                                    ?
                                    <form onSubmit={this.onSubmitGrade} id="formGrade">
                                        <input type="file" className="form-control mt-4" name="grade"
                                               value={this.grade} onChange={this.onGradeUpload}
                                               accept="application/pdf" required
                                        />
                                        <div className="mt-2">
                                        {
                                            this.props.teacher.grade !== null
                                                ?
                                                <button onClick={this.onEditGrade}
                                                        className="btn btn-danger mr-3">
                                                    Cancelar
                                                </button>
                                                :
                                                null
                                        }
                                        <button className="btn btn-dark">Guardar</button>
                                        </div>
                                    </form>
                                    :
                                    <section>

                                        <a target="_blank" rel="noopener noreferrer"
                                           className="btn btn-dsch mr-3"
                                           href={`${SERVER}${this.props.teacher.grade}`}>
                                            <strong>Ver: </strong>
                                        </a>
                                        <button onClick={this.onEditGrade} className="btn btn-warning">
                                            Editar
                                        </button>
                                    </section>
                            }
                        </div>
                    </div>

                    <div className="col-4 p-1">
                        <div className="card py-3 px-2 text-center">
                            <h6><strong>Anexo</strong></h6>
                            {
                                this.props.teacher.annexed === null | this.state.editAnnexed
                                    ?
                                    <form onSubmit={this.onSubmitAnnexed} id="formAnnexed">
                                        <input type="file" className="form-control mt-4" name="annexed"
                                               value={this.annexed} onChange={this.onAnnexedUpload}
                                               accept="application/pdf" required
                                        />
                                        <div className="mt-2">
                                        {
                                            this.props.teacher.annexed !== null
                                                ?
                                                <button onClick={this.onEditAnnexed}
                                                        className="btn btn-danger mr-3">
                                                    Cancelar
                                                </button>
                                                :
                                                null
                                        }
                                        <button className="btn btn-dark">Guardar</button>
                                        </div>
                                    </form>
                                    :
                                    <section>

                                        <a target="_blank" rel="noopener noreferrer"
                                           className="btn btn-dsch mr-3"
                                           href={`${SERVER}${this.props.teacher.annexed}`}>
                                            <strong>Ver: </strong>
                                        </a>
                                        <button onClick={this.onEditAnnexed} className="btn btn-warning">
                                            Editar
                                        </button>
                                    </section>
                            }
                        </div>
                    </div>
                </section>
            </section>
        )
    }
}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, { add_teacher_file } )(TeacherFiles);