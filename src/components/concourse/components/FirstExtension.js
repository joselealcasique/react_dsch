import React,{ Component } from 'react';
import {connect} from "react-redux";
import PropType from "prop-types";

import { add_first_extension } from '../../../actions/concourse/addfirstextension';
import {ConfigDateSave, FormatDate} from '../../FormatDate';
import {SERVER} from "../../../actions/server";


export class FirstExtension extends Component {

    state = {
        edit: false,
        first_extension_start_date: '',
        first_extension_end_date: '',
        first_extension_file: null,
    }

    static propTypes = {
        concourse: PropType.object,
        add_first_extension: PropType.func.isRequired

    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onFileChange = (e) => this.setState({first_extension_file: e.target.files[0]});
    onEdit = (e) => this.setState({edit: true});
    onCancel = (e) => this.setState({
        edit: false, first_extension_start_date: null, first_extension_end_date: null});

    onSubmit = (e) => {
        if (this.props.concourse.is_active === true) {

            if
            (
                this.props.concourse.first_extension_end_date === null
                &
                this.props.concourse.first_extension_start_date=== null
            )
            {
                if (this.state.first_extension_end_date !== '' & this.state.first_extension_start_date !== ''){
                    let data = new FormData()
                    data.append(
                        'first_extension_start_date', ConfigDateSave(this.state.first_extension_start_date))
                    data.append(
                        'first_extension_end_date', ConfigDateSave(this.state.first_extension_end_date))

                    try {
                        data.append(
                            'first_extension_file', this.state.first_extension_file, this.state.first_extension_file.name);
                    } catch (e){
                        if (e.TypeError === undefined){}
                    }

                    this.props.add_first_extension(data, this.props.concourse.id);

                } else{
                    alert('Se Necesitan Ambas Fechas de Primer Prórroga');
                }

            } else {
                if (this.state.first_extension_end_date !== '' & this.state.first_extension_start_date !== ''){
                    let data = new FormData()

                    data.append(
                        'first_extension_start_date', ConfigDateSave(this.state.first_extension_start_date))
                    data.append(
                        'first_extension_end_date', ConfigDateSave(this.state.first_extension_end_date))

                    try {
                        data.append(
                            'first_extension_file', this.state.first_extension_file, this.state.first_extension_file.name);
                    } catch (e){
                        if (e.TypeError === undefined){}
                    }

                    this.props.add_first_extension(data, this.props.concourse.id);

                } else{

                    let data = new FormData()

                    if (this.state.first_extension_start_date === ''){
                        data.append(
                        'first_extension_start_date', this.props.concourse.first_extension_start_date)
                    } else{
                        data.append(
                        'first_extension_start_date', ConfigDateSave(this.state.first_extension_start_date))
                    }

                    if (this.state.first_extension_end_date === ''){
                        data.append(
                        'first_extension_end_date', this.props.concourse.first_extension_end_date)
                    } else{
                        data.append(
                        'first_extension_end_date', ConfigDateSave(this.state.first_extension_end_date))
                    }

                    try {
                        data.append(
                            'first_extension_file', this.state.first_extension_file, this.state.first_extension_file.name);
                    } catch (e){
                        if (e.TypeError === undefined){}
                    }

                    this.props.add_first_extension(data, this.props.concourse.id);
                }
            }


        } else {
            alert('Cambios no realizados. La convocatoria no está activa')
        }
        this.setState({
            edit: false,
            first_extension_start_date: '',
            first_extension_end_date: ''
        });
    }


    render() {

        let formDates = null;

        if (this.state.edit === true){
            formDates = (
                <div className="text-center">
                    <div>
                        <strong>Fecha de Inicio
                            {
                               this.props.concourse.first_extension_start_date !== null
                                   ? ' (Actual - ' + FormatDate(
                                       this.props.concourse.first_extension_start_date) + ')'
                                   :
                                   null
                           }
                        </strong>
                        <input type="date" value={this.state.first_extension_start_date} className="form-control"
                               onChange={this.onChange} name="first_extension_start_date"
                        />
                    </div>
                   <div className="mt-2 mb-2">
                       <strong>Fecha de Fin
                           {
                               this.props.concourse.first_extension_end_date !== null
                                   ? ' (Actual - ' + FormatDate(
                                       this.props.concourse.first_extension_end_date) + ')'
                                   :
                                   null
                           }
                       </strong>
                       <input type="date" value={this.state.first_extension_end_date} className="form-control"
                              onChange={this.onChange} name="first_extension_end_date"
                        />
                   </div>

                    <div className="mt-2 mb-2">
                        <strong>
                            Seleccionar Archivo
                        </strong>
                       <input type="file" value={this.first_extension_file}
                              className="form-control" name="first_extension_file"
                              accept="application/pdf" onChange={this.onFileChange}
                        />
                   </div>

                    <button className="btn btn-primary" onClick={this.onSubmit}>Guardar</button>
                        { this.props.concourse.first_extension_start_date === null ? null
                            :
                            <button className="btn btn-warning ml-2" onClick={this.onCancel}>
                                Cancelar
                            </button>
                        }

                </div>
            );
        } else{
            formDates = (
                <section className="ml-3 text-center">
                    {this.props.concourse.first_extension_start_date === null ? null :
                        <h6><strong>Inicio: </strong>
                            {FormatDate(this.props.concourse.first_extension_start_date)}</h6>
                    }
                    {this.props.concourse.first_extension_end_date === null ? null :
                        <h6><strong>Fin: </strong>
                            {FormatDate(this.props.concourse.first_extension_end_date)}</h6>}

                    {this.props.concourse.first_extension_file === null ? null :
                        <a target="_blank" rel="noopener noreferrer" className="btn btn-dsch mr-4"
                           href={`${SERVER}${this.props.concourse.first_extension_file}`}>
                            <strong>Ver archivo: </strong>
                        </a>}

                    {
                        this.props.concourse.is_active === false
                            ?
                            null
                            :
                            this.props.concourse.first_extension_start_date === null
                            &
                            this.props.concourse.first_extension_end_date === null
                            ?
                                <button className="btn btn-info" onClick={this.onEdit}>
                                Agregar Fechas
                                </button>
                                :
                                <button className="btn btn-warning" onClick={this.onEdit}>
                                Editar Fechas
                                </button>
                    }
                </section>

            );
        }

        return (
            <section className="card col-6 p-2 m-1">
                <h5 className="text-center"><strong>Primer Prórroga</strong></h5>
                <hr></hr>
                <div className=" mt-1 mx-2">
                    {formDates}
                </div>
            </section>
        )
    }
}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, { add_first_extension })(FirstExtension);