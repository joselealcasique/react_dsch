import React, { Component } from 'react';
import PropType from "prop-types";
import {connect} from "react-redux";

import { FormatDate, ConfigDateSave } from "../../FormatDate";
import { add_publication } from '../../../actions/concourse/addpublication'


export class Publication extends Component {

    state = {
        edit: false,
        publication: "",
    }

    static propTypes = {
        concourse: PropType.object.isRequired,
        add_publication: PropType.func.isRequired
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onEdit = (e) => this.setState({edit: true});
    onCancel = (e) => this.setState({edit: false});

    onSendPublication = (e) => {
        if (this.props.concourse.is_active === true){

            if (this.state.publication !== null){
                this.props.add_publication(
                    ConfigDateSave(this.state.publication), this.props.concourse.id);

            }else {
                alert("Es necesario agregar una fecha");
            }

        } else {
            alert('Cambios no realizados. La convocatoria no está activa')
        }

        this.setState({edit: false, publication: null});
    }


    render() {
        let viewPublication = null;

        if (this.props.concourse.publication === null | this.state.edit === true){

            if (this.props.concourse.is_active === true) {
                viewPublication = (
                    <div>
                        <input type="date" className="form-control" required name="publication"
                               value={this.state.publication} onChange={this.onChange}
                        />
                        <section className="text-center mt-2">
                            <button className="btn btn-primary" onClick={this.onSendPublication}>
                                Guardar
                            </button>
                            {
                                this.props.concourse.publication !== null ?
                                    <button className="btn btn-warning ml-2" onClick={this.onCancel}>
                                        Cancelar
                                    </button>
                                    :
                                    null
                            }
                        </section>
                    </div>
                );
            } else {
                viewPublication = (
                    <section className="text-center">
                        <h2>NO APLICA</h2>
                    </section>
                );
            }
        } else {
            viewPublication = (
                <section className="ml-3 text-center">
                    <h6>
                        {FormatDate(this.props.concourse.publication)}
                    </h6>
                    <div className="text-center">
                        {
                            this.props.concourse.is_active === true ?
                                <button className="btn btn-warning" onClick={this.onEdit}>
                                    Editar
                                </button>
                                :
                                null
                        }
                    </div>
                </section>
            )
        }

        return (
            <section className="card col-4 p-2 m-1">
                <h5 className="text-center"><strong>Fecha de Publicación</strong></h5>
                <hr></hr>
                <div className=" mt-1 mx-2">
                    {viewPublication}
                </div>
            </section>
        )
    }
}

const mapStateToProps = (state) => ({

});

export default connect(mapStateToProps, { add_publication })(Publication);