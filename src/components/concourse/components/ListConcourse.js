import React, { Component } from 'react';
import { connect } from "react-redux";
import PropType from "prop-types";

import { FormatDate } from '../../FormatDate'
import {Link} from "react-router-dom";


export class ListConcourse extends Component {

    static propTypes = {
        concourse: PropType.object.isRequired,
        type_concourse: PropType.number.isRequired
    }

    render() {
        if (this.props.concourse.list.length > 0) {
            return (
                <table className="table table-striped mt-3">
                    <thead>
                        <tr className="text-center">
                            <th width={200}>Convocatoria</th>
                            <th width={ this.props.type_concourse === 4 ? 900 : 400 }>Descripción</th>

                            {
                                this.props.type_concourse === 4
                                    ?
                                    null
                                    :
                                    <th width={250}>Fecha de Solicitud</th>
                            }

                            {
                                this.props.type_concourse === 4
                                    ?
                                    null
                                    :
                                     <th width={250}>Fecha de envío de Conv.</th>
                            }
                            <th width={150}>Acciones</th>
                        </tr>
                    </thead>
                    <tbody className="table-scroll-c">
                    {this.props.concourse.list.map((obj, index) => (
                        <tr className="text-center" key={index}>
                            <td width={200}>{obj.code}</td>
                            <td width={ this.props.type_concourse === 4 ? 900 : 400 }>
                                {obj.description}
                            </td>

                            {
                                this.props.type_concourse === 4
                                    ?
                                    null
                                    :
                                    <td width={250}>{FormatDate(obj.date_created)}</td>
                            }

                            {
                                this.props.type_concourse === 4
                                    ?
                                    null
                                    :
                                    <td width={250}>{FormatDate(obj.date_send)}</td>
                            }

                            <td width={150}>
                                <Link to={{
                                    pathname: '/concourse-edit',
                                    state: {
                                        id: obj.id,
                                        type_concourse: this.props.type_concourse
                                    }
                                }} >Ver</Link>
                            </td>
                        </tr>
                    ))}
                    </tbody>
                </table>
            );

        } else{
            return (
                <section className="text-center mt-5 alert alert-danger col-8 mx-auto py-5">
                    <h4><strong>NO HAY COINCIDENCIAS</strong></h4>
                </section>
            )
        }
    }
}

const mapStateToProps = (state) => ({
    concourse: state.concourse
});

export default connect(mapStateToProps, null )(ListConcourse);