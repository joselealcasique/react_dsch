import React, { Component } from 'react';
import PropType from "prop-types";
import {connect} from "react-redux";

import { additional_file } from "../../../actions/concourse/additional_file";
import {SERVER} from "../../../actions/server";


export class Additional extends  Component {

    state = {
        file: null,
        name: '',
    }

    static propTypes = {
        concourse: PropType.object,
        type_concourse: PropType.number.isRequired,
        additional_file: PropType.func.isRequired
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onFileNotification = (e) => this.setState({file: e.target.files[0]});
    onSubmit = (e) => {
        e.preventDefault();
        const data = new FormData();
        data.append('file', this.state.file, this.state.file.name);
        data.append('name', this.state.name);
        this.props.additional_file(data, this.props.concourse.id)
        document.getElementById('form').reset();
        this.setState({name: ''});
    }

    render() {
        return (
            <section>
                <form className="filter-section-dsch-multi-form card mt-2 alert-dark pb-3"
                      onSubmit={this.onSubmit} id="form">
                    <div className="col-12 text-center mt-3">
                        <h5><strong>Registro de Archivos Adicionales</strong></h5>
                    </div>
                    <div className="col-9 filter-section-dsch-multi-form mx-auto">
                        <input type="text" className="form-control" name="name" required
                               placeholder="Nombre de Archivo" onChange={this.onChange}
                               value={this.state.name}
                        />

                        <input type="file" className="form-control col-5" name="file"
                           value={this.file} onChange={this.onFileNotification} required
                               accept="application/pdf"
                        />
                        <div className="col-2">
                            <button className="btn btn-dark mt-1">Guardar</button>
                        </div>
                    </div>
                </form>
                <section className="modal-footer mt-5">
                    {
                        this.props.concourse.additional.map((obj, index) => (
                            <section className="col-4 mx-auto card p-2 text-center mt-1" key={index}>
                                <h6><strong>{ obj.name }</strong></h6>
                                <section className="card section-button">
                                    <a target="_blank" rel="noopener noreferrer" className="btn btn-dark"
                                       href={`${SERVER}${obj.file}`}>
                                        Ver Archivo
                                    </a>
                                </section>
                            </section>
                        ))
                    }
                </section>
            </section>
        )
    }
}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, { additional_file })(Additional);