import React, { Component } from 'react';
import PropType from "prop-types";
import {connect} from "react-redux";

import { add_causal } from '../../../actions/concourse/addcausal';
import {ConfigDateSave, FormatDate} from "../../FormatDate";


export class Causal extends Component{

    state = {
        text: "",
        start_date: "",
        end_date: "",
        edit: false
    }

    static propTypes = {
        causal: PropType.object,
        is_active: PropType.bool,
        pk: PropType.number,
        add_causal: PropType.func.isRequired
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onEdit = (e) => this.setState({edit: true});
    onCancel = (e) => this.setState({edit: false});

    onSubmit = (e) => {
        e.preventDefault();
        if (this.props.is_active === true){
            if (this.props.causal === null) {
                if (this.state.text.trim() === ''){
                    alert("Agregar Descripción");
                    this.setState({text: ''})
                    return false;
                }

                if (this.state.start_date === ''){
                    alert("Agregar Fecha de Inicio");
                    return false;
                }

                if (this.state.end_date === ''){
                    alert("Agregar Fecha de Fin");
                    return false;
                }

            }

            let start = null;
            let end = null;
            let text = null;

            if (this.state.start_date !== ''){
                start = ConfigDateSave(this.state.start_date);

            } else {
                start = this.props.causal.start_date
            }

            if (this.state.end_date !== ''){
                end = ConfigDateSave(this.state.end_date);

            } else {
                end = this.props.causal.end_date
            }

            if (this.state.text.trim() === ''){
                text = this.props.causal.text;

            } else {
                text = this.state.text;
            }

            this.props.add_causal(text, start, end, this.props.pk);
        } else {
            alert('Cambios no realizados. La convocatoria no está activa')
        }
        this.setState({ text: '', start_date: "", end_date: "", edit: false });
    }

    render() {

        let formCausal = null;

        if (this.props.causal === null | this.state.edit === true){

            if (this.props.is_active === true) {
                formCausal = (
                    <div>
                        <div className="text-center">
                            <strong>Descripción</strong>
                            <input type="text" className="form-control text-center" name="text"
                                   value={this.state.text} onChange={this.onChange}
                            />
                        </div>

                        <div className="text-center mt-3">
                            <strong>Fecha de Inicio</strong>
                            <input type="date" className="form-control text-center" name="start_date"
                                   value={this.state.start_date} onChange={this.onChange}
                            />
                        </div>

                        <div className="text-center mt-3">
                            <strong>Fecha de Fin</strong>
                            <input type="date" className="form-control text-center" name="end_date"
                                   value={this.state.end_date} onChange={this.onChange}
                            />
                        </div>

                        <div className="text-center mt-3">
                            <button className="btn btn-primary" onClick={this.onSubmit}>Guardar</button>
                            {this.props.causal === null ? null
                                :
                                <button className="btn btn-warning ml-2" onClick={this.onCancel}>
                                    Cancelar
                                </button>
                            }
                        </div>
                    </div>
                );
            } else {
                formCausal = (
                    <section className="text-center">
                        <h2>NO APLICA</h2>
                    </section>
                );
            }
        } else {
            formCausal = (
                <section className="ml-3">
                    <h6><strong>Descripción: </strong>{this.props.causal.text}</h6>
                    <h6><strong>Inicio: </strong>{FormatDate(this.props.causal.start_date)}</h6>
                    <h6><strong>Fin: </strong>{FormatDate(this.props.causal.end_date)}</h6>
                    <div className="text-center mt-3">
                        {
                            this.props.is_active === true
                                ?
                                <button className="btn btn-warning" onClick={this.onEdit}>
                                    Editar Causal
                                </button>
                                :
                                null
                        }
                    </div>

                </section>
            );
        }

        return (
            <section className="card col-4 p-2 m-1">
                <h5 className="text-center"><strong>Información de Causal</strong></h5>
                <hr></hr>
                <div className=" mt-1 mx-2">
                    {formCausal}
                </div>
            </section>
        )
    }
}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, { add_causal })(Causal);