import React from "react";
import Investigation from "./Investigation";


export class ApprovedProject extends React.Component {

    state = {
        title_page: 'Proyecto de Investigación Aprobados',
        option: 2

    }

    render() {
        return <Investigation
            title_page={this.state.title_page} option_request={this.state.option}
        />
    }

}

export default ApprovedProject;