import React, { Component, Fragment } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import ViewDatesProjects from "./components/ViewDatesProjects";
import ViewFormLetters from "./components/ViewFormLetters";
import ViewOptionsProcess from "./components/ViewOptionsProcess";
import DataInformation from "./components/DataInformation";
import { get_letters_project_investigation } from "../../actions/investigation/get_letters_project_investigation";
import { set_letter_project_investigation } from "../../actions/investigation/set_letter_project_investigation";
import { set_project_editable } from "../../actions/investigation/set_project_editable";
import { send_project_to_commission } from "../../actions/investigation/send_project_to_commission";
import { finalized_process_project } from "../../actions/investigation/finalized_process_project";


export class ViewProject extends Component {

    state = {
        pk: 0,
        list_departament: [],
        obj: null,
        letter: null,
        edit: true,
        btnCancel: false,
        option: 0,
        departament: ''
    }

    static propTypes = {
        user: PropTypes.object.isRequired,
        letters: PropTypes.array,
        get_letters_project_investigation: PropTypes.func.isRequired,
        set_letter_project_investigation: PropTypes.func.isRequired,
        set_project_editable: PropTypes.func.isRequired,
        send_project_to_commission: PropTypes.func.isRequired,
        finalized_process_project: PropTypes.func.isRequired,
        permissions: PropTypes.array,
    }

    componentDidMount() {
        const params = this.props.location.state;
        this.setState({
            pk: params.pk,
            list_departament: params.obj.list_departament,
            obj: params.obj,
            option: params.option
        });
        this.props.get_letters_project_investigation(params.pk);

        if (this.props.user.departament === 'view_academic_humanities'){
            this.setState({departament: 'Humanidades'});

        } else if (this.props.user.departament === 'view_academic_social_sciences'){
            this.setState({departament: 'Ciencias Sociales'});

        } else if (this.props.user.departament === 'view_academic_institutional_studies'){
            this.setState({departament: 'Estudios Institucionales'});
        }
    }

    onLoadFileLetter = (e) => {
        this.setState({letter: e.target.files[0]});
    }

    onSubmit = (e) => {
        e.preventDefault();
        let data = new FormData()
        data.append('file', this.state.letter, this.state.letter.name);
        data.append('departament', this.state.departament);
        this.props.set_letter_project_investigation(data, this.state.pk);
        document.getElementById('form-letter').reset()
    }

    onProjectEditable = (e) => {
        this.props.set_project_editable(this.state.pk);
        setTimeout(this.onRedirectListProjects, 600)
    }

    sendProjectToCommission = () => {
        this.props.send_project_to_commission(this.state.pk);
        window.setTimeout(this.onRedirectListProjects, 600);

    }

    onRedirectListProjects = () => {
        if (this.state.option === 1) {
            return this.props.history.push('/project-list');

        } else if (this.state.option === 2) {
            return this.props.history.push('/project-request');
        }
    }

    render() {
        return (
            <Fragment>
                {
                    this.state.obj !== null
                        ?
                        <section className="mt-3">
                            <div className="col-12 mx-auto alert-warning alert text-center ">
                                <h4><strong>PROYECTO: </strong>{this.state.obj.title}</h4>
                            </div>
                            <section className="filter-section-dsch-multi-form">
                                < ViewDatesProjects
                                    obj={this.state.obj}
                                    pk={this.state.pk}
                                />
                                < ViewFormLetters
                                    onLoadFileLetter={this.onLoadFileLetter}
                                    onSubmit={this.onSubmit}
                                    permissions={this.props.permissions}
                                    obj={this.state.obj}
                                    user={this.props.user}
                                    letters={this.props.letters}
                                    departament={this.state.departament}
                                />
                                {
                                    !this.state.obj.finalized
                                     ?
                                        < ViewOptionsProcess
                                            onProjectEditable={this.onProjectEditable}
                                            permissions={this.props.permissions}
                                            obj={this.state.obj}
                                            user={this.props.user}
                                            pk={this.state.pk}
                                            sendProjectToCommission={this.sendProjectToCommission}
                                            onRedirectListProjects={this.onRedirectListProjects}
                                            finalized_process_project={this.props.finalized_process_project}
                                        />
                                        :
                                        <DataInformation obj={this.state.obj} pk={this.state.pk}/>
                                }

                            </section>
                        </section>
                        :
                        <section>
                            loading..
                        </section>
                }
            </Fragment>
        )
    }

}

const mapStateToProps = (state) => ({
    user: state.auth.user,
    letters: state.investigation.letters,
    permissions: state.auth.permissions
});

export default connect(
    mapStateToProps,
    {
        get_letters_project_investigation,
        set_letter_project_investigation,
        set_project_editable,
        send_project_to_commission,
        finalized_process_project,
    }
)(ViewProject);
