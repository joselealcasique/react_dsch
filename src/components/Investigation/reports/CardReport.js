import React, { Component } from "react";
import PropTypes from "prop-types";
import SelectInitial from "./SelectInitial";
import SelectFinal from "./SelectFinal";


const date = new Date().getFullYear()


export class CardReport extends Component {

    state = {
        initial: date,
        final: date,
    }

    static propTypes = {
        title: PropTypes.string.isRequired,
        number: PropTypes.string.isRequired,
        action: PropTypes.func.isRequired,
        presupuestal: PropTypes.bool.isRequired,
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});

    onGetReport = () => {
        let initial = parseInt(this.state.initial);
        let final = parseInt(this.state.final);

        if ( final < initial){
            alert('El año final debe ser mayor al año inicial');
            return false;
        }

        if (this.props.presupuestal){
            this.props.action(this.props.number, initial, final);
        } else {
            this.props.action(initial, final);
        }


    }

    render() {
        return (
            <section className="card mt-3 col-5 mx-auto px-0">
                <div className="alert bg-form-presupuestal text-center mx-0 mb-0">
                    <h5>
                        <strong>
                            { this.props.title }
                        </strong>
                    </h5>
                </div>
                <div className="text-center alert-warning">
                    <h6 className="col-12 pt-4 mt-0">
                        <strong>
                            Seleccionar rango de años para búsqueda
                        </strong>
                    </h6>
                    <hr></hr>
                    <div
                        className="alert filter-section-dsch-multi-form pt-0">
                        <div className="col-5">
                            <h6>
                                <strong>
                                    Año inicial
                                </strong>
                            </h6>
                            <SelectInitial
                                select_initial={this.state.initial.toString()}
                                onChange={this.onChange}
                            />
                        </div>
                        <div className="col-5">
                            <h6>
                                <strong>
                                    Año Final
                                </strong>
                            </h6>
                            <SelectFinal
                                select_final={this.state.final.toString()}
                                onChange={this.onChange}
                            />
                        </div>

                        <div className="col-2 pt-4">
                            <button className="btn btn-warning" onClick={this.onGetReport}>
                                Enviar
                            </button>
                        </div>
                    </div>
                </div>
            </section>
        )
    }

}


export default CardReport;