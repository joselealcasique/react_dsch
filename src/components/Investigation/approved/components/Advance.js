import React, { Component } from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";

import ModalAdvance from "./components/ModalAdvance";
import ListAdvance from "./components/ListAdvance";
import { add_advance_to_project } from "../../../../actions/investigation/add_advance_to_project";
import { get_list_advance } from "../../../../actions/investigation/get_list_advance";


export class Information extends Component{

    state = {
        visual_register: true,
        file: null,
        zip: null,
        option: false
    }

    static propTypes = {
        obj: PropTypes.object.isRequired,
        user: PropTypes.object.isRequired,
        permissions: PropTypes.array.isRequired,
        add_advance_to_project: PropTypes.func.isRequired,
        get_list_advance: PropTypes.func.isRequired,
        onRedirectFromList: PropTypes.func.isRequired,
    }

    componentDidMount() {
        this.props.get_list_advance(this.props.obj.id);
        if (this.props.obj.pre_terminated | this.props.obj.terminated){
            this.setState({visual_register: false});
        }
    }

    onCheckFileAdvance = (ext) => {
        if (ext === "pdf" || ext === "docx" || ext === "doc"){
            return true;
        } else {
            return false;
        }
    }

    onCheckFileZip = (ext) => {
        if (ext === "zip" || ext === "rar" || ext === "7z"){
            return true;
        } else {
            return false;
        }
    }

    onChangeChecked = (e) => {
        this.setState({option: e.target.checked});
    }

    onChangeFile = (e) => {
        try {
             let name = e.target.files[0].name;
             let extension_name = name.split('.').pop();

            if (e.target.name === 'file'){
                if (this.onCheckFileAdvance(extension_name)){
                    this.setState({file: e.target.files[0]});

                } else {
                    alert('Extensión incompatible');
                    this.onClearToForm();
                }

            } else if (e.target.name === 'zip'){
                if (this.onCheckFileZip(extension_name)){
                  this.setState({zip: e.target.files[0]});

                } else {
                    alert('Extensión incompatible');
                    this.onClearToForm();
                }
            }


        } catch (e){
            if (e.TypeError === undefined){
                return false;
            }
        }

    }

    onClearToForm = () => {
        document.getElementById('form-advance').reset();
    }

    onCheckedUserAuthor = () => {
        let user = this.props.obj.author.find(element => element.user === this.props.user.user);
        if (user === undefined){
            return false;
        }
        return true;
    }

    onSubmit = (e) => {
        e.preventDefault();
        let data = new FormData()
        data.append('file', this.state.file, this.state.file.name)
        data.append('zip', this.state.zip, this.state.zip.name)
        data.append('advance', this.state.option)
        this.props.add_advance_to_project(data, this.props.obj.id)
        this.onClearToForm()

        if (this.state.option){
            this.setState({visual_register: false})
        }
    }

    render() {
        return (
            <section className="col-12 ml-3 text-center m-0 p-0">
                <section className="card py-2">
                    <section className="col-12 filter-section-dsch-multi-form">
                        <section className="col-9 text-right">
                            <h4 className="py-2 mt-1">
                                <strong>
                                    Registro del Informes de Proyecto
                                </strong>
                            </h4>
                        </section>
                        <section className="col-3 ml-auto py-2">
                            {
                                this.state.visual_register & this.onCheckedUserAuthor()
                                    ?
                                    <button className="float-right btn btn-dsch"
                                            data-toggle="modal" data-target="#modal-advance">
                                        Registrar informe
                                    </button>
                                    :
                                    null
                            }
                        </section>
                    </section>
                </section>
                <ListAdvance />
                <ModalAdvance
                    add_advance_to_project={this.props.add_advance_to_project}
                    onSubmit={this.onSubmit}
                    onChangeFile={this.onChangeFile}
                    onChangeChecked={this.onChangeChecked}
                    onClearToForm={this.onClearToForm}
                    file={this.state.file}
                    zip={this.state.zip}
                    option={this.state.option}
                />
            </section>
        )
    }

}

const mapStateToProps = (state) => ({
    permissions: state.auth.permissions,
    user: state.auth.user,
});

export default connect(
    mapStateToProps, { add_advance_to_project, get_list_advance })(Information);