import React, { Component } from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";

import ModalExtension from "./components/ModalExtension";
import ListExtension from "./components/ListExtension";
import { add_file_to_extension } from "../../../../actions/investigation/add_file_to_extension";
import { add_office_to_extension } from "../../../../actions/investigation/add_office_to_extension";
import {
    list_project_investigation_commission
} from "../../../../actions/investigation/get_list_extensions_to_project";
import {
    finalized_process_extension_to_project
} from "../../../../actions/investigation/finalized_process_extension_to_project";


export class Extensions extends Component{

    static propTypes = {
        obj: PropTypes.object.isRequired,
        user: PropTypes.object.isRequired,
        permissions: PropTypes.array.isRequired,
        add_file_to_extension: PropTypes.func.isRequired,
        add_office_to_extension: PropTypes.func.isRequired,
        list_project_investigation_commission: PropTypes.func.isRequired,
        finalized_process_extension_to_project: PropTypes.func.isRequired,
    }

    componentDidMount() {
        this.props.list_project_investigation_commission(this.props.obj.id);
    }

    onCheckedUserAuthor = () => {
        let user = this.props.obj.author.find(element => element.user === this.props.user.user);
        if (user === undefined){
            return false;
        }
        return true;
    }

    onSubmit = (e) => {
        e.preventDefault();
        alert('Enviar')
    }

    render() {
        return (
            <section className="col-12 ml-3 text-center m-0 p-0">
                <section className="card py-2">
                    <section className="col-12 filter-section-dsch-multi-form">
                        <section className="col-9 text-right">
                            <h4 className="py-2 mt-1">
                                <strong>
                                    Registro de Solicitudes de Prórrogas
                                </strong>
                            </h4>
                        </section>
                        <section className="col-3 ml-auto py-2">
                            {
                                this.onCheckedUserAuthor() & this.props.obj.status !== 'Finalizado'
                                    ?
                                    <button className="float-right btn btn-dsch"
                                            data-toggle="modal" data-target="#modal-extension">
                                        Solicitar Prórroga
                                    </button>
                                    :
                                    null
                            }
                        </section>
                    </section>
                </section>
                <ListExtension
                    permissions={this.props.permissions}
                    add_office_to_extension={this.props.add_office_to_extension}
                    finalized_process_extension_to_project={
                        this.props.finalized_process_extension_to_project}
                />
                <ModalExtension
                    add_file_to_extension={this.props.add_file_to_extension}
                    pk={this.props.obj.id}
                />
            </section>
        )
    }

}

const mapStateToProps = (state) => ({
    permissions: state.auth.permissions,
    user: state.auth.user,
});

export default connect(
    mapStateToProps, {
        add_file_to_extension,
        list_project_investigation_commission,
        add_office_to_extension,
        finalized_process_extension_to_project,
    })(Extensions);