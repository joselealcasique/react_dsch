import React, { Component } from "react";
import PropTypes from "prop-types";

import { FormatDate } from "../../../../FormatDate";
import {SERVER} from "../../../../../actions/server";



export class ModalDataPresupuestal extends Component{

    static propTypes = {
        obj: PropTypes.object,
    }

    render() {
        return (
            <div className="modal fade" id="modal-Data-Presupuestal" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered modal-xl" role="document">
                    <div className="modal-content ">
                        <div className="modal-header bg-form-presupuestal">
                            <h5 className="modal-title" id="exampleModalLongTitle">
                                <strong>Información de Asignación Presupuestal</strong>
                            </h5>
                            <button type="button" className="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body pb-5">
                            {
                                this.props.obj !== null
                                    ?
                                    <section className="alert">
                                        <section className="filter-section-dsch-multi-form">
                                            <div className="col-6 alert alert-warning">
                                                <h5 className="text-center">
                                                    <strong>
                                                        {
                                                            this.props.obj.name
                                                        }
                                                    </strong>
                                                </h5>
                                            </div>
                                            <div className="col-6">
                                                <div className="col-12 alert alert-warning">
                                                    <h5 className="text-center">
                                                        <strong>Observaciones: </strong>
                                                        {this.props.obj.observations}
                                                    </h5>
                                                </div>
                                            </div>
                                        </section>

                                        <table className="table mt-4">
                                            <thead class="thead-dark">
                                                <tr>
                                                    <th>Estructura</th>
                                                    <th>Partida Sub especifica</th>
                                                    <th>Forma de asignación</th>
                                                    <th>Fecha de asignación</th>
                                                    <th>Monto</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr className="alert-warning">
                                                    <td>{this.props.obj.code}</td>
                                                    <td>{this.props.obj.sub_code}</td>
                                                    <td>{this.props.obj.assign}</td>
                                                    <td>
                                                        { FormatDate(this.props.obj.date) }
                                                    </td>
                                                    <td>
                                                        $ {this.props.obj.amount}.00
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <div className="col-12 bg-dark my-4">
                                            <a href={`${SERVER}${this.props.obj.file}`}
                                               className="btn btn-dark float-right"
                                               target={"_blank"} rel="noopener noreferrer" >
                                                Ver documentación asignada
                                            </a>
                                        </div>
                                    </section>
                                    :
                                    <section className="py-3">
                                        <h5>
                                            <strong>
                                                loading...
                                            </strong>
                                        </h5>
                                    </section>
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default ModalDataPresupuestal;