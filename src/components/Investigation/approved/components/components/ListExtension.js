import React, { Component } from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";

import ModalOfficeToExtension from "./ModalOfficeToExtension";
import { FormatDate } from "../../../../FormatDate";
import {SERVER} from "../../../../../actions/server";


export  class ListExtension extends Component{

    state = {
        pk: 0
    }


    static propTypes = {
        extensions: PropTypes.array,
        permissions: PropTypes.array.isRequired,
        add_office_to_extension: PropTypes.func.isRequired,
        finalized_process_extension_to_project: PropTypes.func.isRequired
    }

    onSendObjToView = (pk) => this.setState({ pk: pk });

    onCheckPermissionsLeader = () => {
        if (this.props.permissions.includes('investigation.admin_leader_departament')){
            return true;
        }
        return false;
    }

    onCheckPermissionsCommission = () => {
        if (this.props.permissions.includes('investigation.admin_evaluation_commission')){
            return true;
        }
        return false;
    }

    onTerminateProcessExtension = (pk) => {
        this.props.finalized_process_extension_to_project(pk);
    }

    render() {
        return (
            <section className="col-12 card pb-0 pt-3">
                {
                    this.props.extensions.length > 0
                        ?
                        <table className="table table-striped">
                            <thead className="text-center">
                            <tr>
                                <th width={200}>Fecha de Registro</th>
                                <th width={260}>Estado de Solicitud</th>
                                <th width={150}>Prórroga</th>
                                <th width={150}>Oficio</th>
                                <th width={150}>Acción</th>
                            </tr>
                            </thead>
                            <tbody className="text-center table-scroll-presupuestal">
                            {
                                this.props.extensions.map((obj, index) => (
                                    <tr key={index}>
                                        <td width={200} className="pt-3">
                                            { FormatDate(obj.updated) }
                                        </td>
                                        <td width={260} className="pt-3">
                                            { obj.status }
                                        </td>
                                        <td width={150}>
                                            <a href={`${SERVER}${obj.file}`}
                                               className="btn btn-dsch mx-auto"
                                               target={"_blank"}
                                               rel="noopener noreferrer" >
                                                Ver Solicitud
                                            </a>
                                        </td>
                                        <td width={150}>
                                            {
                                                obj.office === null
                                                    ?
                                                    this.onCheckPermissionsLeader()
                                                        ?
                                                        <button
                                                            className="btn btn-warning"
                                                            data-toggle="modal"
                                                            data-target="#modal-extension-office"
                                                            onClick={
                                                                this.onSendObjToView.bind(
                                                                    this, obj.id)
                                                            }
                                                        >
                                                            Agregar Oficio
                                                        </button>
                                                        :
                                                        'PENDIENTE'
                                                    :
                                                    <a href={`${SERVER}${obj.office}`}
                                                       className="btn btn-dsch mx-auto"
                                                       target={"_blank"}
                                                       rel="noopener noreferrer" >
                                                        Ver Oficio
                                                    </a>
                                            }
                                        </td>
                                        <td width={120}>
                                            {
                                                this.onCheckPermissionsCommission()
                                                    &
                                                obj.status === 'Revisión - Oficina Técnica de Consejo'
                                                    ?
                                                    <button
                                                        className="btn btn-warning"
                                                        onClick={
                                                            this.onTerminateProcessExtension.bind(
                                                                this, obj.id)
                                                        }>
                                                        Terminar
                                                    </button>
                                                    :
                                                    null
                                            }
                                        </td>
                                    </tr>
                                ))
                            }
                            </tbody>
                        </table>
                        :
                        <section className="col-10 mx-auto alert alert-danger py-4">
                            <h5>
                                <strong>
                                    SIN REGISTRO DE SOLITUDES DE PRÓRROGA
                                </strong>
                            </h5>
                        </section>
                }
                <ModalOfficeToExtension
                    pk={this.state.pk}
                    add_office_to_extension={this.props.add_office_to_extension}
                />
            </section>
        )
    }

}

const mapStateToProps = (state) => ({
    extensions: state.investigation.extensions
});

export default connect(mapStateToProps, null)(ListExtension);