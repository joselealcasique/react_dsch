import React, { Component } from "react";
import PropTypes from "prop-types";


export class ModalAdvance extends Component{

    static propTypes = {
        add_advance_to_project: PropTypes.func.isRequired,
        onClearToForm: PropTypes.func.isRequired,
        onChangeChecked: PropTypes.func.isRequired,
        onChangeFile: PropTypes.func.isRequired,
        onSubmit: PropTypes.func.isRequired,
        file: PropTypes.any,
        zip: PropTypes.any,
        option: PropTypes.bool,
    }

    render() {
        return (
            <div className="modal fade" id="modal-advance" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered modal-xl" role="document">
                    <div className="modal-content ">
                        <div className="modal-header bg-form-presupuestal">
                            <h5 className="modal-title" id="exampleModalLongTitle">
                                <strong>
                                    Registro del informe de avance o conclusión del Proyecto
                                </strong>
                            </h5>
                            <button type="button" className="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form onSubmit={this.props.onSubmit} id="form-advance"
                              className="modal-body">

                            <div className="my-3 filter-section-dsch-multi-form" >
                                <div className="col-5 mx-auto">
                                    <label className="ml-1">
                                        <strong>Informe de proyecto</strong>
                                    </label>
                                    <input type="file" className="form-control" name="file"
                                           value={this.file} onChange={this.props.onChangeFile}
                                           required />
                                </div>

                                <div className="col-5 mx-auto">
                                    <label className="ml-1">
                                        <strong>Carpeta comprimida</strong>
                                    </label>
                                   <input type="file" className="form-control" name="zip"
                                           value={this.zip}
                                           onChange={this.props.onChangeFile} required />
                                </div>
                                <div className="col-2 mx-auto mt-0">
                                    <label className="ml-1">
                                        <strong>¿Es conclusion?</strong>
                                    </label>
                                   <input type="checkbox" className="form-control" name="option"
                                           value={this.props.option}
                                          onChange={this.props.onChangeChecked}  />
                                </div>
                            </div>

                            <div className="bg-form-presupuestal py-3 m-form-footer">
                                <button type="reset" className="btn btn-dark ml-auto"
                                        data-dismiss="modal" onClick={this.props.onClearToForm}>
                                    Cerrar y limpiar información
                                </button>
                                <button type="submit" className="btn btn-light ml-5">
                                    Guardar Informe
                                </button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        )
    }

}

export default ModalAdvance;