import React, { Component } from "react";
import PropTypes from "prop-types";


export class ModalOfficeToExtension extends Component{

    state = {
        file: null,
    }

    static propTypes = {
        pk: PropTypes.number.isRequired,
        add_office_to_extension: PropTypes.func.isRequired,
    }

    onClearToForm = () => {
        document.getElementById('form-extension-office').reset();
    }

    onChangeFile = (e) => {
        this.setState({file: e.target.files[0]});
    }

    onSubmit = (e) => {
        e.preventDefault();
        alert('Enviar')
        let data = new FormData();
        data.append('office', this.state.file, this.state.file.name);
        this.props.add_office_to_extension(data, this.props.pk);
        this.onClearToForm();
    }

    render() {
        return (
            <div className="modal fade" id="modal-extension-office" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered modal-md" role="document">
                    <div className="modal-content ">
                        <div className="modal-header bg-form-presupuestal">
                            <h5 className="modal-title" id="exampleModalLongTitle">
                                <strong>
                                    Registro de oficio de prórroga, {this.props.pk}
                                </strong>
                            </h5>
                            <button type="button" className="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form onSubmit={this.onSubmit} id="form-extension-office"
                              className="modal-body">

                            <div className="my-3 filter-section-dsch-multi-form" >
                                <div className="col-11 mx-auto">
                                    <label className="ml-1">
                                        <strong>Solicitud de prórroga</strong>
                                    </label>
                                    <input type="file" className="form-control" name="file"
                                           value={this.file} onChange={this.onChangeFile}
                                           required accept="application/pdf"/>
                                </div>
                            </div>

                            <div className="bg-form-presupuestal py-3 m-form-footer">
                                <button type="reset" className="btn btn-dark ml-auto"
                                        data-dismiss="modal" onClick={this.onClearToForm}>
                                    Cerrar y limpiar información
                                </button>
                                <button type="submit" className="btn btn-light ml-5">
                                    Guardar Solicitud
                                </button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        )
    }

}

export default ModalOfficeToExtension;