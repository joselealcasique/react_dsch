import React, { Component } from "react";
import PropTypes from "prop-types";

import FormPresupuestal from "./FormPresupuestal";
import {ConfigDateSave} from "../../../../FormatDate";


export class ModalPresupuestal extends Component{

    state = {
        code: '',
        sub_code: '',
        name: '',
        amount: '',
        date: '',
        assign: 'Directa de Jefe de Departamento',
        file: null,
        observations: '',
    }

    static propTypes = {
        add_presupuestal: PropTypes.func.isRequired,
        pk: PropTypes.number,
        obj: PropTypes.object,
    }

    onLength = (string) => {
        return string.length
    }

    configAmount = (value) => {
        let new_value = value.replace(/,/g, '');
        return new Intl.NumberFormat('es-MX').format(new_value);
    }

    onChange = (e) => {
        this.setState({[e.target.name]: e.target.value});

        if (e.target.name === 'amount'){
            let len_amount = this.onLength(e.target.value)

            if (len_amount > 3) this.setState({
                amount: this.configAmount(e.target.value)
            });
        }
    }
    onChangeFile = (e) => this.setState({file: e.target.files[0]});

    onClearToForm = () => {
        document.getElementById('form-presupuestal').reset();
        this.setState({
            code: '',
            sub_code: '',
            name: '',
            amount: '',
            date: '',
            assign: 'Directa de Jefe de Departamento',
            file: null,
            observations: '',
        });
    }

    onSubmit = (e) => {
        e.preventDefault();
        let data = new FormData()
        data.append('code', this.state.code)
        data.append('sub_code', this.state.sub_code)
        data.append('name', this.state.name)
        data.append('amount', this.state.amount)
        data.append('assign', this.state.assign)
        data.append('file', this.state.file, this.state.file.name)
        data.append('date', ConfigDateSave(this.state.date))
        data.append('observations', this.state.observations)
        this.props.add_presupuestal(this.props.pk, data);
        this.onClearToForm();
    }


    render() {
        return (
            <div className="modal fade" id="modal-Edit-Presupuestal" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <div className="modal-content ">
                        <div className="modal-header bg-form-presupuestal">
                            <h5 className="modal-title" id="exampleModalLongTitle">
                                <strong>Asignación Presupuestal</strong>
                            </h5>
                            <button type="button" className="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <FormPresupuestal
                            onChange={this.onChange}
                            onSubmit={this.onSubmit}
                            onChangeFile={this.onChangeFile}
                            pk={this.props.pk}
                            obj={null}
                            name={this.state.name}
                            date={this.state.date}
                            code={this.state.code}
                            sub_code={this.state.sub_code}
                            assign={this.state.assign}
                            amount={this.state.amount}
                            file={this.state.file}
                            observations={this.state.observations}
                        />

                    </div>
                </div>
            </div>
        )
    }

}

export default ModalPresupuestal;