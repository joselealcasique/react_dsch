import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";

import Information from "./components/Information";
import Presupuestal from "./components/Presupuestal";
import CommissionInformation from "./components/CommissionInformation";
import Advance from "./components/Advance";
import Extensions from "./components/Extensions";
import NotView from "./components/NotView";


export class DataInformation extends Component {

    state = {
        view: 1,
        template: <Information
            obj={this.props.obj}
            onRedirectFromList={this.props.onRedirectFromList}
        />
    }

    static propTypes = {
        pk: PropTypes.number,
        obj: PropTypes.object,
        letters: PropTypes.array,
        presupuestal: PropTypes.array,
        permissions: PropTypes.array,
        onRedirectFromList: PropTypes.func.isRequired,
    }

    onChange = (e) => {
        this.setState({[e.target.name]: e.target.value});

        switch (parseInt(e.target.value)){

            case 1:
                this.setState({
                    template: <Information
                        obj={this.props.obj}
                        onRedirectFromList={this.props.onRedirectFromList}
                    />
                });
                break;

            case 2:
                this.setState({
                    template: <Presupuestal
                        presupuestal={this.props.presupuestal}
                        pk={this.props.pk}
                        permissions={this.props.permissions}

                    />
                });
                break;

            case 3:
                this.setState({
                    template: <CommissionInformation
                        obj={this.props.obj}
                        onRedirectFromList={this.props.onRedirectFromList}
                        permissions={this.props.permissions}
                    />
                });
                break;

            case 4:
                this.setState({
                    template: <Advance
                        obj={this.props.obj}
                        onRedirectFromList={this.props.onRedirectFromList}
                    />
                });
                break;

            case 5:
                this.setState({
                    template: <Extensions obj={this.props.obj} />
                });
                break;

            default:
                this.setState({ template: <NotView /> });
        }
    }


    render() {
        return (
            <Fragment>
                <section className="col-3 mt-2 m-0 p-0">
                    <section className="card px-3 pt-3 pb-4 alert-warning">
                        <div>
                            <h6><strong>+ Estado de Proyecto</strong></h6>
                            <h6 className="ml-5">Proyecto {this.props.obj.status}</h6>
                        </div>
                        <hr></hr>
                        <div className="text-center">
                            <h6>
                                <strong>
                                    Opciones a visualizar del proyecto
                                </strong>
                            </h6>
                            <select className="form-control" name="view"
                                    onChange={this.onChange} value={this.state.view}>
                                <option key={1} value={1}>
                                    Información general del proyecto
                                </option>

                                {
                                    this.props.obj.status === 'Aceptado'
                                    ||
                                    this.props.obj.status === 'Espera de Aceptación'
                                    ||
                                    this.props.obj.status === 'Finalizado'
                                        ?
                                        <option key={3} value={3}>
                                            Información de aceptación
                                        </option>
                                        :
                                        null

                                }

                                {
                                    this.props.obj.status === 'Aceptado'
                                        |
                                    this.props.obj.status === 'Finalizado'
                                        ?
                                        <option key={2} value={2}>Asignación presupuestal</option>
                                        :
                                        null

                                }
                                {
                                    this.props.obj.status === 'Aceptado'
                                        |
                                    this.props.obj.status === 'Finalizado'
                                        ?
                                        <option key={4} value={4}>
                                            Registro de informes de proyecto
                                        </option>
                                        :
                                        null

                                }

                                {
                                    this.props.obj.status === 'Aceptado'
                                        |
                                    this.props.obj.status === 'Finalizado'
                                        ?
                                        <option key={5} value={5}>
                                            Solicitud de Prórrogas
                                        </option>
                                        :
                                        null

                                }
                            </select>
                        </div>
                    </section>
                </section>
                <section className="col-9 mt-2">
                    { this.state.template }
                </section>
            </Fragment>
        );
    }
}

export default DataInformation;