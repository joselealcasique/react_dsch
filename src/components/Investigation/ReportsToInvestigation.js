import React, { Component, Fragment } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import CardReport from "./reports/CardReport";
import { get_report_presupuestal } from "../../actions/investigation/get_report_presupuestal";
import {
    get_report_project_investigation
} from "../../actions/investigation/get_report_projects_investigation";


export class ReportsToInvestigation extends Component {

    static propTypes = {
        permissions: PropTypes.array.isRequired,
        user: PropTypes.object.isRequired,
        get_report_presupuestal: PropTypes.func.isRequired,
        get_report_project_investigation: PropTypes.func.isRequired,
    }

    onCheckPermissionsCommission = () => {
        if (this.props.permissions.includes('investigation.admin_evaluation_commission')){
            return true
        }
        return false;
    }

    onCheckPermissionsLeader = () => {
        if (this.props.permissions.includes('investigation.admin_leader_departament')){
            return true
        }
        return false;
    }

    render() {

        return (
            <Fragment>
                <section className="alert alert-warning col-12 mt-3 text-center py-3">
                    <h4>
                        <strong>
                            Reportes de Investigación y Presupuestal
                        </strong>
                    </h4>
                </section>
                <section className="filter-section-dsch-multi-form">
                    {
                        this.onCheckPermissionsLeader() | this.onCheckPermissionsCommission()
                            ?
                            <CardReport
                                title={
                                    this.onCheckPermissionsCommission()
                                        ?
                                        'Reporte presupuestal de los departamentos'
                                        :
                                        'Reporte presupuestal por departamento'
                                }
                                action={this.props.get_report_presupuestal}
                                number={this.props.user.user}
                                presupuestal={true}
                            />
                            :
                            null
                    }

                    {
                        this.onCheckPermissionsCommission()
                            ?
                            <CardReport
                                title={'Reporte de proyecto aceptados y finalizados'}
                                action={this.props.get_report_project_investigation}
                                number={this.props.user.user}
                                presupuestal={false}
                            />
                            :
                            null
                    }
                </section>
            </Fragment>
        )
    }

}

const mapStateToProps = (state) => ({
    permissions: state.auth.permissions,
    user: state.auth.user,
});

export default connect(
    mapStateToProps, { get_report_presupuestal, get_report_project_investigation }
)(ReportsToInvestigation);