import React from "react";
import Investigation from "./Investigation";


export class ProjectRequest extends React.Component {

    state = {
        title_page: 'Solicitudes de Proyectos de Investigación',
        option: 1

    }

    render() {
        return <Investigation
            title_page={this.state.title_page} option_request={this.state.option}
        />
    }

}

export default ProjectRequest;