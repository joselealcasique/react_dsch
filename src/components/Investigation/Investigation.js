import React, { Component } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import Year from "../academic/components/Year";
import ListProjectInvestigation from "../generals/investigation/ListProjectInvestigation";
import {
    list_project_investigation_departament
} from "../../actions/investigation/list_project_investigation_departament";
import {
    list_project_investigation_commission
} from "../../actions/investigation/list_project_investigation_commission";


export class Investigation extends Component {

    state = {
        year: new Date().getFullYear(),
        departament: 'Humanidades'
    }

    static propTypes = {
        title_page: PropTypes.string.isRequired,
        option_request: PropTypes.number.isRequired,
        user: PropTypes.object.isRequired,
        investigation: PropTypes.object.isRequired,
        list_project_investigation_departament: PropTypes.func.isRequired,
        list_project_investigation_commission: PropTypes.func.isRequired,
    }

    componentDidMount() {
        if (this.props.user.departament === 'view_evaluation_commission'){
            this.props.list_project_investigation_commission(
                this.state.year, this.props.option_request);

        }else {
            let departament = '';

            if (this.props.user.departament === 'view_academic_humanities'){
                departament = 'Humanidades'

            } else if (this.props.user.departament === 'view_academic_social_sciences'){
                departament = 'Ciencias Sociales'

            } else if (this.props.user.departament === 'view_academic_institutional_studies'){
                departament = 'Estudios Institucionales'
            }
            this.props.list_project_investigation_departament(
                departament, this.state.year, this.props.option_request);
            this.setState({departament: departament});
        }
    }

    onChange = (e) => {
        this.setState({[e.target.name]: e.target.value});

        if (this.props.user.departament === 'view_evaluation_commission') {
            this.props.list_project_investigation_commission(
                e.target.value, this.props.option_request);

        }else {
            this.props.list_project_investigation_departament(
                this.state.departament, e.target.value, this.props.option_request);
        }
    }

    render () {
        return (
            <section className="col-12 mx-auto">
                <section className="filter-section-dsch-multi-form mt-2">
                    <div className="col-6 mx-auto mt-3 text-center">
                        <h4>
                            <strong>
                                { this.props.title_page }
                            </strong>
                        </h4>
                    </div>
                    <section className="alert-warning col-5 py-2 filter-section-dsch-multi-form mt-2">
                        <div className="col-9 pt-1">
                            <h6>
                                <strong>
                                    Seleccionar año para listar proyectos en evaluación
                                </strong>
                            </h6>
                        </div>
                        <Year
                            select_year={this.state.year.toString()}
                            onChange={this.onChange}
                        />
                    </section>
                </section>

                <ListProjectInvestigation option={2} />
            </section>
        )
    }

}

const mapStateToProps = (state) => ({
    investigation: state.investigation,
    user: state.auth.user
});

export default connect(
    mapStateToProps,
    { list_project_investigation_departament, list_project_investigation_commission }
)(Investigation);

