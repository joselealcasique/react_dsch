import React, { Component } from "react";
import {Link} from "react-router-dom";
import PropTypes from "prop-types";


export class ViewOptionsProcess extends Component {

    state = {
        file: null
    }

    static propTypes = {
        pk: PropTypes.number,
        user: PropTypes.object,
        obj: PropTypes.object,
        permissions: PropTypes.array,
        onProjectEditable: PropTypes.func.isRequired,
        sendProjectToCommission: PropTypes.func.isRequired,
        onRedirectListProjects: PropTypes.func.isRequired,
        finalized_process_project: PropTypes.func.isRequired,
    }

    componentDidMount() {
        this.CheckUserAuthor();

    }

    CheckUserAuthor = () => {
        let option = this.props.obj.author.find(user => user.user === this.props.user.user);
        if (option === undefined){
             return true;

        } else {
            return false;
        }
    }

    onChangeFile = (e) => this.setState({file: e.target.files[0]});

    onFinalizedProject = (option) => {
        let data = new FormData();

        try {
            data.append('file', this.state.file, this.state.file.name);
        } catch (e){

            if (e.TypeError === undefined){
                alert('No se adjuntó archivo. Debe de adjuntar un oficio para justificar la decisión')
                return false;

            }
        }
        data.append('finalized', option)
        this.props.finalized_process_project(data, this.props.pk);
        setTimeout(this.props.onRedirectListProjects, 600)
    }

    render() {
        const OptionEdit = (
            <div
                className="mx-auto text-center alert alert-primary">
                <p className="text-justify">
                    El proyecto requiere cambios, al concluir
                    los cambios, la solicitud debe ser reenviada.
                </p>
                <Link to={{
                    pathname: '/pre-project',
                    state: {
                        pk: this.props.pk,
                        edit: true,
                        obj: this.props.obj,
                        is_sketch: this.props.obj.is_sketch
                    }
                }}>
                    <button className="btn btn-primary py-2">
                        <strong>Editar Proyecto</strong>
                    </button>
                </Link>
            </div>
        );

        const ActivateEdit = (
            <div className="mx-auto  text-center alert alert-warning">
                <p className="text-justify">
                    Esta opción sirve para que el encargado del
                    proyecto pueda realizar cambios en la estructura.
                </p>
                {
                    !this.props.obj.is_editable
                        ?
                        <button className="btn btn-warning py-2"
                                onClick={this.props.onProjectEditable}>
                            <strong>Habilitar edición</strong>
                        </button>
                        :
                        <p>
                            <strong>
                                Opción actualmente habilitada
                            </strong>
                        </p>
                }
            </div>
        )

        const SendCommission = (
            <div className="mx-auto mt-1 text-center alert alert-danger">
                <p className="text-justify">
                    Esta opción sirve para enviar el proyecto y
                    las cartas respectivas a la comisión evaluadora.
                </p>
                <button className="btn btn-danger py-2"
                        onClick={this.props.sendProjectToCommission}>
                    <strong>Enviar</strong>
                </button>
            </div>
        )

        const SendCommissionInactive = (
            <div className="mx-auto mt-1 text-center alert alert-danger">
                <p className="text-justify">
                    Esta opción se habilitará cuando el
                    profesor responsable del proyecto reenvié la solicitud.
                </p>
            </div>
        )

        const SendCommissionStop = (
            <div className="mx-auto mt-1 text-center alert alert-danger">
                <p className="text-justify">
                    El proyecto esta en revisión y en espera de una decisión por parte de la
                    <strong> Comisión de evaluación. </strong>
                </p>
            </div>
        )

        const SendLeaderStop = (
            <div className="mx-auto mt-1 text-center alert alert-danger">
                <p className="text-justify">
                    El proyecto esta en revisión y en espera de una decisión por parte del
                    <strong> Jefe de Departamento. </strong>
                </p>
            </div>
        )

        const FinalizedProcessProject = (
            <div className="mx-auto mt-1 text-center card py-2">
                <p className="text-center col-12">
                    <strong>¿El proyecto fue aceptado?</strong>
                    <section className=" alert alert-dark py-3 mt-3" onSubmit={this.onSubmit}>
                        <div>
                            <label><strong>Agregar Oficio</strong></label>
                            <input type="file" className="form-control mx-auto" name="file"
                                   value={this.file} onChange={this.onChangeFile}
                                   accept={"application/pdf"}
                            />
                        </div>
                        <div className="filter-section-dsch-multi-form pt-3">
                            <button className="btn btn-primary col-5 mx-auto"
                                    onClick={this.onFinalizedProject.bind(this, true)}>
                                Sí
                            </button>
                            <button className="btn btn-danger col-5 mx-auto"
                                    onClick={this.onFinalizedProject.bind(this, false)}>
                                No
                            </button>
                        </div>
                    </section>
                </p>
            </div>
        )

        return (
            <section className="col-3 ml-4 card mt-2 py-3">
                {
                    /*
                    *
                    * En esta sección se definirá que usuario
                    * pueden habilitar la edición del proyecto
                    *
                    */

                    this.props.permissions.includes('investigation.admin_leader_departament')
                        ?
                        !this.props.obj.revision
                            ?
                            this.props.obj.is_editable
                                ?
                                null
                                :
                                ActivateEdit
                            :
                            null
                        :
                    this.props.permissions.includes('investigation.admin_evaluation_commission')
                        ?
                        this.props.obj.revision
                            ?
                            this.props.obj.is_editable
                                ?
                                null
                                :
                                ActivateEdit
                            :
                            null
                        :
                        null
                }

                {
                    !this.CheckUserAuthor()
                        ?
                        this.props.obj.is_editable
                            ?
                            OptionEdit
                            :
                            null
                        :
                        null
                }

                {
                    this.props.permissions.includes('investigation.admin_leader_departament')
                        ?
                        !this.props.obj.revision
                            ?
                            !this.props.obj.is_editable
                                ?
                                SendCommission
                                :
                                SendCommissionInactive
                            :
                            SendCommissionStop
                        :
                    this.props.permissions.includes('investigation.admin_evaluation_commission')
                        ?
                        !this.props.obj.revision
                            ?
                            !this.props.obj.is_editable
                                ?
                                SendLeaderStop
                                :
                                null
                            :
                            FinalizedProcessProject
                        :
                        this.props.obj.is_editable
                            ?
                            null
                            :
                            !this.props.obj.revision
                                ?
                                SendLeaderStop
                                :
                                SendCommissionStop

                }
            </section>
        )
    }
}

export default ViewOptionsProcess;