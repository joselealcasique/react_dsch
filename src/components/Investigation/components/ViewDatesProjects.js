import React, { Component } from "react";
import {FormatDate} from "../../FormatDate";
import {SERVER} from "../../../actions/server";
import PropTypes from "prop-types";


export class ViewDatesProjects extends Component {

    static propTypes = {
        pk: PropTypes.number,
        obj: PropTypes.object,
    }

    render() {
        return (
            <section className="col-3 ml-4 card py-3 mt-2">
                <div className="mb-2">
                    <h6><strong>+ Fecha de envío de Pre-Solicitud</strong></h6>
                    <h6 className="ml-5">{FormatDate(this.props.obj.send)}</h6>
                </div>
                <div>
                    <h6><strong>+ Estado de Proyecto</strong></h6>
                    <h6 className="ml-5">{this.props.obj.status}</h6>
                </div>
                <hr></hr>
                <div className="mb-2">
                    <h6><strong>+ Fecha considerada para iniciar</strong></h6>
                    <h6 className="ml-5">{FormatDate(this.props.obj.initial)}</h6>
                </div>
                <div className="mb-2">
                    <h6><strong>+ Fecha considerada para finalizar</strong></h6>
                    <h6 className="ml-5">{FormatDate(this.props.obj.fin)}</h6>
                </div>
                <div className="mb-2">
                    <h6><strong>+ Duración de proyecto</strong></h6>
                    <h6 className="ml-5">{this.props.obj.duration} años</h6>
                </div>
                <div className="text-right">
                    <a className="btn btn-dark px-4"
                       href={
                           this.props.pk !== 0
                               ?
                               `${SERVER}/investigation/pdf/${this.props.pk}/1/`
                               :
                               "#"
                       } target={this.props.pk !== 0 ? "_blank" : ''} rel="noopener noreferrer">
                        <h5>
                            <strong>Ver Archivo</strong>
                        </h5>
                    </a>
                </div>
            </section>
        );
    }
}

export default ViewDatesProjects;