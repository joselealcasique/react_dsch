import React, { Component } from 'react';
import {connect} from 'react-redux';
import PropTypes from "prop-types";

import ModalAddUser from "./components/ModalAddUser";
import {FormatDate, ConfigDateSave} from "../FormatDate";
import {create_request_editorial} from "../../actions/editorial/create_request_editorial"


export class Editorial extends Component {

    state = {
        title: '',
        author: '',
        date_created: ''
    }

    static propTypes = {
        create_request_editorial: PropTypes.func.isRequired
    };

    onChange = (e) => this.setState({[e.target.name]: e.target.value});

    onSubmit = (e) => {
        e.preventDefault();
        this.props.create_request_editorial(
            this.state.title, this.state.author, ConfigDateSave(this.state.date_created));
        this.setState({
            title: '',
            author: '',
            date_created: ''
        });
    }

    render() {
        return (
            <section className="col-11 m-auto text-center pt-4 filter-section-dsch-multi-form">
                <form className="card card-body col-6 m-auto" onSubmit={this.onSubmit}>
                    <h4 className=""><strong>Registro de Solicitudes A Editorial</strong> </h4>
                    <hr></hr>

                    <div className="form-group col-8 m-auto pt-3">
                        <h5>Título de Obra</h5>
                        <input type="text" name="title" className="form-control text-center" required
                               value={this.state.title} onChange={this.onChange} maxLength={140}/>
                    </div>

                    <div className="form-group col-8 m-auto pt-4">
                        <h5>Autor de Obra (No. Económico)</h5>
                        <input type="text" name="author" className="form-control text-center" required
                               value={this.state.author} onChange={this.onChange} maxLength={20}/>
                    </div>

                    <div className="form-group col-8 m-auto pt-3">
                        <h5>Fecha de Entrega (Manuscrito)</h5>
                        <input type="date" name="date_created" className="form-control text-center"
                               required value={this.state.date_created} onChange={this.onChange}/>
                    </div>

                    <div className="form-group col-6 m-auto pt-5">
                        <button className="btn-dsch">Guardar</button>
                    </div>
                </form>

                <section className="col-1 m-auto">

                </section>

                <section className="card card-body col-5 m-auto alert-dark">
                    <div className="col-12 mb-4">
                        <button className="btn btn-dark float-right" data-toggle="modal"
                                data-target="#modalAddUser">
                            Agregar Usuario
                        </button>
                    </div>
                    <h4 className=""><strong>Previzualización De Solicitud</strong> </h4>
                    <hr></hr>
                    <div className="form-group col-12 mt-1">
                        <label><strong>Título De Obra</strong> </label>
                        <div className="card card-body ">
                            <label>{this.state.title}</label>
                        </div>
                    </div>

                    <div className="form-group col-12 mt-1">
                        <label><strong>Autor De Obra</strong></label>
                        <div className="card card-body ">
                            <label>{this.state.author}</label>
                        </div>
                    </div>

                    <div className="form-group col-12 mt-1">
                        <label><strong>Fecha de Manuscrito</strong></label>
                        <div className="card card-body ">
                            <label>{this.state.date_created !== '' ?
                                FormatDate(ConfigDateSave(this.state.date_created)) :''}</label>
                        </div>
                    </div>
                </section>
                <ModalAddUser />

            </section>
        )
    }
}


const mapStateToProps = (state) => ({

});

export default connect(mapStateToProps, {create_request_editorial})(Editorial);