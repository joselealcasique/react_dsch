import React, { Component } from 'react';
import {connect} from 'react-redux';
import PropTypes from "prop-types";

import { add_user_external } from '../../../actions/editorial/add_user_external';

class ModalAddUser extends Component {

    state = {
        name: '',
        paternal_surname: '',
        maternal_surname: '',
        mail_personal: '',
    }

    static propTypes = {
        add_user_external: PropTypes.func.isRequired,
    };

    onChange = (e) => this.setState({[e.target.name]: e.target.value});

    onSubmit = (e) => {
        e.preventDefault();
        this.props.add_user_external(
            this.state.name,
            this.state.paternal_surname,
            this.state.maternal_surname,
            this.state.mail_personal
        );
        this.setState({
            name: '',
            paternal_surname: '',
            maternal_surname: '',
            mail_personal: ''
        })
    }

    render() {
        return (
            <div className="modal fade" id="modalAddUser" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-lg" role="document">
                    <div className="modal-content ">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">
                                Registrar Nuevo Usuario
                            </h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <form onSubmit={this.onSubmit}>
                                <div className="filter-section-dsch-multi-form">
                                    <h6 className="pt-3 col-3 text-center">
                                        <strong>Nombre de Usuario: </strong>
                                    </h6>
                                    <input type="text" className="form-control" name="name" required
                                           value={this.state.name} onChange={this.onChange} />
                                </div>

                                <div className="filter-section-dsch-multi-form">
                                    <h6 className="pt-3 col-3 text-center">
                                        <strong>Apellido Paterno: </strong>
                                    </h6>
                                    <input type="text" className="form-control"name="paternal_surname" required
                                           value={this.state.paternal_surname} onChange={this.onChange}  />
                                </div>

                                <div className="filter-section-dsch-multi-form">
                                    <h6 className="pt-3 col-3 text-center">
                                        <strong>Apellido Materno: </strong>
                                    </h6>
                                    <input type="text" className="form-control"name="maternal_surname" required
                                           value={this.state.maternal_surname} onChange={this.onChange}  />
                                </div>

                                <div className="filter-section-dsch-multi-form">
                                    <h6 className="pt-3 col-3 text-center">
                                        <strong>Correo Personal: </strong>
                                    </h6>
                                    <input type="email" className="form-control"name="mail_personal" required
                                           value={this.state.mail_personal} onChange={this.onChange}  />
                                </div>
                                <hr></hr>
                                <div className="col-12 mt-2">
                                    <button className="btn btn-primary float-right">Guardar Usuario</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, { add_user_external })(ModalAddUser);