import React, {Component} from 'react';
import PropTypes from "prop-types";
import {connect} from "react-redux";

import {FormatDate, ConfigDateSave} from "../../../FormatDate";
import {update_date} from '../../../../actions/editorial/update_date';


export class IsbnDate extends Component {

    state = {
        isbn_date: new Date()
    }

    static propTypes = {
        isbn_date: PropTypes.string,
        finalized: PropTypes.bool.isRequired,
        pk: PropTypes.number,
        update_date: PropTypes.func.isRequired
    };

    onChange = (e) => this.setState({[e.target.name]: e.target.value});

    onSubmit = (e) => {
        const data = JSON.stringify({
            "isbn_date": ConfigDateSave(this.state.isbn_date)})
        this.props.update_date(data, this.props.pk);
    }

    render() {

        if (this.props.isbn_date == null){
            if (this.props.finalized === true){
                return (
                    <div className="alert alert-info p-2 text-center">
                        <h5 className="pt-1">
                            NO HAY REGISTRO ALMACENADO - SOLICITUD DE ISBN
                        </h5>
                    </div>
                )
            }else {
                return (
                    <div className="filter-section-dsch-multi-form alert alert-warning p-1">
                        <h5 className="mt-3 col-5">Solicitud de ISBN: </h5>
                        <input type="date" className="form-control col-4" name="isbn_date"
                               onChange={this.onChange} value={this.state.isbn_date}
                        />
                        <section className="col-1"></section>
                        <button className="btn btn-warning" onClick={this.onSubmit}>Guardar</button>
                    </div>
                );
            }
        } else {
           return (
               <div className="alert alert-success p-2 filter-section-dsch-multi-form">
                    <h5 className="col-5 pt-1">Solicitud de ISBN:  </h5>
                    <h5 className="pt-1">
                        <strong>{FormatDate(this.props.isbn_date)}</strong>
                    </h5>
                </div>
            );
        }
    }
}

const mapStateToProps = (state) => ({});


export default connect(mapStateToProps, {update_date})(IsbnDate)