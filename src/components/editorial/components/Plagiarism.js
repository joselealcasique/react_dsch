import React, { Component } from 'react';
import {connect} from 'react-redux';
import PropTypes from "prop-types";

import {add_plagiarism } from '../../../actions/editorial/add_plagiarism';
import {SERVER} from "../../../actions/server";


export class Plagiarism extends Component {

    state = {
        percentage: 0,
        observations: '',
        file: null
    }

    static propTypes = {
        plagiarism: PropTypes.object,
        title: PropTypes.string.isRequired,
        pk: PropTypes.number.isRequired,
        add_plagiarism: PropTypes.func.isRequired
    };

    onChange = (e) => this.setState({[e.target.name]: e.target.value});

    onloadFile = (e) => {
        this.setState({file: e.target.files[0]});
        const reader = new FileReader();
        reader.onload = () => {
            if (reader.readyState === 2) this.setState({viewFile: reader.result});
        }
        reader.readAsDataURL(e.target.files[0]);
    }

    onSubmit = (e) =>{
        e.preventDefault();
        if (this.state.percentage < 0){
            alert('No se aceptan valores Negativos');
            return false;
        } else{
            const data = new FormData();
            data.append('percentage', this.state.percentage);
            data.append('title', this.props.title);
            data.append('observations', this.state.observations,);
            data.append('file', this.state.file, this.state.file.name);
            this.props.add_plagiarism(data, this.props.pk);
        }
    }

    render() {

        if (this.props.plagiarism !== null){
            return (
                <section className="card text-left">
                    <div className="dsch-det-card alert-dark text-left">
                        <label>
                            <strong>Procentaje De Plagio Encontrado </strong>
                        </label>
                    </div>
                    <h6 className="pl-3">{this.props.plagiarism.percentage}% encontrado</h6>

                    <div className="dsch-det-card alert-dark text-left">
                        <label>
                            <strong>Observaciones</strong>
                        </label>
                    </div>
                    <h6 className="pl-3 pr-3">{this.props.plagiarism.observations}</h6>

                    <div className="dsch-det-card alert-dark text-left">
                        <label>
                            <strong>Archivo De Evidencias de Plagio</strong>
                        </label>
                    </div>
                    <h6 className="pl-3 pr-3">
                        <a target="_blank" rel="noopener noreferrer" className="float-right pr-2"
                            href={`${SERVER}${this.props.plagiarism.file}`}>
                             Ver Archivo
                         </a>
                    </h6>
                </section>
            );
        } else {
            return (
                <form className="card" onSubmit={this.onSubmit}>
                    <div className="form-group col-12 m-auto pt-3">
                        <h5>Procentaje de Plagio</h5>
                        <input type="number" name="percentage" className="form-control" required
                               value={this.state.percentage} onChange={this.onChange}/>
                    </div>
                    <div className="form-group col-12 m-auto pt-3">
                        <h5>Archivo de Evidencia</h5>
                        <input type="file" name="file" className="form-control" required
                               value={this.file} onChange={this.onloadFile} accept="application/pdf"/>
                    </div>
                    <div className="form-group col-12 m-auto pt-3">
                        <h5>Observaciones</h5>
                        <textarea name="observations" className="form-control" required rows="2"
                                  value={this.state.observations} onChange={this.onChange}>
                        </textarea>
                    </div>
                    <div className="form-group col-12 m-auto pt-3 pb-3">
                        <button className="btn-dsch">Guardar</button>
                    </div>
                </form>
            );
        }


    }
}

const mapStateToProps = (state) => ({

});


export default connect(mapStateToProps, {add_plagiarism})(Plagiarism);