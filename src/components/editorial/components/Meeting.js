import React, { Component } from 'react';
import {connect} from 'react-redux';
import PropTypes from "prop-types";

import {FormatDate, ConfigDateSave} from "../../FormatDate";
import {add_meeting} from '../../../actions/editorial/add_meeting';
import {update_meeting} from '../../../actions/editorial/update_meeting';
import {change_status} from '../../../actions/editorial/change_status';


export class Meeting extends Component {

    state = {
        date: '',
        status: 0,
        observations: ''
    }

    static propTypes = {
        meeting: PropTypes.object,
        pk: PropTypes.number.isRequired,
        add_meeting: PropTypes.func.isRequired,
        update_meeting: PropTypes.func.isRequired,
        change_status: PropTypes.func.isRequired,
        is_active: PropTypes.bool.isRequired
    };

    onChange = (e) => this.setState({[e.target.name]: e.target.value});

    onSubmit = (e) => {
        e.preventDefault();
        this.props.add_meeting(ConfigDateSave(this.state.date), this.state.status, this.props.pk)
    }

    onUpdateMeeting = (e) =>{
        if (this.state.observations.length <= 0){
            alert('AGREGUE IFNORMACIÓN EN OBSERVACIONES');
            this.setState({observations: ''});
            return;
        }
        this.props.update_meeting(this.state.observations, this.props.pk);
        this.setState({observations: ''});
    }

    onChangeStatus = (e) => {
        let new_status = 0;

        if (e.target.name === 'pend') {
            new_status = 0;

        } else if (e.target.name === 'dic') {
            new_status = 1;

        } else if (e.target.name === 'pred') {
            new_status = 2;

        } else if (e.target.name === 'rec') {
            new_status = 3;
        } else {
            alert('OPCIÓN DESCONOCIDA');
            return;
        }

        if (new_status == this.props.meeting.status){
            alert('CAMBIO NO REALIZADO, EL ESTADO ENVIADO ES IGUAL AL ACTUAL');
            return;
        }

        this.props.change_status(new_status, this.props.pk);
    }

    render() {

        if (this.props.meeting !== null) {

            let status_temp = null;
            let modal = null
            let btn_status_edit = (
                <div className="dropdown dropleft float-right mr-3">
                    {
                        this.props.is_active === false
                        ?
                            <button className="btn btn-warning dropdown-toggle" type="button"
                                    id="dropdownMenuButton" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                Cambiar
                            </button>
                            :
                            null
                    }
                    <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a className="dropdown-item" href="#" name="pend" onClick={this.onChangeStatus}>
                            PENDIENTE
                        </a>
                        <a className="dropdown-item" href="#" name="dic" onClick={this.onChangeStatus}>
                            DICTAMEN
                        </a>
                        <a className="dropdown-item" href="#" name="pred" onClick={this.onChangeStatus}>
                            PREDICTAMEN
                        </a>
                        <a className="dropdown-item" href="#" name="rec" onClick={this.onChangeStatus}>
                            RECHAZADO
                        </a>
                    </div>
                </div>
            )

            if (this.props.meeting.status == 0) status_temp='PENDIENTE';
            if (this.props.meeting.status == 1) status_temp='DICTAMEN';
            if (this.props.meeting.status == 2) status_temp='PREDICTAMEN';
            if (this.props.meeting.status == 3) status_temp='RECHAZADO';



            if (this.props.meeting.finalized === false){
                modal = (
                    <div className="form-group col-12 m-auto pt-1">
                        <h5>Observaciones</h5>
                        <textarea name="observations" className="form-control" required rows="2"
                              value={this.state.observations} onChange={this.onChange}>
                        </textarea>
                    </div>
                )
            }

            if (this.props.meeting.finalized !== true){
                return  (
                    <section className="card text-left pb-2">
                        <div className="dsch-det-card alert-dark text-left">
                            <label>
                                <strong>Fecha de Reunión de Consejo Editorial </strong>
                            </label>
                        </div>
                        <h5 className="pl-3">{FormatDate(this.props.meeting.date)}</h5>

                        <div className="dsch-det-card alert-dark text-left">
                            <label>
                                <strong>Estado Actual</strong>
                            </label>
                        </div>
                        <h5 className="pl-3">
                            {status_temp}
                            {
                                this.props.meeting.status == 3
                                    ?
                                    null
                                    :
                                    btn_status_edit
                            }
                        </h5>

                        <div className="dsch-det-card alert-dark text-left">
                            <label>
                                <strong>Acciones</strong>
                            </label>
                        </div>
                        {modal}
                        <div className="pt-2 m-auto">
                            <button className="btn-dsch m-auto" onClick={this.onUpdateMeeting}>
                                Entrega de Archivos
                            </button>
                        </div>
                    </section>
                );

            } else{

                return (
                    <section className="card text-left pb-2">
                        <div className="dsch-det-card alert-dark text-left">
                            <label>
                                <strong>Fecha de Reunión de Consejo Editorial </strong>
                            </label>
                        </div>
                        <h6 className="pl-3">{FormatDate(this.props.meeting.date)}</h6>

                        <div className="dsch-det-card alert-dark text-left">
                            <label>
                                <strong>Estado Actual</strong>
                            </label>
                        </div>
                        <h6 className="pl-3">
                            {status_temp}{this.props.meeting.status == 3 ? null: btn_status_edit}
                        </h6>

                        <div className="dsch-det-card alert-dark text-left">
                            <label>
                                <strong>Observaciones</strong>
                            </label>
                        </div>
                        <h6 className="pl-3">{this.props.meeting.observations}</h6>

                        <div className="dsch-det-card alert-dark text-left">
                            <label>
                                <strong>Fecha de Entrega de Archivos</strong>
                            </label>
                        </div>
                        <h6 className="pl-3">{FormatDate(this.props.meeting.date_finalized)}</h6>
                    </section>
                );
            }
        } else{
            return (
                <form className="card" onSubmit={this.onSubmit}>
                    <div className="form-group col-12 m-auto pt-3">
                        <h5>Fecha de Reunión de Consejo</h5>
                        <input type="date" name="date" className="form-control" required
                               value={this.state.date} onChange={this.onChange}/>
                    </div>
                    <div className="form-group col-12 m-auto pt-3">
                        <h5>Estado Actual del Proceso</h5>
                        <select className="form-control col-11 m-auto" name="status" required
                                value={parseInt(this.state.status)} onChange={this.onChange}>
                            <option key={0} value={0}>PENDIENTE</option>
                            <option key={1} value={1}>DICTAMEN</option>
                            <option key={2} value={2}>PREDICTAMEN</option>
                            <option key={3} value={3}>RECHAZADO</option>
                        </select>
                    </div>
                    <div className="form-group col-12 m-auto pt-3 pb-3">
                        <button className="btn-dsch">Guardar</button>
                    </div>
                </form>
            )
        }
    }
}

const mapStateToProps = (state) => ({

});


export default connect(
    mapStateToProps, {add_meeting, update_meeting, change_status})(Meeting);