import React, { Component } from 'react';
import {connect} from 'react-redux';
import PropTypes from "prop-types";

import {SERVER} from "../../../actions/server";
import {add_evaluation_one} from '../../../actions/editorial/addevaluationone';
import {add_evaluation_two} from '../../../actions/editorial/addevaluationtwo';
import {add_evaluation_result} from '../../../actions/editorial/addevaluationresult'


export class Dictum extends Component {

    state = {
        evaluationOne: null,
        evaluationTwo: null,
        result: 1
    }

    static propTypes = {
        dictum: PropTypes.object,
        pk: PropTypes.number.isRequired,
        add_evaluation_one: PropTypes.func.isRequired,
        add_evaluation_two: PropTypes.func.isRequired,
        add_evaluation_result: PropTypes.func.isRequired
    };

    onChange = (e) => this.setState({[e.target.name]: e.target.value});

    onloadFileOne = (e) => {
        this.setState({evaluationOne: e.target.files[0]});
        const reader = new FileReader();
        reader.onload = () => {
            if (reader.readyState === 2) this.setState({viewFile: reader.result});
        }
        reader.readAsDataURL(e.target.files[0]);
    }

    onloadFileTwo = (e) => {
        this.setState({evaluationTwo: e.target.files[0]});
        const reader = new FileReader();
        reader.onload = () => {
            if (reader.readyState === 2) this.setState({viewFile: reader.result});
        }
        reader.readAsDataURL(e.target.files[0]);
    }

    sendEvaluationOne = (e) => {
        e.preventDefault();
        const data = new FormData();
        data.append('evaluationOne',this.state.evaluationOne, this.state.evaluationOne.name);
        this.props.add_evaluation_one(data, this.props.pk);
    }

    sendEvaluationTwo = (e) => {
        e.preventDefault();
        const data = new FormData();
        data.append('evaluationTwo',this.state.evaluationTwo, this.state.evaluationTwo.name);
        this.props.add_evaluation_two(data, this.props.pk);
    }

    onSubmit = (e) => {
        e.preventDefault();
        this.props.add_evaluation_result(this.state.result, this.props.pk);
    }

    render() {

        let formEvaluationOne = (
            <form className="card pt-2" onSubmit={this.sendEvaluationOne}>
                <label>Archivo de Primera Evaluación</label>
                <input type="file" name="evaluationOne" className="form-control col-11 m-auto" required
                       value={this.evaluationOne} onChange={this.onloadFileOne} />
                <section className="col-4 m-auto pt-2 pb-2">
                    <button className="btn btn-warning">Guardar</button>
                </section>
            </form>
        );

        let formEvaluationTwo = (
            <form className="card pt-2 mt-2" onSubmit={this.sendEvaluationTwo}>
                <label>Archivo de Segunda Evaluación</label>
                <input type="file" name="evaluationTwo" className="form-control col-11 m-auto" required
                       value={this.evaluationTwo} onChange={this.onloadFileTwo} />
                <section className="col-4 m-auto pt-2 pb-2">
                    <button className="btn btn-warning">Guardar</button>
                </section>
            </form>

        );

        let formResultStatus = (
            <form className="card pt-2 mt-2" onSubmit={this.onSubmit}>
                <label>Resultado</label>
                <select className="form-control col-11 m-auto" name="result" value={this.state.result}
                        onChange={this.onChange}>
                    <option key={0} value={1}>Publicable</option>
                    <option key={1} value={2}>Publicable Condicionado</option>
                    <option key={2} value={3}>No Publicable</option>
                    <option key={3} value={4}>Publicable con Modificaciones</option>

                </select>
                <section className="col-4 m-auto pt-2 pb-2">
                    <button className="btn btn-warning">Guardar</button>
                </section>
            </form>
        );

        if (this.props.dictum !== null){
            if ('evaluationOne' in this.props.dictum) {
                formEvaluationOne = (
                    <section className="col-12 m-auto card py-2 text-center">
                        <h6 className="pr-3">
                            <strong>Primer Evaluación: </strong>
                            <a target="_blank" rel="noopener noreferrer" className="float-right pl-2"
                               href={`${SERVER}${this.props.dictum.evaluationOne}`}>
                                Ver Archivo
                            </a>
                        </h6>
                    </section>
                )
            }

            if ('evaluationTwo' in this.props.dictum) {
                if (this.props.dictum.evaluationTwo !== ""){
                    formEvaluationTwo = (
                        <section className="col-12 card py-2 text-center mt-2">
                            <h6 className="pr-3">
                                <strong>Segunda Evaluación: </strong>
                                <a target="_blank" rel="noopener noreferrer" className="float-right pl-2"
                                   href={`${SERVER}${this.props.dictum.evaluationTwo}`}>
                                    Ver Archivo
                                </a>
                            </h6>
                        </section>
                    );
                }
            }


            if ('result' in this.props.dictum) {
                if (this.props.dictum.result !== null){
                    formResultStatus = (
                        <section className="col-12 card py-2 text-center mt-2">
                            <strong>Resultado: </strong>
                            <h6 className="pr-3">
                                {
                                    this.props.dictum.result === 1
                                        ?
                                        'Publicable'
                                        :
                                    this.props.dictum.result === 2
                                        ?
                                        'Publicable condicionado a cambios y sugerencias del dictamen'
                                        :
                                    this.props.dictum.result === 3
                                        ?
                                        'No Publicable'
                                        :
                                    this.props.dictum.result === 4
                                        ?
                                        'Publicable con modificaciones sustantivas y sujeto a una nueva revisión'
                                        :
                                        null
                                }
                            </h6>
                        </section>
                    );
                }
            }
        }

        return (
            <section>

                {formEvaluationOne}
                {formEvaluationTwo}
                {formResultStatus}
            </section>
        )


    }
}

const mapStateToProps = (state) => ({

});


export default connect(
    mapStateToProps, {
        add_evaluation_one, add_evaluation_two, add_evaluation_result})(Dictum);