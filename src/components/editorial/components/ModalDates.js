import React, { Component } from 'react';
import {connect} from 'react-redux';
import PropTypes from "prop-types";

import DeliveryDateToEditorial from "./dates/DeliveryDateToEditorial";
import CorrectionDate from "./dates/CorrectionDate";
import FirstReviewDate from "./dates/FirstReviewDate";
import SecondRevisionDate from "./dates/SecondRevisionDate";
import ThirdReviewDate from "./dates/ThirdReviewDate";
import GalleyReviewDate from "./dates/GalleyReviewDate";
import IsbnDate from "./dates/IsbnDate";
import DeliveryDate from "./dates/DeliveryDate";
import PriceDate from "./dates/PriceDate";


export class ModalDates extends Component {

    static propTypes = {
        edit: PropTypes.object.isRequired
    };

    render() {
        return (
            <div className="modal fade" id="modalDates" tabIndex="-1" aria-labelledby="exampleModalLabel"
                 aria-hidden="true">
                <div className="modal-dialog modal-lg">
                    <div className="modal-content">
                        <div className="modal-body">
                            <DeliveryDateToEditorial
                                delivery_date_to_editorial={this.props.edit.delivery_date_to_editorial}
                                finalized={this.props.edit.finalized}
                                pk={this.props.edit.id}
                            />
                            <CorrectionDate
                                correction_date={this.props.edit.correction_date}
                                finalized={this.props.edit.finalized}
                                pk={this.props.edit.id}
                            />
                            <FirstReviewDate
                                first_review_date={this.props.edit.first_review_date}
                                finalized={this.props.edit.finalized}
                                pk={this.props.edit.id}
                            />
                            <SecondRevisionDate
                                second_revision_date={this.props.edit.second_revision_date}
                                finalized={this.props.edit.finalized}
                                pk={this.props.edit.id}
                            />
                            <ThirdReviewDate
                                third_review_date={this.props.edit.third_review_date}
                                finalized={this.props.edit.finalized}
                                pk={this.props.edit.id}
                            />

                            <GalleyReviewDate
                                galley_review_date={this.props.edit.galley_review_date}
                                finalized={this.props.edit.finalized}
                                pk={this.props.edit.id}
                            />

                            <IsbnDate
                                isbn_date={this.props.edit.isbn_date}
                                finalized={this.props.edit.finalized}
                                pk={this.props.edit.id}
                            />

                            <DeliveryDate
                                delivery_date={this.props.edit.delivery_date}
                                finalized={this.props.edit.finalized}
                                pk={this.props.edit.id}
                            />

                            <PriceDate
                                price_date={this.props.edit.price_date}
                                finalized={this.props.edit.finalized}
                                pk={this.props.edit.id}
                            />

                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-warning" data-dismiss="modal">
                                Cerrar Registro de Fechas
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({});


export default connect(mapStateToProps, null)(ModalDates);