import React, {Component} from 'react';
import PropTypes from "prop-types";
import {connect} from "react-redux";

import {SERVER} from "../../../actions/server";
import {FormatDate} from "../../FormatDate";
import {add_letter} from '../../../actions/editorial/add_letter';


export class Letter extends Component {

    state = {
        observations: '',

    }

    static propTypes = {
        letter: PropTypes.object,
        pk: PropTypes.number.isRequired,
        add_letter: PropTypes.func.isRequired
    };

    onChange = (e) => this.setState({[e.target.name]: e.target.value});

    onSubmit = (e) => {
        e.preventDefault();
        this.props.add_letter(this.state.observations, this.props.pk);
    }

    render() {

        if (this.props.letter !== null){
            return (
                <section className="card text-left">
                    <div className="dsch-det-card alert-dark text-left">
                        <label>
                            <strong>Fecha de Entrega </strong>
                        </label>
                    </div>
                    <h6 className="pl-3">{FormatDate(this.props.letter.date)}</h6>

                    <div className="dsch-det-card alert-dark text-left">
                        <label>
                            <strong>Observaciones</strong>
                        </label>
                    </div>
                    <h6 className="pl-3 pr-3">{this.props.letter.observations}</h6>
                </section>
            );
        } else {
           return (
               <form className="card" onSubmit={this.onSubmit}>
                    <div className="form-group col-12 m-auto pt-3">
                        <h5>Observaciones</h5>
                        <textarea name="observations" className="form-control" required rows="3"
                                  value={this.state.observations} onChange={this.onChange}>
                        </textarea>
                    </div>

                   <div className="form-group col-12 m-auto pt-3 pb-3">
                        <button className="btn-dsch">Entregado</button>
                    </div>
                </form>
            );
        }
    }
}

const mapStateToProps = (state) => ({});


export default connect(mapStateToProps, {add_letter})(Letter)