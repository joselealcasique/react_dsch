import React, { Component } from 'react';
import { connect } from "react-redux";

import ConcourseList from "../concourse/ConcourseList";


export class ListCurricular extends  Component {

    render() {
        return (
            <ConcourseList type_concourse={1}/>
        )
    }
}


const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, null )(ListCurricular);